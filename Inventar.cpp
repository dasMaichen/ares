//
// Created by das_maichen on 15.06.18.
//

#include <iostream>
#include "Inventar.h"
#include "DEFINITIONS.h"
#include "States/MainMenuState.h"

Inventar::Inventar(GameDataRef data) : data(data){

    menuSite = 1;
    titleInventar.setFont(this->data->assets.getFont("GameFont"));
    titleInventar.setCharacterSize(30);
    titleInventar.setColor(sf::Color::White);
    titleInventar.setString("Inventar");


    chosenItem = new Item(data);
    chosenItem->inizialize(true, false, false, false, "ChosenItem", "NoType", sf::Vector2f(0,0),0);
    settingActiveItem = false;

    chosenLevelLayer.setSize(sf::Vector2f(60,40));
    chosenLevelLayer.setFillColor(sf::Color(0,255,255,128));
    chosenLevelLayer.setOutlineThickness(3);
    chosenLevelLayer.setOutlineColor(sf::Color(0,12,169,128));


    chosenOption.setSize(sf::Vector2f(43,40));
    chosenOption.setFillColor(sf::Color(128,128,128, 200));

    selectedItemColumn = 2;
    selectedItemRow = 0;

    selectedLayerColumn = 1;
    selectedLayerRow = 0;

    getFlintstone = false;
    getCrossbow = false;
    getTalisman = false;
    getTorch = false;
    getKey = false;

    //inventar.setTexture(this->data->assets.getTexture("InventarBox"));
    menuBox.setTexture(this->data->assets.getTexture("InventarBox"));
    menuBox.setScale(0.45,0.45);

    //flintstone.setScale(0.5, 0.5);
    flintstone.setTexture(this->data->assets.getTexture("Feuerstein"));
    crossbow.setTexture(this->data->assets.getTexture("Crossbow"));
    talisman.setTexture(this->data->assets.getTexture("Talisman"));
    torch.setTexture(this->data->assets.getTexture("Torch"));
    key.setTexture(this->data->assets.getTexture("Key"));

    countKey = 0;
    countTalisman = 0;
    countTorch = 0;

    countKeyText.setFont(this->data->assets.getFont("Number"));
    countKeyText.setCharacterSize(20);
    countKeyText.setColor(sf::Color::White);
    //countKeyText.setString(std::to_string(countKey));

    countTalismanText.setFont(this->data->assets.getFont("Number"));
    countTalismanText.setCharacterSize(20);
    countTalismanText.setColor(sf::Color::White);
    //countTalismanText.setString(std::to_string(countTalisman));

    countTorchText.setFont(this->data->assets.getFont("Number"));
    countTorchText.setCharacterSize(20);
    countTorchText.setColor(sf::Color::White);
    //countTorchText.setString(std::to_string(countTorch));


    //crossbow.setScale(0.5,0.5);

    chosenItemBox.setSize(sf::Vector2f(100,100));
    chosenItemBox.setPosition(sf::Vector2f(menuBox.getPosition().x - 90, menuBox.getPosition().y + 220));
    chosenItemBox.setFillColor(sf::Color(0,255,255,128));
    chosenItemBox.setOutlineThickness(3);
    chosenItemBox.setOutlineColor(sf::Color(0,12,169,128));

    activeItemSprite.setTexture(this->data->assets.getTexture("NoItem"));

    for (int i = 0; i < 5; i++) {
        itemSprites[i].setSize(sf::Vector2f(100,100));
        //menuBox.getPosition()
        itemSprites[i].setPosition(sf::Vector2f(menuBox.getPosition().x + (i*100+i*10) - 90, menuBox.getPosition().y + 200));
        itemSprites[i].setFillColor(sf::Color(128,128,128,128));
    }
    int j = 0;
    for (int i = 5; i < 10; i++) {
        itemSprites[i].setSize(sf::Vector2f(100,100));
        itemSprites[i].setPosition(sf::Vector2f(menuBox.getPosition().x + (j*100+j*10) - 90, menuBox.getPosition().y + 320));
        itemSprites[i].setFillColor(sf::Color(128,128,128,128));
        j++;
    }

    for (int k = 0; k < 2; k++) {
        levelLayerSprites[k].setSize(sf::Vector2f(60,40));
        //levelLayerSprites[k].setPosition(sf::Vector2f(menuBox.getPosition().x + 400, menuBox.getPosition().y + (20*k + 20*k) + 100));
        levelLayerSprites[k].setPosition(sf::Vector2f(menuBox.getPosition().x + 600, menuBox.getPosition().y + (40*k + 20*k) + 330));
        levelLayerSprites[k].setFillColor(sf::Color(128,128,128, 128));
    }

    //levelLayerSprites[2].setSize(sf::Vector2f(30,30));
    //levelLayerSprites[2].setPosition(sf::Vector2f(menuBox.getPosition().x + 50, menuBox.getPosition().y + 300));
    //levelLayerSprites[2].setFillColor(sf::Color(128,128,128, 128));

    eg.setFont(this->data->assets.getFont("GameFont"));
    eg.setCharacterSize(25);
    eg.setColor(sf::Color::White);
    eg.setString("EG");

    ug.setFont(this->data->assets.getFont("GameFont"));
    ug.setCharacterSize(25);
    ug.setColor(sf::Color::White);
    ug.setString("UG");

    titleMap.setFont(this->data->assets.getFont("GameFont"));
    titleMap.setCharacterSize(30);
    titleMap.setColor(sf::Color::White);
    titleMap.setString("Dungeon-Karte");

    this->data->assets.loadTexture("map0_eg", "Content/Texture/MenuTexture/map_0.png");
    this->data->assets.loadTexture("map0_ug", "Content/Texture/MenuTexture/map_0_ug.png");
    this->data->assets.loadTexture("map1", "Content/Texture/MenuTexture/map_1.png");
    this->data->assets.loadTexture("map2", "Content/Texture/MenuTexture/map_2.png");
    this->data->assets.loadTexture("map3", "Content/Texture/MenuTexture/map_3.png");
    this->data->assets.loadTexture("map4", "Content/Texture/MenuTexture/map_4.png");
    this->data->assets.loadTexture("map5", "Content/Texture/MenuTexture/map_5.png");
    this->data->assets.loadTexture("map6", "Content/Texture/MenuTexture/map_6.png");
    this->data->assets.loadTexture("map7", "Content/Texture/MenuTexture/map_7.png");
    this->data->assets.loadTexture("map8", "Content/Texture/MenuTexture/map_8.png");
    this->data->assets.loadTexture("arrowLeft", "Content/Texture/MenuTexture/arrow_left_grey.png");
    this->data->assets.loadTexture("arrowLeftWhite", "Content/Texture/MenuTexture/arrow_left_white.png");
    this->data->assets.loadTexture("arrowRight", "Content/Texture/MenuTexture/arrow_right_grey.png");
    this->data->assets.loadTexture("arrowRightWhite", "Content/Texture/MenuTexture/arrow_right_white.png");

    haveDungeonMap = false;

    noDungeonMap.setFont(this->data->assets.getFont("GameFont"));
    noDungeonMap.setCharacterSize(30);
    noDungeonMap.setString("Keine Karte vorhanden");
    noDungeonMap.setColor(sf::Color(255,255,255,128));

    arrowLeft.setTexture(this->data->assets.getTexture("arrowLeft"));
    arrowLeft.setScale(0.5,0.5);

    arrowRight.setTexture(this->data->assets.getTexture("arrowRight"));
    arrowRight.setScale(0.5,0.5);

    //arrowLeftWhite.setTexture(this->data->assets.getTexture("arrowLeftWhite"));
    //arrowLeftWhite.setScale(0.7,0.7);

    menuTitle.setFont(this->data->assets.getFont("GameFont"));
    menuTitle.setCharacterSize(30);
    menuTitle.setColor(sf::Color::White);
    menuTitle.setString("Menu");

    toMainMenu = false;

    menuText[0].setFont(this->data->assets.getFont("GameFont"));
    menuText[0].setColor(sf::Color::Cyan);
    menuText[0].setString("Main Menu");
    //-50 ist hier noch ein workaround :P
    //menuText[0].setPosition(335,260);
    //menuText[0].setPosition(sf::Vector2f((width/2 - 50), height/(MAX_NUMBER_OF_ITEMS_MAINMENU+1)*2));

    menuText[1].setFont(this->data->assets.getFont("GameFont"));
    menuText[1].setColor(sf::Color::White);
    menuText[1].setString("Save");
    //menuText[1].setPosition(365,335);

    menuText[2].setFont(this->data->assets.getFont("GameFont"));
    menuText[2].setColor(sf::Color::White);
    menuText[2].setString("Settings");
    //menuText[2].setPosition(345,410);

    for (int l = 0; l < 3; ++l) {
        buttons[l].setTexture(this->data->assets.getTexture("PlayButton"));
        buttons[l].setScale(0.2, 0.2);
    }


}


void Inventar::chooseItem(sf::Event event) {
    if (event.type == sf::Event::KeyReleased) {
        if(menuSite == 0){
            switch (event.key.code) {
                case sf::Keyboard::Right:
                    menuMoveRight();
                    break;
                case sf::Keyboard::Left:
                    menuMoveLeft();
                    break;
                case sf::Keyboard::Up:
                    if(selectedMenuIndexColumn == 0){
                        menuMoveUp();
                    }
                    break;
                case sf::Keyboard::Down:
                    if(selectedMenuIndexColumn == 0){
                        menuMoveDown();
                    }
                    break;

                case sf::Keyboard::Return:
                    //if(selectedMenuIndexColumn == 0){
                        switch (selectedMenuIndexRow){
                            case 0:
                                toMainMenu = true;
                        }
                    //}

            }
        }
        if(menuSite == 1) {
            switch (event.key.code) {
                case sf::Keyboard::Right:
                    itemMoveRight();
                    break;
                case sf::Keyboard::Left:
                    itemMoveLeft();
                    break;
                case sf::Keyboard::Up:
                    if(selectedItemColumn >1 && selectedItemColumn < 7){
                        itemMoveUp();
                    }

                    break;
                case sf::Keyboard::Down:
                    if(selectedItemColumn >1 && selectedItemColumn < 7) {
                        itemMoveDown();
                        if(selectedItemColumn >1 && selectedItemColumn < 7){

                        }
                    }
                    break;
                case sf::Keyboard::Space:
                    settingActiveItem = true;
                    break;
            }
        }
        if(menuSite == 2) {
            switch (event.key.code) {
                case sf::Keyboard::Right:
                    mapMoveRight();
                    break;
                case sf::Keyboard::Left:
                    mapMoveLeft();
                    break;
                case sf::Keyboard::Up:
                    if(selectedLayerColumn == 2){
                        mapMoveUp();
                    }
                    break;
                case sf::Keyboard::Down:
                    if(selectedLayerColumn == 2){
                        mapMoveDown();
                    }

                    break;
            }
        }

    }
}


void Inventar::menuMoveDown() {
    if(selectedMenuIndexRow + 1 < 3){
        menuText[selectedMenuIndexRow].setColor(sf::Color::White);
        selectedMenuIndexRow ++;
        menuText[selectedMenuIndexRow].setColor(sf::Color::Cyan);
    }



}

void Inventar::menuMoveUp() {
    if(selectedMenuIndexRow -1 >= 0){
        menuText[selectedMenuIndexRow].setColor(sf::Color::White);
        selectedMenuIndexRow--;
        menuText[selectedMenuIndexRow].setColor(sf::Color::Cyan);
    }

}

void Inventar::menuMoveLeft() {
    if(selectedMenuIndexColumn - 1 >= 0){
        selectedMenuIndexColumn--;
    }

}

void Inventar::menuMoveRight() {

    if(selectedMenuIndexColumn + 1 < 3){

        if(selectedMenuIndexColumn + 1 == 2){
            menuSite = 1;
            selectedItemColumn = 1;

        }

        selectedMenuIndexColumn++;
        //chosenItemBox.setPosition(sf::Vector2f(chosenItemBox.getPosition().x+110,chosenItemBox.getPosition().y));
    }
}

void Inventar::itemMoveRight() {
    if(selectedItemColumn + 1 < 9){

        if(selectedItemColumn + 1 == 8){
            menuSite = 2;
            selectedLayerColumn = 2;
        }

        selectedItemColumn++;
        //chosenItemBox.setPosition(sf::Vector2f(chosenItemBox.getPosition().x+110,chosenItemBox.getPosition().y));
    }settingActiveItem = false;

}

void Inventar::itemMoveLeft() {
    if(selectedItemColumn - 1 >= 0){
        selectedItemColumn--;
        if(selectedItemColumn == 0){
            menuSite = 0;
            selectedMenuIndexColumn = 0;
            selectedMenuIndexRow = 0;
            for (int i = 0; i < 3; ++i) {
                menuText[i].setColor(sf::Color::White);
            }
        }
        settingActiveItem = false;
        //chosenItemBox.setPosition(sf::Vector2f(chosenItemBox.getPosition().x-110,chosenItemBox.getPosition().y));
    }
}

void Inventar::itemMoveDown() {
    if(selectedItemRow + 1 < 2){
        selectedItemRow++;
        chosenItemBox.setPosition(sf::Vector2f(chosenItemBox.getPosition().x,chosenItemBox.getPosition().y+120));
    }settingActiveItem = false;

}

void Inventar::itemMoveUp() {
    if(selectedItemRow - 1 >= 0){
        selectedItemRow--;
        chosenItemBox.setPosition(sf::Vector2f(chosenItemBox.getPosition().x,chosenItemBox.getPosition().y-120));
    }settingActiveItem = false;
}

void Inventar::mapMoveDown() {
    if(selectedLayerRow + 1 < 2){
        selectedLayerRow++;
        //chosenLevelLayer.setPosition(sf::Vector2f(.getPosition().x,chosenItemBox.getPosition().y+120));
    }
}

void Inventar::mapMoveUp() {
    if(selectedLayerRow - 1 >= 0){
        selectedLayerRow--;
        //chosenItemBox.setPosition(sf::Vector2f(chosenItemBox.getPosition().x,chosenItemBox.getPosition().y-120));
    }
}

void Inventar::mapMoveLeft() {
    //menuSite = 1;
    //selectedItemColumn = 4;
    if(selectedLayerColumn - 1 >= 0){
        selectedLayerColumn--;

        if(selectedLayerColumn == 0){
            menuSite = 1;
            selectedItemColumn = 2;
        }

        //if(selectedLayerColumn == 1){
            //chosenLevelLayer.setPosition(sf::Vector2f(chosenItemBox.getPosition().x-25,chosenItemBox.getPosition().y));
        //}

    }
}

void Inventar::mapMoveRight() {
    if(selectedLayerColumn + 1 < 3){

        selectedLayerColumn++;
        //chosenItemBox.setPosition(sf::Vector2f(chosenItemBox.getPosition().x+110,chosenItemBox.getPosition().y));
    }

}




void Inventar::update(sf::Vector2f position, int roomNo) {

    menuBox.setPosition(sf::Vector2f(position.x - 350,position.y - 250));

    if(menuSite == 0){
        arrowRight.setPosition(sf::Vector2f(menuBox.getPosition().x + 638, menuBox.getPosition().y + 230));

        menuTitle.setPosition(sf::Vector2f(menuBox.getPosition().x + 70, menuBox.getPosition().y + 50));
        for (int j = 0; j < 3; ++j) {
            buttons[j].setPosition(menuBox.getPosition().x + 220, menuBox.getPosition().y + (j * 80) + 180);
        }

        if(selectedMenuIndexColumn == 0) {


            menuText[0].setPosition(menuBox.getPosition().x + 280, menuBox.getPosition().y + 195);
            menuText[1].setPosition(menuBox.getPosition().x + 325, menuBox.getPosition().y + 275);
            menuText[2].setPosition(menuBox.getPosition().x + 305, menuBox.getPosition().y + 355);

            if (selectedMenuIndexRow == 0) {
                buttons[0].setColor(sf::Color(255, 255, 255, 255));
                buttons[1].setColor(sf::Color(255, 255, 255, 128));
                buttons[2].setColor(sf::Color(255, 255, 255, 128));

                menuText[0].setColor(sf::Color::Cyan);
            }

            if (selectedMenuIndexRow == 1) {
                buttons[0].setColor(sf::Color(255, 255, 255, 128));
                buttons[1].setColor(sf::Color(255, 255, 255, 255));
                buttons[2].setColor(sf::Color(255, 255, 255, 128));
                menuText[1].setColor(sf::Color::Cyan);
            }

            if (selectedMenuIndexRow == 2) {
                buttons[0].setColor(sf::Color(255, 255, 255, 128));
                buttons[1].setColor(sf::Color(255, 255, 255, 128));
                buttons[2].setColor(sf::Color(255, 255, 255, 255));
                menuText[2].setColor(sf::Color::Cyan);
            }
        } else{
            menuText[0].setPosition(menuBox.getPosition().x + 280, menuBox.getPosition().y + 195);
            menuText[1].setPosition(menuBox.getPosition().x + 325, menuBox.getPosition().y + 275);
            menuText[2].setPosition(menuBox.getPosition().x + 305, menuBox.getPosition().y + 355);

            menuText[0].setColor(sf::Color::White);
            menuText[1].setColor(sf::Color::White);
            menuText[2].setColor(sf::Color::White);

            buttons[0].setColor(sf::Color(255, 255, 255, 128));
            buttons[1].setColor(sf::Color(255, 255, 255, 128));
            buttons[2].setColor(sf::Color(255, 255, 255, 128));
        }

        if(selectedMenuIndexColumn == 1){
            //chosenItemBox.setSize(sf::Vector2f(43,40));
            arrowRight.setTexture(this->data->assets.getTexture("arrowRightWhite"));
            chosenOption.setFillColor(sf::Color(0,255,255,128));
            chosenOption.setOutlineThickness(3);
            chosenOption.setOutlineColor(sf::Color(0,12,169,128));
            chosenOption.setPosition(sf::Vector2f(menuBox.getPosition().x + 640, menuBox.getPosition().y + 235));
        } else{
            arrowRight.setTexture(this->data->assets.getTexture("arrowRight"));
        }

    }



    if(menuSite == 1){

        titleInventar.setPosition(sf::Vector2f(menuBox.getPosition().x + 70, menuBox.getPosition().y +50));

        //chosenItemBox.setPosition(sf::Vector2f(menuBox.getPosition().x + (selectedItemColumn*100+selectedItemColumn*10) + 90, menuBox.getPosition().y + 200 + (selectedItemRow * 120)));


        if(selectedItemColumn > 1) {
            chosenItemBox.setSize(sf::Vector2f(100,100));
            chosenItemBox.setPosition(sf::Vector2f(menuBox.getPosition().x +
                                                   ((selectedItemColumn-2)*100+(selectedItemColumn-2)*10) + 90,
                                                   menuBox.getPosition().y + 200 + (selectedItemRow * 120)));

            for (int i = 0; i < 5; ++i) {
                itemSprites[i].setPosition(
                        sf::Vector2f(menuBox.getPosition().x + (i * 100 + i * 10) + 90, menuBox.getPosition().y + 200));
                if (i == 0) {
                    flintstone.setPosition(itemSprites[i].getPosition());
                }
                if (i == 1) {
                    crossbow.setPosition(itemSprites[i].getPosition());
                }
            }


            int j = 0;
            for (int i = 5; i < 10; ++i) {
                itemSprites[i].setPosition(
                        sf::Vector2f(menuBox.getPosition().x + (j * 100 + j * 10) + 90, menuBox.getPosition().y + 320));
                if(i == 5){
                    key.setPosition(itemSprites[i].getPosition());
                    countKeyText.setPosition(itemSprites[i].getPosition().x + 70,itemSprites[i].getPosition().y + 70);
                }
                if(i == 6){
                    talisman.setPosition(itemSprites[i].getPosition());
                    countTalismanText.setPosition(itemSprites[i].getPosition().x + 70,itemSprites[i].getPosition().y + 70);
                }
                if(i == 7){
                    torch.setPosition(itemSprites[i].getPosition());
                    countTorchText.setPosition(itemSprites[i].getPosition().x + 70,itemSprites[i].getPosition().y + 70);
                }
                j++;
            }
        }

        if(selectedItemColumn == 1){
            chosenItemBox.setSize(sf::Vector2f(43,40));
            arrowLeft.setTexture(this->data->assets.getTexture("arrowLeftWhite"));
            chosenItemBox.setPosition(sf::Vector2f(menuBox.getPosition().x + 35, menuBox.getPosition().y + 235));
        } else{
            arrowLeft.setTexture(this->data->assets.getTexture("arrowLeft"));
        }

        if(selectedItemColumn == 7){
            chosenItemBox.setSize(sf::Vector2f(43,40));
            arrowRight.setTexture(this->data->assets.getTexture("arrowRightWhite"));
            chosenItemBox.setPosition(sf::Vector2f(menuBox.getPosition().x + 640, menuBox.getPosition().y + 235));
        } else{
            arrowRight.setTexture(this->data->assets.getTexture("arrowRight"));
        }




        //flintstone.setPosition(position);
        //crossbow.setPosition(position.x + 50, position.y);


        if(chosenItemBox.getPosition() == flintstone.getPosition() && getFlintstone && settingActiveItem){
            for (int i = 0; i < 5; ++i) {
                itemSprites[i].setFillColor(sf::Color(128,128,128,128));
            }
            for (int j = 5; j < 10; ++j) {
                itemSprites[j].setFillColor(sf::Color(128,128,128,128));
            }
            itemSprites[0].setFillColor(sf::Color(0,255,255,128));

            chosenItem->setName("Feuerstein");

            settingActiveItem = false;
            //selectedItemColumn = 2;
        }
        if(chosenItemBox.getPosition() == crossbow.getPosition() && getCrossbow && settingActiveItem){
            chosenItem->setName("Crossbow");
            for (int i = 0; i < 5; ++i) {
                itemSprites[i].setFillColor(sf::Color(128,128,128,128));
            }
            for (int j = 5; j < 10; ++j) {
                itemSprites[j].setFillColor(sf::Color(128,128,128,128));
            }
            itemSprites[1].setFillColor(sf::Color(0,255,255,128));
            settingActiveItem = false;

            //selectedItemColumn = 3;
        }


        if(chosenItemBox.getPosition() == key.getPosition() && getKey && settingActiveItem && countKey > 0){
            chosenItem->setName("Key");
            for (int i = 0; i < 5; ++i) {
                itemSprites[i].setFillColor(sf::Color(128,128,128,128));
            }
            for (int j = 5; j < 10; ++j) {
                itemSprites[j].setFillColor(sf::Color(128,128,128,128));
            }
            itemSprites[5].setFillColor(sf::Color(0,255,255,128));
            settingActiveItem = false;
            //selectedItemColumn = 3;
        }

        if(chosenItemBox.getPosition() == talisman.getPosition() && getTalisman && settingActiveItem && countTalisman > 0){
            chosenItem->setName("Talisman");
            for (int i = 0; i < 5; ++i) {
                itemSprites[i].setFillColor(sf::Color(128,128,128,128));
            }
            for (int j = 5; j < 10; ++j) {
                itemSprites[j].setFillColor(sf::Color(128,128,128,128));
            }
            itemSprites[6].setFillColor(sf::Color(0,255,255,128));
            settingActiveItem = false;

            //selectedItemColumn = 3;
        }

        if(chosenItemBox.getPosition() == torch.getPosition() && getTorch && settingActiveItem && countTorch > 0){
            chosenItem->setName("Torch");
            for (int i = 0; i < 5; ++i) {
                itemSprites[i].setFillColor(sf::Color(128,128,128,128));
            }
            for (int j = 5; j < 10; ++j) {
                itemSprites[j].setFillColor(sf::Color(128,128,128,128));
            }
            itemSprites[7].setFillColor(sf::Color(0,255,255,128));
            settingActiveItem = false;

            //selectedItemColumn = 3;
        }
        /*
        if(selectedItemColumn == 5){
            chosenItemBox.setPosition(sf::Vector2f(menuBox.getPosition().x + (selectedItemColumn*100+selectedItemColumn*10) + 90, menuBox.getPosition().y + 250));
        }*/
        arrowLeft.setPosition(sf::Vector2f(menuBox.getPosition().x + 30, menuBox.getPosition().y + 230));
        arrowRight.setPosition(sf::Vector2f(menuBox.getPosition().x + 638, menuBox.getPosition().y + 230));

        countKeyText.setString(std::to_string(countKey));
        countTalismanText.setString(std::to_string(countTalisman));
        countTorchText.setString(std::to_string(countTorch));
    }

    if(menuSite == 2){

        titleMap.setPosition(sf::Vector2f(menuBox.getPosition().x + 70, menuBox.getPosition().y +50));
        noDungeonMap.setPosition(sf::Vector2f(menuBox.getPosition().x + 70, menuBox.getPosition().y +100));

        if(roomNo == 1){
            if(selectedLayerRow == 1){
                map.setTexture(this->data->assets.getTexture("map1"));
            } else{
                map.setTexture(this->data->assets.getTexture("map0_eg"));
            }
            //selectedLayerRow = 1;
        }
        if(roomNo == 2){
            if(selectedLayerRow == 0){
                map.setTexture(this->data->assets.getTexture("map2"));
            } else{
                map.setTexture(this->data->assets.getTexture("map0_ug"));
            }
        }
        if(roomNo == 3){
            if(selectedLayerRow == 0){
                map.setTexture(this->data->assets.getTexture("map3"));
            } else{
                map.setTexture(this->data->assets.getTexture("map0_ug"));
            }
        }
        if(roomNo == 4){
            if(selectedLayerRow == 0){
                map.setTexture(this->data->assets.getTexture("map4"));
            } else{
                map.setTexture(this->data->assets.getTexture("map0_ug"));
            }
        }
        if(roomNo == 5){
            if(selectedLayerRow == 0){
                map.setTexture(this->data->assets.getTexture("map5"));
            } else{
                map.setTexture(this->data->assets.getTexture("map0_ug"));
            }
        }
        if(roomNo == 6){
            if(selectedLayerRow == 0){
                map.setTexture(this->data->assets.getTexture("map0_eg"));
            } else{
                map.setTexture(this->data->assets.getTexture("map6"));
            }
        }
        if(roomNo == 7){
            if(selectedLayerRow == 0){
                map.setTexture(this->data->assets.getTexture("map7"));
            } else{
                map.setTexture(this->data->assets.getTexture("map0_ug"));
            }
        }if(roomNo == 8){
            if(selectedLayerRow == 0){
                map.setTexture(this->data->assets.getTexture("map8"));
            } else{
                map.setTexture(this->data->assets.getTexture("map0_ug"));
            }
        }

        if(selectedLayerColumn == 1){
            chosenLevelLayer.setSize(sf::Vector2f(43,40));
            chosenLevelLayer.setFillColor(sf::Color(0,255,255,128));
            chosenLevelLayer.setOutlineThickness(3);
            chosenLevelLayer.setOutlineColor(sf::Color(0,12,169,128));
            arrowLeft.setTexture(this->data->assets.getTexture("arrowLeftWhite"));
            chosenLevelLayer.setPosition(sf::Vector2f(menuBox.getPosition().x + 35, menuBox.getPosition().y + 235));
        }


        map.setPosition(menuBox.getPosition().x + 100, menuBox.getPosition().y +50);

        for (int k = 0; k < 2; k++) {
            levelLayerSprites[k].setPosition(sf::Vector2f(menuBox.getPosition().x + 600, menuBox.getPosition().y + (40*k + 20*k) + 330));
        }


        if(selectedLayerColumn == 2) {
            arrowLeft.setTexture(this->data->assets.getTexture("arrowLeft"));
            chosenLevelLayer.setSize(sf::Vector2f(60,40));
            //chosenLevelLayer.setFillColor(sf::Color(128,128,128,200));
            chosenLevelLayer.setPosition(sf::Vector2f(menuBox.getPosition().x + 600, menuBox.getPosition().y +
                                                                                     (40 * selectedLayerRow +
                                                                                      20 * selectedLayerRow) + 330));

        }


        /*
        if(selectedLayerRow == 0){
            eg.setColor(sf::Color::Cyan);
            ug.setColor(sf::Color::White);
        }else if(selectedLayerRow == 1){
            eg.setColor(sf::Color::White);
            ug.setColor(sf::Color::Cyan);
        }
        if(selectedLayerColumn == 1){
            eg.setColor(sf::Color::White);
            ug.setColor(sf::Color::White);
        }
         */


        //levelLayerSprites[2].setPosition(sf::Vector2f(menuBox.getPosition().x + 50, menuBox.getPosition().y + 230));
        arrowLeft.setPosition(sf::Vector2f(menuBox.getPosition().x + 30, menuBox.getPosition().y + 230));

        eg.setPosition(sf::Vector2f(menuBox.getPosition().x + 615, menuBox.getPosition().y + 335));
        ug.setPosition(sf::Vector2f(menuBox.getPosition().x + 615, menuBox.getPosition().y + 395));
    }



}

void Inventar::draw() {

    //this->data->window.draw(inventar);
    this->data->window.draw(menuBox);

    if(menuSite == 0){
        if(selectedMenuIndexColumn == 1){
            this->data->window.draw(chosenOption);
        }
        this->data->window.draw(arrowRight);
        this->data->window.draw(menuTitle);

        for (int j = 0; j < 3; ++j) {
            this->data->window.draw(buttons[j]);
        }

        for (int i = 0; i < 3; ++i) {
            this->data->window.draw(menuText[i]);
        }
    }

    if(menuSite == 1) {

        for (int i = 0; i < 10; ++i) {
            this->data->window.draw(itemSprites[i]);
        }

        this->data->window.draw(chosenItemBox);

        if (getFlintstone) {
            this->data->window.draw(flintstone);
        }

        if (getCrossbow) {
            this->data->window.draw(crossbow);
        }

        if(getTalisman){
            this->data->window.draw(talisman);
            this->data->window.draw(countTalismanText);
        }

        if(getTorch){
            this->data->window.draw(torch);
            this->data->window.draw(countTorchText);
        }

        if(getKey){
            this->data->window.draw(key);
            this->data->window.draw(countKeyText);
        }

        this->data->window.draw(chosenItem->getSprite());
        this->data->window.draw(titleInventar);
        this->data->window.draw(arrowLeft);
        this->data->window.draw(arrowRight);
    }

    if(menuSite == 2){
        this->data->window.draw(titleMap);

        if(haveDungeonMap){
            this->data->window.draw(map);
            for (int i = 0; i < 2; i++) {
                this->data->window.draw(levelLayerSprites[i]);
            }

            this->data->window.draw(chosenLevelLayer);
            this->data->window.draw(eg);
            this->data->window.draw(ug);

        } else{
            this->data->window.draw(noDungeonMap);
        }

        this->data->window.draw(arrowLeft);

    }
}


Inventar::~Inventar() {
    delete chosenItem;
}









