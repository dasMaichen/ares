//
// Created by das_maichen on 15.06.18.
//

#ifndef MYGAME_INVENTAR_H
#define MYGAME_INVENTAR_H


#include "Game.h"
#include "Item.h"

class Inventar {

public:
    Inventar(GameDataRef data);
    ~Inventar();
    void update(sf::Vector2f position, int roomNo);
    void draw();
    void drawActiveItem();
    void chooseItem(sf::Event event);

    void setPosition(sf::Vector2f position){
        inventar.setPosition(position);
    }

    void setChosenItemPosition(sf::Vector2f position){
        chosenItemBox.setPosition(position);
    };

    Item* getChosenItem(){ return chosenItem;};

    void setGetFlintstone(bool picked){
        getFlintstone = picked;
    };

    void setGetCrossbow(bool picked){
        getCrossbow = picked;
    };

    void setGetTalisman(bool picked){
        getTalisman = picked;
    }
    void setGetTorch(bool picked){
        getTorch = picked;
    }

    void setGetKey(bool picked){
        getKey = picked;
    }

    void setKeyCount(int count){this->countKey = count;};
    void setTalismanCount(int count){this->countTalisman = count;};
    void setTorchCount(int count){this->countTorch = count;};

    int getKeyCount(){ return this->countKey;};
    int getTalismanCount(){ return this->countTalisman;};
    int getTorchCount(){ return this->countTorch;};

    bool getToMainMenu(){ return toMainMenu;};

    void setHaveDungeonMap(bool dungeonMap){this->haveDungeonMap = dungeonMap;};

    void setchosenItemName(std:: string name){this->chosenItem->setName(name);};


private:
    GameDataRef data;
    sf::Sprite menuBox;
    sf::Sprite inventar;
    int menuSite;
    sf::Sprite arrowLeft;
    sf::Sprite arrowRight;

    sf::Sprite flintstone;
    sf::Sprite crossbow;
    sf::Sprite talisman;
    sf::Sprite torch;
    sf::Sprite key;
    sf::RectangleShape itemSprites[10];
    sf::RectangleShape chosenItemBox;
    int selectedItemColumn;
    int selectedItemRow;
    bool getFlintstone;
    bool getCrossbow;
    bool getTalisman;
    bool getTorch;
    bool getKey;
    Item* chosenItem;
    sf::Sprite activeItemSprite;
    sf::Text titleInventar;
    bool settingActiveItem;
    int countTalisman;
    int countKey;
    int countTorch;
    sf::Text countTalismanText;
    sf::Text countKeyText;
    sf::Text countTorchText;


    sf::RectangleShape chosenLevelLayer;
    sf::RectangleShape levelLayerSprites[2];
    int selectedLayerColumn;
    int selectedLayerRow;
    sf::Sprite map;
    sf::Text eg;
    sf::Text ug;
    sf::Text titleMap;
    bool haveDungeonMap;
    sf::Text noDungeonMap;

    sf::Text menuTitle;
    sf::RectangleShape chosenOption;
    sf::Text menuText[3];
    int selectedMenuIndexColumn;
    int selectedMenuIndexRow;
    sf::Sprite buttons[3];
    bool toMainMenu;


    void itemMoveRight();
    void itemMoveLeft();
    void itemMoveDown();
    void itemMoveUp();

    void mapMoveDown();
    void mapMoveUp();
    void mapMoveLeft();
    void mapMoveRight();


    void menuMoveDown();
    void menuMoveUp();
    void menuMoveLeft();
    void menuMoveRight();






};


#endif //MYGAME_INVENTAR_H
