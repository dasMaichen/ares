//
// Created by das_maichen on 09.05.18.
//

#ifndef MYGAME_DEFINITIONS_H
#define MYGAME_DEFINITIONS_H

//Konstanten
#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

#define MAX_NUMBER_OF_ITEMS_MAINMENU 4
#define MAX_NUMBER_OF_ITEMS_GAMEOVER 2

#define SPLASH_STATE_SHOW_TIME 2.0

#define ARES_ANIMATION_DURATION 0.4f

#define ARES_SPEED 300.0f

#define ARES_JUMP_HEIGHT 150.0f

#define JUMP_N_RUN_BEGINN_WAITING_TIME 3.0

#define ENEMY_ANIMATION_DURATION 0.4f

#define UNVULNERABLE_DURATION 1.0f

#define MAX_INVENTAR_SIZE 5.0;


enum GameStates
{
    READY,
    PLAYING,
    GAMEOVER,
    LEVEL_1_CLEAR
};

//Erstes Fenster Assets
#define SPLASH_SCENE_BACKGROUND_FILEPATH "Content/title_probe.jpg"

//MainMenu Assests
#define BUTTON_BACKGROUND_FILEPATH "Content/title_button_background2.png"
#define MAIN_MENU_BACKGROUND_FILEPATH "Content/mainmenu.jpg"
#define GAME_FONT_FILEPATH "Content/Fonts/heineken.ttf"
#define MAIN_MENU_ARES_FILEPATH "Content/title_image_ares.png"

//#define MAIN_MENU_BACKGROUND_FILEPATH "Content/wald1.jpg"
#define GAME_TITLE_FILEPATH "Content/title_probe.jpg"
#define PLAY_BUTTON_FILEPATH "Content/play_button_probe.jpg"

//Story Assets
#define STORY1_FILEPATH "Content/story1.jpg"
#define STORY2_FILEPATH "Content/story2.jpg"
#define STORY3_FILEPATH "Content/story3.jpg"
#define STORY4_FILEPATH "Content/story_kap4.jpg"
#define STORY_BOX_FILEPATH "Content/textBox2.png"
#define SPEECH_BOX_FILEPATH "Content/speechBox.png"
#define STORY_NEXT_BUTTON_FILEPATH "Content/next_button.png"


//JumpNRun Assets
#define JUMP_N_RUN_BACKGROUND_FILEPATH "Content/wald2.jpg"
//#define JUMP_N_RUN_BACKGROUND_2_FILEPATH "Content/Texture/close_background.png"
//#define JUMP_N_RUN_BACKGROUND_2_FILEPATH "Content/level1_background_raster.png"
#define JUMP_N_RUN_BACKGROUND_PART1_FILEPATH "Content/level1_background_part1.png"
#define JUMP_N_RUN_BACKGROUND_PART2_FILEPATH "Content/level1_background_part2.png"
//#define JUMP_N_RUN_FOREGROUND_FILEPATH "Content/Texture/foreground.png"
#define JUMP_N_RUN_FOREGROUND_PART1_FILEPATH "Content/level1_foreground_part1.png"
#define JUMP_N_RUN_FOREGROUND_PART2_FILEPATH "Content/level1_foreground_part2.png"
#define JUMP_N_RUN_TREE_FILEPATH "Content/Texture/tree.png"

//#define PLATTFORM_FILEPATH "Content/plattform.png"
//#define PLATTFORM_FILEPATH "Content/Texture/ground.png"
#define PLATTFORM_FILEPATH "Content/ground_dummy.png"
#define OBSTACLE_BRANCH_LEFT_FILEPATH "Content/Texture/ast_links.png"
#define OBSTACLE_BRANCH_RIGHT_FILEPATH "Content/Texture/ast_rechts.png"
#define OBSTACLE_BRANCH_LEFT_SMALL_FILEPATH "Content/Texture/ast_links_klein.png"
#define OBSTACLE_BRANCH_RIGHT_SMALL_FILEPATH "Content/Texture/ast_rechts_klein.png"
#define OBSTACLE_TREETOP_FILEPATH "Content/Texture/treetop.png"
#define OBSTACLE_STONE1_FILEPATH "Content/Texture/stone1.png"
#define OBSTACLE_STONE2_FILEPATH "Content/Texture/stone2.png"
#define OBSTACLE_STONE_SMALL_FILEPATH "Content/Texture/stone_small.png"
#define OBSTACLE_HOLLOW_TREE_FILEPATH "Content/Texture/hollow_tree.png"
#define OBSTACLE_FLOWER_FILEPATH "Content/Texture/flower.png"

#define ENEMIES_DEER_FILEPATH "Content/deer.png"

#define OBSTACLE_STONE_FILEPATH "Content/stein.png"
#define ANIMAL_FILEPATH "Content/images2.png"
#define ARES_ANIMATION_FILEPATH "Content/ares_animation_farbe.png"

#define HEART_FILEPATH "Content/heart.png"

#define ARES_STANDING_FILEPATH "Content/Ares_pixelart_standing.png"

#define ENEMY_FRAME_1_FILEPATH "Content/Texture/Hirsch/deer1_right.png"
#define ENEMY_FRAME_2_FILEPATH "Content/Texture/Hirsch/deer2_right.png"
#define ENEMY_FRAME_3_FILEPATH "Content/Texture/Hirsch/deer3_right.png"
#define ENEMY_FRAME_4_FILEPATH "Content/Texture/Hirsch/deer4_right.png"
#define ENEMY_FRAME_5_FILEPATH "Content/Texture/Hirsch/deer5_right.png"
#define ENEMY_FRAME_6_FILEPATH "Content/Texture/Hirsch/deer6_right.png"
#define ENEMY_FRAME_7_FILEPATH "Content/Texture/Hirsch/deer7_right.png"
#define ENEMY_FRAME_8_FILEPATH "Content/Texture/Hirsch/deer8_right.png"
#define ENEMY_FRAME_9_FILEPATH "Content/Texture/Hirsch/deer9_right.png"


//GameOverState
#define GAME_OVER_BACKGROUND_FILEPATH "Content/game_over_background.png"


//Adventure
#define ADVENTURE_ARES_ANIMATION "Content/ares_pixel_animation_probe.png"
#define ADVENTURE_OBJECT "Content/heart.png"


#endif //MYGAME_DEFINITIONS_H
