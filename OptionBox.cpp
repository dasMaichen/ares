//
// Created by das_maichen on 13.07.18.
//

#include "OptionBox.h"
#include "DEFINITIONS.h"

OptionBox::OptionBox(GameDataRef data, sf::View& view ) : data(data) {

    returnToMenu = false;

    selectedBoxIndex = 0;
    chosenDifficulityState = 0;
    chosenMusicState = 0;
    chosenSoundState = 0;
    selectedOptionIndex = 0;

    bufferChangeSettings.loadFromFile("Content/Sounds/change-setting.wav");
    soundChangeSettings.setBuffer(bufferChangeSettings);
    bufferNavigate.loadFromFile("Content/Sounds/options.wav");
    soundNavigate.setBuffer(bufferNavigate);

    this->data->assets.loadFont("GameFont", GAME_FONT_FILEPATH);
    this->background.setSize(sf::Vector2f(1200,600));
    background.setPosition(-150,0);
    //background.setOrigin(view.getCenter());
    background.setFillColor(sf::Color(0,0,0,180));

    //optionBox.setSize(sf::Vector2f(400,370));
    //optionBox.setFillColor(sf::Color::Blue);
    this->data->assets.loadTexture("OptionBox", "Content/optionBox.png");
    optionBox.setTexture(this->data->assets.getTexture("OptionBox"));
    optionBox.setScale(0.35,0.35);
    optionBox.setPosition(190,200);

    title.setFont(this->data->assets.getFont("GameFont"));
    title.setColor(sf::Color::Cyan);
    title.setString("Settings");
    title.setCharacterSize(40);
    title.setPosition(320, 220);

    sf::Vector2f optionElementsSize = sf::Vector2f(360,60);
    sf::Vector2f buttonSize = sf::Vector2f(150,30);

    difficultyBox.setSize(optionElementsSize);
    difficultyBox.setPosition(220, 280);
    difficultyBox.setFillColor(sf::Color(128,128,128,255));
    //difficultyBox.setOutlineThickness(3);
    //difficultyBox.setOutlineColor(sf::Color::Black);

    titleDifficulty.setFont(this->data->assets.getFont("GameFont"));
    titleDifficulty.setString("Difficulty:");
    titleDifficulty.setColor(sf::Color::White);
    titleDifficulty.setPosition(230, 280);
    titleDifficulty.setCharacterSize(20);


    difficultyBoxText[0].setFont(this->data->assets.getFont("GameFont"));
    difficultyBoxText[0].setColor(sf::Color::Cyan);
    difficultyBoxText[0].setString("Easy");
    difficultyBoxText[0].setPosition(250,300);

    difficultyBoxText[1].setFont(this->data->assets.getFont("GameFont"));
    difficultyBoxText[1].setColor(sf::Color::White);
    difficultyBoxText[1].setString("Normal");
    difficultyBoxText[1].setPosition(340,300);

    difficultyBoxText[2].setFont(this->data->assets.getFont("GameFont"));
    difficultyBoxText[2].setColor(sf::Color::White);
    difficultyBoxText[2].setString("Hard");
    difficultyBoxText[2].setPosition(470,300);

    musicBox.setSize(optionElementsSize);
    musicBox.setPosition(220, 360);
    musicBox.setFillColor(sf::Color(128,128,128,128));

    titleMusic.setFont(this->data->assets.getFont("GameFont"));
    titleMusic.setString("Music:");
    titleMusic.setColor(sf::Color::White);
    titleMusic.setPosition(230, 360);
    titleMusic.setCharacterSize(20);

    musicBoxText[0].setFont(this->data->assets.getFont("GameFont"));
    musicBoxText[0].setColor(sf::Color::Cyan);
    musicBoxText[0].setString("On");
    musicBoxText[0].setPosition(350,380);

    musicBoxText[1].setFont(this->data->assets.getFont("GameFont"));
    musicBoxText[1].setColor(sf::Color::White);
    musicBoxText[1].setString("Off");
    musicBoxText[1].setPosition(400,380);

    soundBox.setSize(optionElementsSize);
    soundBox.setPosition(220,440);
    soundBox.setFillColor(sf::Color(128,128,128,128));

    titleSound.setFont(this->data->assets.getFont("GameFont"));
    titleSound.setString("Sound:");
    titleSound.setColor(sf::Color::White);
    titleSound.setPosition(230, 440);
    titleSound.setCharacterSize(20);

    soundBoxText[0].setFont(this->data->assets.getFont("GameFont"));
    soundBoxText[0].setColor(sf::Color::Cyan);
    soundBoxText[0].setString("On");
    soundBoxText[0].setPosition(350,460);

    soundBoxText[1].setFont(this->data->assets.getFont("GameFont"));
    soundBoxText[1].setColor(sf::Color::White);
    soundBoxText[1].setString("Off");
    soundBoxText[1].setPosition(400,460);


    cancelButton.setSize(buttonSize);
    cancelButton.setPosition(325, 520);
    cancelButton.setFillColor(sf::Color(255,255,255,128));
    titleCancel.setFont(this->data->assets.getFont("GameFont"));
    titleCancel.setCharacterSize(20);
    titleCancel.setColor(sf::Color::Cyan);
    titleCancel.setString("Back");
    titleCancel.setPosition(380, 520);
}

void OptionBox::draw() {
    this->data->window.draw(background);
    this->data->window.draw(optionBox);
    this->data->window.draw(difficultyBox);
    this->data->window.draw(musicBox);
    this->data->window.draw(soundBox);
    this->data->window.draw(cancelButton);

    this->data->window.draw(titleCancel);
    //this->data->window.draw(saveButton);

    this->data->window.draw(titleDifficulty);
    for(int i = 0; i<3; i++){
        this->data->window.draw(this->difficultyBoxText[i]);
    }

    this->data->window.draw(titleMusic);
    for(int i = 0; i<2; i++){
        this->data->window.draw(this->musicBoxText[i]);
    }

    this->data->window.draw(titleSound);
    for(int i = 0; i<2; i++){
        this->data->window.draw(this->soundBoxText[i]);
    }


    this->data->window.draw(title);
}

void OptionBox::moveUp() {
    if(selectedBoxIndex -1 >= 0){
        if(chosenSoundState == 0){
            soundNavigate.play();
        }

        //menuText[selectedItemIndex].setColor(sf::Color::White);
        selectedBoxIndex--;
        //menuText[selectedItemIndex].setColor(sf::Color::Cyan);
    }

    if(selectedBoxIndex == 0){
        titleDifficulty.setColor(sf::Color::Cyan);
        titleMusic.setColor(sf::Color::White);
        titleSound.setColor(sf::Color::White);
        difficultyBox.setFillColor(sf::Color(128,128,128,255));
        musicBox.setFillColor(sf::Color(128,128,128,128));
        soundBox.setFillColor(sf::Color(128,128,128,128));

        cancelButton.setFillColor(sf::Color(255,255,255,128));
    }

    if(selectedBoxIndex == 1){
        titleDifficulty.setColor(sf::Color::White);
        titleMusic.setColor(sf::Color::Cyan);
        titleSound.setColor(sf::Color::White);
        difficultyBox.setFillColor(sf::Color(128,128,128,128));
        musicBox.setFillColor(sf::Color(128,128,128, 255));
        soundBox.setFillColor(sf::Color(128,128,128,128));

        cancelButton.setFillColor(sf::Color(255,255,255,128));
    }

    if(selectedBoxIndex == 2){
        titleDifficulty.setColor(sf::Color::White);
        titleMusic.setColor(sf::Color::White);
        titleSound.setColor(sf::Color::Cyan);
        difficultyBox.setFillColor(sf::Color(128,128,128,128));
        musicBox.setFillColor(sf::Color(128,128,128,128));
        soundBox.setFillColor(sf::Color(128,128,128,255));

        cancelButton.setFillColor(sf::Color(255,255,255,128));
    }

    returnToMenu = false;

    if(selectedBoxIndex == 3){
        titleDifficulty.setColor(sf::Color::White);
        titleMusic.setColor(sf::Color::White);
        titleSound.setColor(sf::Color::White);
        difficultyBox.setFillColor(sf::Color(128,128,128,128));
        musicBox.setFillColor(sf::Color(128,128,128,128));
        soundBox.setFillColor(sf::Color(128,128,128,128));

        cancelButton.setFillColor(sf::Color(255,255,255,255));
        //titleCancel.setColor(sf::Color::Cyan);
        returnToMenu = true;
    }
}

void OptionBox::moveDown() {
    if(selectedBoxIndex + 1 < 4){
        if(chosenSoundState == 0){
            soundNavigate.play();
        }

        //menuText[selectedItemIndex].setColor(sf::Color::White);
        selectedBoxIndex ++;
        //menuText[selectedItemIndex].setColor(sf::Color::Cyan);
    }

    if(selectedBoxIndex == 0){
        titleDifficulty.setColor(sf::Color::Cyan);
        titleMusic.setColor(sf::Color::White);
        titleSound.setColor(sf::Color::White);
        difficultyBox.setFillColor(sf::Color(128,128,128,255));
        musicBox.setFillColor(sf::Color(128,128,128,128));
        soundBox.setFillColor(sf::Color(128,128,128,128));

        cancelButton.setFillColor(sf::Color(255,255,255,128));
    }

    if(selectedBoxIndex == 1){
        titleDifficulty.setColor(sf::Color::White);
        titleMusic.setColor(sf::Color::Cyan);
        titleSound.setColor(sf::Color::White);
        difficultyBox.setFillColor(sf::Color(128,128,128,128));
        musicBox.setFillColor(sf::Color(128,128,128,255));
        soundBox.setFillColor(sf::Color(128,128,128,128));

        cancelButton.setFillColor(sf::Color(255,255,255,128));
    }

    if(selectedBoxIndex == 2){
        titleDifficulty.setColor(sf::Color::White);
        titleMusic.setColor(sf::Color::White);
        titleSound.setColor(sf::Color::Cyan);
        difficultyBox.setFillColor(sf::Color(128,128,128,128));
        musicBox.setFillColor(sf::Color(128,128,128,128));
        soundBox.setFillColor(sf::Color(128,128,128,255));

        cancelButton.setFillColor(sf::Color(255,255,255,128));
    }

    returnToMenu = false;

    if(selectedBoxIndex == 3){
        titleDifficulty.setColor(sf::Color::White);
        titleMusic.setColor(sf::Color::White);
        titleSound.setColor(sf::Color::White);
        difficultyBox.setFillColor(sf::Color(128,128,128,128));
        musicBox.setFillColor(sf::Color(128,128,128,128));
        soundBox.setFillColor(sf::Color(128,128,128,128));

        cancelButton.setFillColor(sf::Color(255,255,255,255));
        //titleCancel.setColor(sf::Color::Cyan);
        returnToMenu = true;
    }
}

void OptionBox::moveRight() {
    if(selectedBoxIndex == 0){
        maxOptions = 3;
    } else if(selectedBoxIndex == 1 || selectedBoxIndex == 2){
        maxOptions = 2;
    }

    if(selectedOptionIndex + 1 < maxOptions){
        if(chosenSoundState == 0){
            soundChangeSettings.play();
        }
        if(selectedBoxIndex == 0){
            difficultyBoxText[selectedOptionIndex].setColor(sf::Color::White);
            selectedOptionIndex ++;
            chosenDifficulityState = selectedOptionIndex;
            difficultyBoxText[selectedOptionIndex].setColor(sf::Color::Cyan);
        }
        if(selectedBoxIndex == 1){
            musicBoxText[selectedOptionIndex].setColor(sf::Color::White);
            selectedOptionIndex ++;
            chosenMusicState = selectedOptionIndex;
            musicBoxText[selectedOptionIndex].setColor(sf::Color::Cyan);
        }
        if(selectedBoxIndex == 2){
            soundBoxText[selectedOptionIndex].setColor(sf::Color::White);
            selectedOptionIndex ++;
            chosenSoundState = selectedOptionIndex;
            soundBoxText[selectedOptionIndex].setColor(sf::Color::Cyan);
        }
        //selectedBoxIndex ++;
        //menuText[selectedItemIndex].setColor(sf::Color::Cyan);
    }
}

void OptionBox::moveLeft() {
    if(selectedOptionIndex - 1 >= 0){
        if(chosenSoundState == 0){
            soundChangeSettings.play();
        }
        if(selectedBoxIndex == 0){
            difficultyBoxText[selectedOptionIndex].setColor(sf::Color::White);
            selectedOptionIndex --;
            chosenDifficulityState = selectedOptionIndex;
            difficultyBoxText[selectedOptionIndex].setColor(sf::Color::Cyan);
        }
        if(selectedBoxIndex == 1){
            musicBoxText[selectedOptionIndex].setColor(sf::Color::White);
            selectedOptionIndex --;
            chosenMusicState = selectedOptionIndex;
            musicBoxText[selectedOptionIndex].setColor(sf::Color::Cyan);
        }
        if(selectedBoxIndex == 2){
            soundBoxText[selectedOptionIndex].setColor(sf::Color::White);
            selectedOptionIndex --;
            chosenSoundState = selectedOptionIndex;
            soundBoxText[selectedOptionIndex].setColor(sf::Color::Cyan);
        }
        //selectedBoxIndex ++;
        //menuText[selectedItemIndex].setColor(sf::Color::Cyan);
    }


}

int OptionBox::getChosenOptionIndex() {
    if(selectedBoxIndex == 0){
        return chosenDifficulityState;
    }
    if(selectedBoxIndex == 1){
        return chosenMusicState;
    }
    if(selectedBoxIndex == 2){
        return chosenSoundState;
    }
}

void OptionBox::setSelectedBoxIndexToBeginn() {
    selectedBoxIndex = 0;

    titleDifficulty.setColor(sf::Color::Cyan);
    titleMusic.setColor(sf::Color::White);
    titleSound.setColor(sf::Color::White);
    difficultyBox.setFillColor(sf::Color(128,128,128,255));
    musicBox.setFillColor(sf::Color(128,128,128,128));
    soundBox.setFillColor(sf::Color(128,128,128,128));

    cancelButton.setFillColor(sf::Color(255,255,255,128));

}

