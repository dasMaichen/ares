//
// Created by das_maichen on 13.07.18.
//

#ifndef MYGAME_OPTIONBOX_H
#define MYGAME_OPTIONBOX_H


#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Sound.hpp>
#include "Game.h"

class OptionBox {

public:
    OptionBox(GameDataRef data, sf::View& view);
    ~OptionBox();

    void draw();
    void setSelectedOptionIndex(int index){selectedOptionIndex = index;};
    int getChosenOptionIndex();
    bool getReturnTo(){ return returnToMenu;};
    void setSelectedBoxIndexToBeginn();

    int getDifficultyLevelState(){ return chosenDifficulityState;};
    int getMusicState(){ return chosenMusicState;};
    int getSoundState(){ return chosenSoundState;};

public:
    void moveUp();
    void moveDown();
    void moveLeft();
    void moveRight();

private:
    GameDataRef data;

    sf::RectangleShape background;
    sf::Sprite optionBox;
    //sf::RectangleShape optionBox;

    sf::RectangleShape musicBox;
    sf::RectangleShape soundBox;
    sf::RectangleShape difficultyBox;

    sf::RectangleShape cancelButton;
    sf::Text titleCancel;


    sf::Text titleDifficulty;
    sf::Text titleMusic;
    sf::Text titleSound;

    sf::Text difficultyBoxText[3];
    sf::Text musicBoxText[2];
    sf::Text soundBoxText[2];

    sf::Text title;
    int selectedBoxIndex;
    int selectedOptionIndex;
    int maxOptions;

    int chosenDifficulityState;
    int chosenMusicState;
    int chosenSoundState;

    sf::SoundBuffer bufferChangeSettings;
    sf::Sound soundChangeSettings;

    sf::SoundBuffer bufferNavigate;
    sf::Sound soundNavigate;

    bool returnToMenu;

};


#endif //MYGAME_OPTIONBOX_H
