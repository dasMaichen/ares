//
// Created by das_maichen on 12.05.18.
//

#ifndef MYGAME_PLATTFORMS_H
#define MYGAME_PLATTFORMS_H


#include "Game.h"
#include "Collider.h"


class Plattforms {
public:
    Plattforms(GameDataRef data);

    void drawPlattforms();
    void setPlattforms(float width, float height);
    //void movePlattforms(float dt);
    std::vector<sf::Sprite>& getSprites(){ return plattformSprite;};

    void loadPlattformPosition(std::string filePath);

    Collider getCollider(int i){Collider(plattformBody.at(i));};

    void setDebugModus(bool modus){
        debugModus = modus;
    };


    //sf::RectangleShape& getBody(){ return body;};
    //Collider getCollider(){ return Collider(body);};

    std::vector<sf::RectangleShape> plattformBody;


private:
    GameDataRef data;
    //sf::Sprite plattformSprite;
    std::vector<sf::Sprite> plattformSprite;
    bool debugModus;

    //sf::Vector2f plattformPosition;
   // sf::RectangleShape body;

};



#endif //MYGAME_PLATTFORMS_H
