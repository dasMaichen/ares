//
// Created by das_maichen on 10.04.18.
//

#include "Animation.h"

//Animation::Animation(const std::string &animationTexture, sf::Vector2u imageCount, float switchTime) {

    Animation::Animation(sf::Texture animationTexture, sf::Vector2u imageCount, float switchTime) : texture(animationTexture) {

   // texture.loadFromFile(animationTexture);

    this->imageCount = imageCount;
    this->switchTime = switchTime;

    totalTime = 0.0f;
    currentImage.x = 0;

    uvRect.width = texture.getSize().x / float(imageCount.x);
    uvRect.height = texture.getSize().y / float(imageCount.y);
}

void Animation::update(int row, float deltaTime, bool faceRight) {
    currentImage.y = row;
    totalTime += deltaTime;

    if(row == 0){
        currentImage.x = 0.0f;
    }

    if(totalTime >= switchTime){
        totalTime -= switchTime;
        currentImage.x ++;

        if(currentImage.x >= imageCount.x){
            currentImage.x = 0;
        }
    }


    uvRect.top = currentImage.y * uvRect.height;

    if(faceRight){
        uvRect.left = currentImage.x * uvRect.width;
        uvRect.width= abs(uvRect.width);
    }else {
        uvRect.left = (currentImage.x +1) * abs(uvRect.width);
        uvRect.width = -abs(uvRect.width);
    }

}

Animation::~Animation() {

}
