//
// Created by das_maichen on 23.05.18.
//

#ifndef MYGAME_ENEMY_H
#define MYGAME_ENEMY_H


#include "Game.h"

class Enemy {

public:
    Enemy(GameDataRef data, std::string type, sf::Vector2f position, sf::Vector2f startAtPosition, sf::Vector2f speed);
    ~Enemy();

    //void inizialize(std::string name, std::string type, sf::Vector2f position, sf::Vector2f startAtPosition, sf::Vector2f speed);
    void draw();
    void update(float dt, sf::Vector2f viewPosition);

    void animate(float deltaTime);
    sf::RectangleShape& getCollisionBox(){ return collisionBox;};
    void setDebugModus(bool modus){this->debugModus = modus;};

    void startingMovement(sf::Vector2f viewPosition);
    bool getInGame(){ return moving;};
    std::string getType(){ return this->type;};

    void setPosition(sf::Vector2f position){this->enemySprite.setPosition(position);};

private:
    GameDataRef data;

    bool debugModus;

    sf::Sprite enemySprite;
    std::vector<sf::Texture> animationFrames;
    sf::RectangleShape collisionBox;
    unsigned int animationIterator;

    sf::Clock clock;

    std::string name;
    std::string type;
    sf::Vector2f position;
    sf::Vector2f startAtPosition;
    sf::Vector2f speed;
    bool moving;
};


#endif //MYGAME_ENEMY_H
