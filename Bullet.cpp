//
// Created by das_maichen on 07.06.18.
//

#include "Bullet.h"

Bullet::Bullet(GameDataRef data) : data(data){
}

void Bullet::inizialize(sf::Vector2f startposition, sf::Vector2f direction) {
    //texture = this->data->assets.getTexture("Arrow");
    //core.setTexture(&texture);
    //this->startposition = sf::Vector2f(startposition.x-50, startposition.y - 50);
    this->direction = direction;



    core.setFillColor(sf::Color::White);
    //core.move(direction);


    /*
    if(direction == sf::Vector2f(1,0)){
        texture = this->data->assets.getTexture("ArrowRight");
        this->startposition = sf::Vector2f(startposition.x, startposition.y - 50);
    } else if(direction == sf::Vector2f(-1,0)){
        texture = this->data->assets.getTexture("ArrowLeft");
        this->startposition = sf::Vector2f(startposition.x-50, startposition.y - 50);
    } else if(direction == sf::Vector2f(0,1)){
        texture = this->data->assets.getTexture("ArrowDown");
        this->startposition = sf::Vector2f(startposition.x-25, startposition.y);
    } else if (direction == sf::Vector2f(0,-1)){
        texture = this->data->assets.getTexture("ArrowUp");
        this->startposition = sf::Vector2f(startposition.x-25, startposition.y - 100);
    }
     */

    if(direction == sf::Vector2f(1,0)){
        texture = this->data->assets.getTexture("ArrowRight");
        this->startposition = sf::Vector2f(startposition.x+50, startposition.y);
    } else if(direction == sf::Vector2f(-1,0)){
        texture = this->data->assets.getTexture("ArrowLeft");
        this->startposition = sf::Vector2f(startposition.x-50, startposition.y);
    } else if(direction == sf::Vector2f(0,1)){
        texture = this->data->assets.getTexture("ArrowDown");
        this->startposition = sf::Vector2f(startposition.x, startposition.y+50);
    } else if (direction == sf::Vector2f(0,-1)){
        texture = this->data->assets.getTexture("ArrowUp");
        this->startposition = sf::Vector2f(startposition.x, startposition.y-50);
    }

    sprite.setPosition(this->startposition);
    core.setPosition(this->startposition);
    //core.setTexture(&texture);
    sprite.setTexture(texture);

    //sprite.move(direction);
}

Bullet::~Bullet() {

}

void Bullet::draw() {
    this->data->window.draw(sprite);
    this->data->window.draw(core);

}

void Bullet::update() {
    sprite.move(direction * 5.0f);
    core.move(direction * 5.0f);


}

void Bullet::calculateNextPosition() {
    if(direction == sf::Vector2f(0,-1)){
        nextPosition = (sf::Vector2f(actualPosition.x, actualPosition.y - 50));
    } else if(direction == sf::Vector2f(0,1)){
        nextPosition = (sf::Vector2f(actualPosition.x, actualPosition.y + 50));
    } else if(direction == sf::Vector2f(1,0)){
        nextPosition = (sf::Vector2f(actualPosition.x - 50, actualPosition.y));
    } else if(direction == sf::Vector2f(-1,0)){
        nextPosition = (sf::Vector2f(actualPosition.x + 50, actualPosition.y));
    }

}




