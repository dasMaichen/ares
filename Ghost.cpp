//
// Created by das_maichen on 07.08.18.
//

#include "Ghost.h"

Ghost::Ghost(GameDataRef data, sf::Texture &ghostTexture, sf::Vector2u imageCount, float switchTime, float speed, int roomNo, sf::Vector2f position, bool exist) :
        animation(ghostTexture,imageCount,switchTime), data(data) {

    debugModus = false;
    animating = false;
    this->exist = exist;

    this->roomNo = roomNo;

    row = 0;
    faceRight = true;

    texture = ghostTexture;

    collisionsBox.setSize(sf::Vector2f(50.0f, 50.0f));
    collisionsBox.setOrigin(collisionsBox.getSize()/2.0f);
    //collisionsBox.setPosition(1950 - 25, 1800 +25);
    collisionsBox.setPosition(position.x - 25, position.y +25);
    collisionsBox.setFillColor(sf::Color(255,0,0,128));

    //body.setSize(sf::Vector2f(100.0f, 100.0f));
    body.setSize(sf::Vector2f(70,150));
    body.setOrigin(body.getSize().x/2.0f, body.getSize().y - 25);
    body.setPosition(collisionsBox.getPosition());
    body.setTexture(&texture);

    // position = sf::Vector2f(collisionsBox.getPosition());

    direction = sf::Vector2f(0,1);
    targetPosition = collisionsBox.getPosition();
    actualPosition = collisionsBox.getPosition();
    nextPosition = collisionsBox.getPosition();




}

Ghost::~Ghost()
{

}

void Ghost::update(float deltaTime) {


    sf::Vector2f movement(0.0f, 0.0f);

    int deltaX = collisionsBox.getPosition().x - targetPosition.x;
    int deltaY = collisionsBox.getPosition().y - targetPosition.y;

    if(deltaX > 0){
        collisionsBox.move(-2,0);
        movement = sf::Vector2f(-1,0);
        animating = true;
    } else if(deltaX < 0){
        collisionsBox.move(2,0);
        movement = sf::Vector2f(1,0);
        animating = true;
    } else if(deltaY > 0){
        collisionsBox.move(0,-2);
        movement = sf::Vector2f(0,-1);
        animating = true;
    } else if(deltaY < 0){
        collisionsBox.move(0,2);
        movement = sf::Vector2f(0,1);
        animating = true;
    } else{
        collisionsBox.move(0,0);
        movement.x = (0,0);
        movement.y = (0,0);
    }

    if (movement.x == 0 && movement.y == 0 && direction.x == 0 && direction.y > 0) {
        row = 0;
    } else if(movement.x == 0 && movement.y == 0 && direction.x != 0 && direction.y == 0) {
        row = 0;
        if(direction.x < 0){
            faceRight = true;
        } else{
            faceRight = false;
        }
    } else if(movement.x == 0 && movement.y == 0 && direction.x == 0 && direction.y < 0) {
        row = 0;


    } else if (movement.x != 0 && movement.y == 0) {
        faceRight = true;
        row = 0;

        if (movement.x < 0.0f) {
            faceRight = false;
        } else {
            faceRight = true;
        }
    } else if (movement.x == 0 && movement.y != 0) {
        if(movement.y > 0){
            row = 0;
        } else{
            row = 0;
        }

    }


    if (collisionsBox.getPosition().x == targetPosition.x && collisionsBox.getPosition().y == targetPosition.y) {

        collisionsBox.move(0, 0);
        actualPosition = targetPosition;
        animating = false;
    }


    animation.update(row, deltaTime,faceRight);
    body.setTextureRect(animation.uvRect);
    //body.move(2,0);


    //collisionsBox.move(2,0);
    body.setPosition(collisionsBox.getPosition());

}



void Ghost::draw(sf::RenderWindow& window)
{
    if(debugModus){
        window.draw(collisionsBox);
    }
    window.draw(body);
    //window.draw(collisionsBox);
    /*
    if(inventar.size() < 0) {
        window.draw(activeItem->getSprite());
    }
     */
}

void Ghost::calculateNextPosition(sf::Event event) {

    if (event.type == sf::Event::KeyPressed) {
        switch (event.key.code) {
            case sf::Keyboard::Up:
                direction = sf::Vector2f(0, 1);
                nextPosition = (
                        sf::Vector2f(actualPosition.x, actualPosition.y + 50));

                //std::cout << "up!" << std::endl;
                break;
            case sf::Keyboard::Down:
                nextPosition = (sf::Vector2f(actualPosition.x, actualPosition.y - 50));
                direction = sf::Vector2f(0,-1);
                //std::cout << "down!" << std::endl;
                break;
            case sf::Keyboard::Right:
                nextPosition = (sf::Vector2f(actualPosition.x - 50, actualPosition.y));
                direction = sf::Vector2f(1,0);
                //std::cout << "right!" << std::endl;
                break;
            case sf::Keyboard::Left:
                nextPosition = (sf::Vector2f(actualPosition.x + 50, actualPosition.y));
                direction = sf::Vector2f(-1,0);
                //std::cout << "left!" << std::endl;
                break;
        }
    }
}

