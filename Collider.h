//
// Created by das_maichen on 12.04.18.
//

#ifndef MYGAME_COLLIDER_H
#define MYGAME_COLLIDER_H


#include <SFML/Graphics/RectangleShape.hpp>

class Collider {
public:
    Collider(sf::RectangleShape& body);
    ~Collider();

    //JumpNRun
    bool checkCollision(Collider& other, sf::Vector2f& direction, float push);

    //RPG Teil
    bool checkCollisionAdventure(Collider& other, float push);

    void move(float dx, float dy){
        body.move(dx,dy);
    };

    sf::Vector2f getPosition(){
        return body.getPosition();
    };

    sf::Vector2f getHalfSize(){
        float x = body.getGlobalBounds().width/2;
        float y = body.getGlobalBounds().height/2;
        return sf::Vector2f(x,y);

        //return body.getSize()/2.0f;
    }



private:
    sf::RectangleShape& body;

};


#endif //MYGAME_COLLIDER_H
