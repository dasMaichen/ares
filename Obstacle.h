//
// Created by das_maichen on 16.05.18.
//

#ifndef MYGAME_OBSTACLE_H
#define MYGAME_OBSTACLE_H


#include "Game.h"
#include "Collider.h"

class Obstacle {
public:
    Obstacle(GameDataRef data);
    ~Obstacle(){};

    void drawObstacle();
    void setObstacle(float width, float height, std::string spriteName);
    std::vector<sf::Sprite>& getSprites(){ return obstacleSprites;};
    void loadObstaclePosition(std::string filePath);

    Collider getCollider(int i){Collider(obstacleBody.at(i));};

    std::vector<sf::RectangleShape> obstacleBody;
    void setDebugModus(bool modus){
        debugModus = modus;
    };


private:
    GameDataRef data;
    bool debugModus;
    std::vector<sf::Sprite> obstacleSprites;

};


#endif //MYGAME_OBSTACLE_H
