//
// Created by das_maichen on 23.05.18.
//

#include "Enemy.h"
#include "DEFINITIONS.h"

Enemy::Enemy(GameDataRef data, std::string type, sf::Vector2f position, sf::Vector2f startAtPosition, sf::Vector2f speed) : data(data) {
    animationIterator = 0;

    debugModus = false;
    moving = false;

    this->startAtPosition = startAtPosition;
    this->speed = speed;
    this->type = type;

    if(type == "deer") {

        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame1"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame2"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame3"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame4"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame5"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame6"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame7"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame8"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame9"));
    }

    if(type == "deer2"){
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeer2Frame1"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeer2Frame2"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeer2Frame3"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeer2Frame4"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeer2Frame5"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeer2Frame6"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeer2Frame7"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeer2Frame8"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeer2Frame9"));
    }

    if(type == "butterfly"){
        animationFrames.push_back(this->data->assets.getTexture("Butterfly1"));
        animationFrames.push_back(this->data->assets.getTexture("Butterfly2"));
        animationFrames.push_back(this->data->assets.getTexture("Butterfly3"));
    }

    enemySprite.setTexture(animationFrames.at(animationIterator));
    //enemySprite.setScale(0.3,0.3);

    sf::Vector2f size;
    size.x = enemySprite.getGlobalBounds().width;
    size.y = enemySprite.getGlobalBounds().height;

    enemySprite.setOrigin(size/2.0f);
    //enemySprite.setPosition(3000,430);
    enemySprite.setPosition(position);

    if(type != "butterfly"){
        collisionBox.setFillColor(sf::Color(0,255,255,128));
        collisionBox.setSize(sf::Vector2f(enemySprite.getGlobalBounds().width/2,enemySprite.getGlobalBounds().height/2 - 25));
        collisionBox.setOrigin(collisionBox.getSize()/2.0f);
        collisionBox.setPosition(enemySprite.getPosition());
    }

    if(type == "butterfly"){
        collisionBox.setFillColor(sf::Color(0,255,255,128));
        collisionBox.setSize(sf::Vector2f(enemySprite.getGlobalBounds().width/4,enemySprite.getGlobalBounds().height));
        collisionBox.setOrigin(collisionBox.getSize()/2.0f);
        collisionBox.setPosition(enemySprite.getPosition());
    }


}

Enemy::~Enemy() {
}


/*
void Enemy::inizialize(std::string name, std::string type, sf::Vector2f position, sf::Vector2f startAtPosition,
                       sf::Vector2f speed) {
    this->name = name;
    this->type = type;

    this->position = position;
    this->startAtPosition = startAtPosition;
    this->speed = speed;

    if(type == "deer"){
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame1"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame2"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame3"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame4"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame5"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame6"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame7"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame8"));
        animationFrames.push_back(this->data->assets.getTexture("EnemyDeerFrame9"));
    }

    enemySprite.setTexture(animationFrames.at(animationIterator));
    sf::Vector2f size;
    size.x = enemySprite.getGlobalBounds().width;
    size.y = enemySprite.getGlobalBounds().height;

    enemySprite.setOrigin(size/2.0f);
    enemySprite.setPosition(position);

    collisionBox.setFillColor(sf::Color(0,255,255));
    //collisionBox.setSize(sf::Vector2f(enemySprite.getGlobalBounds().width/2,enemySprite.getGlobalBounds().height/2));
    collisionBox.setOrigin(collisionBox.getSize()/2.0f);
    collisionBox.setPosition(enemySprite.getPosition());


}
 */


void Enemy::draw()
{
    if(moving){
        data->window.draw(enemySprite);
        if(debugModus){
            data->window.draw(collisionBox);

        }
    }


}

void Enemy::animate(float dt) {
    if (clock.getElapsedTime().asSeconds() > ENEMY_ANIMATION_DURATION / animationFrames.size()) {
        if (animationIterator < animationFrames.size() - 1) {
            animationIterator++;
        } else {
            animationIterator = 0;
        }

        enemySprite.setTexture(animationFrames.at(animationIterator));

        clock.restart();
    }
}

void Enemy::update(float dt, sf::Vector2f viewPosition) {
    //enemySprite.move(speed);
    startingMovement(viewPosition);
    if(moving){
        enemySprite.move(speed);
        collisionBox.setPosition(enemySprite.getPosition());
        if(this->type == "butterfly"){
            collisionBox.setPosition(enemySprite.getPosition().x - 100, enemySprite.getPosition().y);
        }



        animate(dt);
    }
}

void Enemy::startingMovement(sf::Vector2f viewPosition) {
    if(viewPosition.x > startAtPosition.x){
        moving = true;
    }
}
