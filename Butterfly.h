//
// Created by das_maichen on 21.08.18.
//

#ifndef MYGAME_BUTTERFLY_H
#define MYGAME_BUTTERFLY_H


#include "Game.h"

class Butterfly {

public:
    Butterfly(GameDataRef data, std::string type, sf::Vector2f position, sf::Vector2f startAtPosition, sf::Vector2f speed);
    ~Butterfly();

    //void inizialize(std::string name, std::string type, sf::Vector2f position, sf::Vector2f startAtPosition, sf::Vector2f speed);
    void draw();
    void update(float dt, sf::Vector2f viewPosition);

    void animate(float deltaTime);

    void setDebugModus(bool modus){this->debugModus = modus;};

    void startingMovement(sf::Vector2f viewPosition);
    bool getInGame(){ return moving;};

private:
    GameDataRef data;

    bool debugModus;

    sf::Sprite enemySprite;
    std::vector<sf::Texture> animationFrames;
    sf::RectangleShape collisionBox;
    unsigned int animationIterator;

    sf::Clock clock;

    std::string name;
    std::string type;
    sf::Vector2f position;
    sf::Vector2f startAtPosition;
    sf::Vector2f speed;
    bool moving;
};



#endif //MYGAME_BUTTERFLY_H
