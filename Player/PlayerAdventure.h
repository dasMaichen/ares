//
// Created by das_maichen on 18.04.18.
//

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "../Collider.h"
#include "../Animation.h"
#include "../Item.h"

#ifndef MYGAME_PLAYERADVENTURE_H
#define MYGAME_PLAYERADVENTURE_H


class PlayerAdventure {


public:
    PlayerAdventure(GameDataRef data, sf::Texture &playerTexture, sf::Vector2u imageCount, float switchTime, float speed);
    ~PlayerAdventure();

    void update(float deltaTime);
    void calculateNextPosition(sf::Event event);
    void draw(sf::RenderWindow& window);

    sf::Vector2f getNextPosition(){ return nextPosition;};


    sf::Vector2f getPosition(){return body.getPosition();};
    // void setPosition(sf::Vector2f newPosition){position = newPosition; };


    sf::RectangleShape& getCollisionsBox(){ return collisionsBox;};
    void setCollisionBoxPosition(sf::Vector2f position){
        collisionsBox.setPosition(position);
    };

    sf::Vector2f getTargetPosition(){
        return targetPosition;
    };
    void setTargetPosition(sf::Vector2f pos){
        targetPosition = pos;
    };
    void setActualPosition(sf::Vector2f pos){
        actualPosition = pos;
    };
    void setNextPosition(sf::Vector2f pos){
        nextPosition = pos;
    };


    void setDebugModus(bool modus){
        debugModus = modus;
    };

    //std::map<std::string, Item*> getInventar(){ return inventar;};

    //std::vector<Item*> getInventar(){ return inventar;};
    //void setInventar(Item* item){inventar.push_back(item);};



    /*
    void setInventar(Item* item){
        std::pair<std::string, Item*> pair (item->name, item);
        inventar.insert(pair);
    };
     */

    //void setFlintstone(bool getFlintstone){flintstoneStatus = getFlintstone;};
    //void setCrossbow(bool getCrossbow){crossbowStatus = getCrossbow;};

    //bool getFlintstoneStatus(){ return flintstoneStatus;};
    //bool getCrossbowStatus(){ return crossbowStatus;};

    bool getAnimating(){ return animating;};
    void animate(float dt);

    sf::Vector2f getDirection(){ return this->direction;};


private:

    GameDataRef data;
    sf::RectangleShape body;
    sf::RectangleShape collisionsBox;
    sf::Vector2f targetPosition;
    sf::Vector2f actualPosition;

    bool animating;

    Animation animation;
    unsigned int row;
    bool faceRight;
    sf::Vector2f nextPosition;

    sf::Vector2f direction;

    sf::Texture texture;

    bool debugModus;
    //sf::Vector2f position;
    //std::map<std::string,Item*> inventar;
    //std::vector<Item*> inventar;

    //bool flintstoneStatus;
    //bool crossbowStatus;


};


#endif //MYGAME_PLAYERADVENTURE_H
