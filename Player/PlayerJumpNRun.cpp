//
// Created by das_maichen on 05.04.18.
//

#include <tgmath.h>
#include "PlayerJumpNRun.h"

//PlayerJumpNRun::PlayerJumpNRun(const std::string &playerTexture, sf::Vector2u imageCount, float switchTime, float speed, float jumpHeight)
 //       : animation(playerTexture,imageCount,switchTime) {

PlayerJumpNRun::PlayerJumpNRun(sf::Texture &textureInput, sf::Vector2u imageCount, float switchTime, float speed, float jumpHeight)
        : animation(textureInput,imageCount,switchTime) {

    debugModus = false;

    this->speed= speed;
    this->jumpHeight = jumpHeight;
    row = 0;
    faceRight = true;

    texture = textureInput;

    collisionsBox.setSize(sf::Vector2f(50.0f, 150.0f));
    collisionsBox.setOrigin(collisionsBox.getSize()/2.0f);
    collisionsBox.setPosition(600,400);
    collisionsBox.setFillColor(sf::Color(255,0,0,128));

    body.setSize(sf::Vector2f(150.0f, 150.0f));
    body.setOrigin(body.getSize()/2.0f);
    body.setPosition(625,450);
    body.setTexture(&texture);

    jump = false;

    buffer.loadFromFile("Content/Sounds/jump.wav");
    jumpSound.setBuffer(buffer);
}

PlayerJumpNRun::~PlayerJumpNRun()
{
    //dtor
}

void PlayerJumpNRun::update(float deltaTime) {

    //Hier beginnt der jump&run Teil

    //sinnvoll?
    velocity.x = 0;


    velocity.x *= 0.2f;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
        velocity.x -= speed;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
        velocity.x += speed;
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && canJump){
        jumpSound.play();
        canJump = false;
        jump = true;
        velocity.y = -sqrt(2.0f * 981.0f * jumpHeight);
    }

    velocity.y += 981.0f * deltaTime;


    if (velocity.x == 0) {
        row = 0;
    } else{
        row = 1;
        if(velocity.x > 0.0f){
            faceRight = true;
            //faceRight = false;
        } else{
            //faceRight = true;
            faceRight = false;
        }
    }

    if(jump){
        row = 2;
    }


    animation.update(row, deltaTime,faceRight);
    body.setTextureRect(animation.uvRect);
    //body.move(velocity * deltaTime);
    collisionsBox.move(velocity* deltaTime);
    body.setPosition(collisionsBox.getPosition());
}

void PlayerJumpNRun::draw(sf::RenderWindow& window)
{
    window.draw(body);
    if(debugModus){
        window.draw(collisionsBox);
    }
}

void PlayerJumpNRun::onCollision(sf::Vector2f direction) {
    if(direction.x < 0.0f){
        velocity.x = 0.0f;
    }else if(direction.x >0.0f){
        velocity.x = 0.0f;
    }

    if(direction.y < 0.0){
        velocity.y = 0.0f;
        canJump = true;
        jump = false;
    } else if(direction.y < 0.0){
        velocity.y = 0.0f;
    }

}


/*
void PlayerJumpNRun::handleEvents(sf::Event& event)
{
    if (event.type == sf::Event::KeyPressed)
    {
        if (event.key.code == sf::Keyboard::Up)
        {
            position.y -= 5.f;
        }
        else if (event.key.code == sf::Keyboard::Down)
        {
            position.y += 5.f;
        }

        if (event.key.code == sf::Keyboard::Left)
        {
            position.x -= 5.f;
        }
        else if (event.key.code == sf::Keyboard::Right)
        {
            position.x += 5.f;
        }
    }
}
 */