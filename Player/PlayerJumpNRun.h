// Created by das_maichen on 05.04.18.
//

#ifndef MYGAME_PLAYER_H
#define MYGAME_PLAYER_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include "../Animation.h"
#include "../Collider.h"
#include "../Game.h"
#include "../Lives.h"

class PlayerJumpNRun
{
public:
    //PlayerJumpNRun(const std::string &playerTexture, sf::Vector2u imageCount, float switchTime, float speed, float jumpHeight);
    PlayerJumpNRun(sf::Texture &texture, sf::Vector2<unsigned int> imageCount, float switchTime, float speed, float jumpHeight);
    ~PlayerJumpNRun();

    void update(float deltaTime);
    void draw(sf::RenderWindow& window);
    void onCollision(sf::Vector2f direction);

    sf::Vector2f getPosition(){return body.getPosition();};
    void setPosition(sf::Vector2f position){collisionsBox.setPosition(position);};

    sf::RectangleShape& getBody(){ return body;};

    Collider getCollider(){ return Collider(body);}
    sf::RectangleShape& getCollisionsBox(){ return collisionsBox;};
    void setDebugModus(bool modus){
        debugModus = modus;
    };



    //  void handleEvents(sf::Event& event);

private:
    GameDataRef data;
    sf::RectangleShape body;
    Animation animation;
    unsigned int row;
    float speed;
    bool faceRight;
    sf::Vector2f velocity;
    bool canJump;
    float jumpHeight;

    sf::Texture texture;

    bool jump;
    sf::RectangleShape collisionsBox;
    bool debugModus;

    sf::Sound jumpSound;
    sf::SoundBuffer buffer;



};


#endif //MYGAME_PLAYER_H