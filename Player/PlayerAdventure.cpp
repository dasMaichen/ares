//
// Created by das_maichen on 18.04.18.
//

#include <iostream>
#include "PlayerAdventure.h"
#include "../DEFINITIONS.h"

PlayerAdventure::PlayerAdventure(GameDataRef data, sf::Texture &playerTexture, sf::Vector2u imageCount, float switchTime, float speed)
        : animation(playerTexture,imageCount,switchTime), data(data) {

    debugModus = false;
    animating = false;
    //flintstoneStatus = false;
    //crossbowStatus = false;

    //this->speed= speed;
    row = 0;
    faceRight = true;

    texture = playerTexture;

    collisionsBox.setSize(sf::Vector2f(50.0f, 50.0f));
    collisionsBox.setOrigin(collisionsBox.getSize()/2.0f);
    //collisionsBox.setPosition(1950 - 25, 1800 +25);
    collisionsBox.setPosition(100 - 25, 250 +25);
    collisionsBox.setFillColor(sf::Color(255,0,0,128));

    //body.setSize(sf::Vector2f(100.0f, 100.0f));
    body.setSize(sf::Vector2f(50,100));
    body.setOrigin(body.getSize().x/2.0f, body.getSize().y - 25);
    body.setPosition(collisionsBox.getPosition());
    body.setTexture(&texture);

    // position = sf::Vector2f(collisionsBox.getPosition());

    direction = sf::Vector2f(0,1);
    targetPosition = collisionsBox.getPosition();
    actualPosition = collisionsBox.getPosition();
    nextPosition = collisionsBox.getPosition();




}

PlayerAdventure::~PlayerAdventure()
{

}

void PlayerAdventure::update(float deltaTime) {

    //Dieser Teil soll für den RPG Teil rein.

    /*
    sf::Vector2f movement(0.0f, 0.0f);

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
        //collisionsBox.setPosition(collisionsBox.getPosition().x - 50, collisionsBox.getPosition().y);
        movement.x -= speed * deltaTime;
        direction = sf::Vector2f(1,0);
        //std::cout << "links" << std::endl;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
        movement.x += speed * deltaTime;
        direction = sf::Vector2f(-1,0);
        //std::cout << "rechts" << std::endl;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
        movement.y -= speed * deltaTime;
        direction = sf::Vector2f(0,1);
        //std::cout << "oben" << std::endl;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
        movement.y += speed * deltaTime;
        direction = sf::Vector2f(0,-1);
        //std::cout << "unten" << std::endl;
    }



    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
        collisionsBox.setPosition(collisionsBox.getPosition().x - 5, collisionsBox.getPosition().y);
        direction = sf::Vector2f(1,0);
        //std::cout << "links" << std::endl;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
        collisionsBox.setPosition(collisionsBox.getPosition().x + 5, collisionsBox.getPosition().y);
        direction = sf::Vector2f(-1,0);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
        collisionsBox.setPosition(collisionsBox.getPosition().x, collisionsBox.getPosition().y - 5);
        direction = sf::Vector2f(0,1);
        //std::cout << "oben" << std::endl;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
        collisionsBox.setPosition(collisionsBox.getPosition().x, collisionsBox.getPosition().y + 5);
        direction = sf::Vector2f(0,-1);
        //std::cout << "unten" << std::endl;
    }

*/


    sf::Vector2f movement(0.0f, 0.0f);

    int deltaX = collisionsBox.getPosition().x - targetPosition.x;
    int deltaY = collisionsBox.getPosition().y - targetPosition.y;

    if(deltaX > 0){
        collisionsBox.move(-2,0);
        movement = sf::Vector2f(-1,0);
        animating = true;
    } else if(deltaX < 0){
        collisionsBox.move(2,0);
        movement = sf::Vector2f(1,0);
        animating = true;
    } else if(deltaY > 0){
        collisionsBox.move(0,-2);
        movement = sf::Vector2f(0,-1);
        animating = true;
    } else if(deltaY < 0){
        collisionsBox.move(0,2);
        movement = sf::Vector2f(0,1);
        animating = true;
    } else{
        collisionsBox.move(0,0);
        movement.x = (0,0);
        movement.y = (0,0);
    }

    if (movement.x == 0 && movement.y == 0 && direction.x == 0 && direction.y > 0) {
        row = 0;
    } else if(movement.x == 0 && movement.y == 0 && direction.x != 0 && direction.y == 0) {
        row = 1;
        if(direction.x < 0){
            faceRight = false;
        } else{
            faceRight = true;
        }
    } else if(movement.x == 0 && movement.y == 0 && direction.x == 0 && direction.y < 0) {
        row = 2;


    } else if (movement.x != 0 && movement.y == 0) {
        faceRight = false;
        row = 4;

        if (movement.x < 0.0f) {
            faceRight = true;
        } else {
            faceRight = false;
        }
    } else if (movement.x == 0 && movement.y != 0) {
        if(movement.y > 0){
            row = 3;
        } else{
            row = 5;
        }

    }


    if (collisionsBox.getPosition().x == targetPosition.x && collisionsBox.getPosition().y == targetPosition.y) {

        collisionsBox.move(0, 0);
        actualPosition = targetPosition;
        animating = false;
    }


    animation.update(row, deltaTime,faceRight);
    body.setTextureRect(animation.uvRect);
    //body.move(2,0);


    //collisionsBox.move(2,0);
    body.setPosition(collisionsBox.getPosition());

}



void PlayerAdventure::draw(sf::RenderWindow& window)
{
    if(debugModus){
        window.draw(collisionsBox);
    }
    window.draw(body);
    //window.draw(collisionsBox);
    /*
    if(inventar.size() < 0) {
        window.draw(activeItem->getSprite());
    }
     */
}

void PlayerAdventure::calculateNextPosition(sf::Event event) {

    if (event.type == sf::Event::KeyPressed) {
        switch (event.key.code) {
            case sf::Keyboard::Up:
                direction = sf::Vector2f(0, -1);
                nextPosition = (
                        sf::Vector2f(actualPosition.x, actualPosition.y - 50));

                //std::cout << "up!" << std::endl;
                break;
            case sf::Keyboard::Down:
                nextPosition = (sf::Vector2f(actualPosition.x, actualPosition.y + 50));
                direction = sf::Vector2f(0,1);
                //std::cout << "down!" << std::endl;
                break;
            case sf::Keyboard::Right:
                nextPosition = (sf::Vector2f(actualPosition.x + 50, actualPosition.y));
                direction = sf::Vector2f(-1,0);
                //std::cout << "right!" << std::endl;
                break;
            case sf::Keyboard::Left:
                nextPosition = (sf::Vector2f(actualPosition.x - 50, actualPosition.y));
                direction = sf::Vector2f(1,0);
                //std::cout << "left!" << std::endl;
                break;
        }
    }
}

