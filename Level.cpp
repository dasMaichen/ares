//
// Created by das_maichen on 05.06.18.
//

#include <fstream>
#include <iostream>
#include "Level.h"
#include "DEFINITIONS.h"

//Level::Level(GameDataRef data) : data(data), items() {

//}


Level::Level(GameDataRef data, bool load) : data(data), gegenstaende(),enemies() {
    this->load = load;

}

void Level::inizialize() {

    this->data->assets.loadTexture("Plattform", PLATTFORM_FILEPATH);

    torchRoom6 = false;
    //plattforms = new Plattforms(data);
    //plattforms->loadPlattformPosition("Content/adventure_level.level");

    plattformsRoom1 = new Plattforms(data);
    plattformsRoom1->loadPlattformPosition("Content/LevelFile/level2_room1.level");

    plattformsRoom2 = new Plattforms(data);
    plattformsRoom2->loadPlattformPosition("Content/LevelFile/level2_room2.level");

    plattformsRoom3 = new Plattforms(data);
    plattformsRoom3->loadPlattformPosition("Content/LevelFile/level2_room3.level");

    plattformsRoom4 = new Plattforms(data);
    plattformsRoom4->loadPlattformPosition("Content/LevelFile/level2_room4.level");

    plattformsRoom5 = new Plattforms(data);
    plattformsRoom5->loadPlattformPosition("Content/LevelFile/level2_room5.level");

    plattformsRoom6 = new Plattforms(data);
    plattformsRoom6->loadPlattformPosition("Content/LevelFile/level2_room6.level");

    plattformsRoom7 = new Plattforms(data);
    plattformsRoom7->loadPlattformPosition("Content/LevelFile/level2_room7.level");

    plattformsRoom8 = new Plattforms(data);
    plattformsRoom8->loadPlattformPosition("Content/LevelFile/level2_room8.level");
    //setItems("Content/items.item");
    //setEnemies("Content/level1.enemies");

    if(!load) {
        setItems("Content/LevelFile/level2_items.item");
    } else{
        setItems("Content/LevelFile/level2_load_items.item");
    }
    //startroomBackground.setTexture(this->data->assets.getTexture("LevelBackground"));
    //foreground.setTexture(this->data->assets.getTexture("Foreground"));
    //this->data->assets.loadTexture("room1", "Content/Texture/levelTexture/room1.png");
    //this->data->assets.loadTexture("room1_foreground","Content/Texture/levelTexture/room1_foreground.png" );
    this->data->assets.loadTexture("room1", "Content/Texture/levelTexture/room1_2.png");
    this->data->assets.loadTexture("room1_foreground","Content/Texture/levelTexture/room1_foreground.png" );
    room1.setTexture(this->data->assets.getTexture("room1"));
    room1.setScale(0.5,0.5);
    room1Foreground.setTexture(this->data->assets.getTexture("room1_foreground"));

    this->data->assets.loadTexture("room2", "Content/Texture/levelTexture/room2_2.png");
    this->data->assets.loadTexture("room2_foreground","Content/Texture/levelTexture/room2_foreground.png" );
    room2.setTexture(this->data->assets.getTexture("room2"));
    room2Foreground.setTexture(this->data->assets.getTexture("room2_foreground"));

    this->data->assets.loadTexture("room3", "Content/Texture/levelTexture/room3_2.png");
    this->data->assets.loadTexture("room3_foreground", "Content/Texture/levelTexture/room3_2foreground.png");
    room3.setTexture(this->data->assets.getTexture("room3"));
    room3Foreground.setTexture(this->data->assets.getTexture("room3_foreground"));

    this->data->assets.loadTexture("room4", "Content/Texture/levelTexture/room4_2.png");
    this->data->assets.loadTexture("room4_foreground", "Content/Texture/levelTexture/room4_foreground.png");
    room4.setTexture(this->data->assets.getTexture("room4"));
    room4Foreground.setTexture(this->data->assets.getTexture("room4_foreground"));

    this->data->assets.loadTexture("room5", "Content/Texture/levelTexture/room5.png");
    this->data->assets.loadTexture("room5_foreground", "Content/Texture/levelTexture/room5_foreground.png");
    room5.setTexture(this->data->assets.getTexture("room5"));
    room5Foreground.setTexture(this->data->assets.getTexture("room5_foreground"));

    this->data->assets.loadTexture("room6", "Content/Texture/levelTexture/room6_2.png");
    this->data->assets.loadTexture("room6_foreground", "Content/Texture/levelTexture/room6_foreground_darkness.png");
    this->data->assets.loadTexture("room6_foreground_torch", "Content/Texture/levelTexture/room6_foreground_dark.png");

    room6.setTexture(this->data->assets.getTexture("room6"));
    room6Foreground.setTexture(this->data->assets.getTexture("room6_foreground"));
    room6ForegroundTorch.setTexture(this->data->assets.getTexture("room6_foreground_torch"));

    this->data->assets.loadTexture("room7", "Content/Texture/levelTexture/room7_2.png");
    this->data->assets.loadTexture("room7_foreground", "Content/Texture/levelTexture/room7_foreground.png");
    room7.setTexture(this->data->assets.getTexture("room7"));
    room7Foreground.setTexture(this->data->assets.getTexture("room7_foreground"));

    this->data->assets.loadTexture("room8", "Content/Texture/levelTexture/room8_2.png");
    this->data->assets.loadTexture("room8_foreground", "Content/Texture/levelTexture/room8_foreground.png");
    room8.setTexture(this->data->assets.getTexture("room8"));
    room8Foreground.setTexture(this->data->assets.getTexture("room8_foreground"));




    //this->data->assets.loadTexture("lightOnForeground","Content/Texture/levelTexture/lightOn.png" );

    //foreground.setTexture(this->data->assets.getTexture("lightOnForeground"));


    actualRoom = room1;
    actualRoomForeground = room1Foreground;

    actualRoom.setPosition(-50,0);
    actualRoomForeground.setPosition(-50,0);
    roomNo = 1;
}

void Level::update() {

    for(int i = 0; i < gegenstaende.size(); i++){
        gegenstaende.at(i)->update();
    }

    switch (roomNo){
        case 1:
            plattforms = plattformsRoom1;
            actualRoom = room1;
            actualRoomForeground = room1Foreground;
            actualRoom.setPosition(-50,0);
            actualRoomForeground.setPosition(-50,0);
            break;
        case 2:
            plattforms = plattformsRoom2;
            actualRoom = room2;
            actualRoom.setScale(0.5,0.5);
            actualRoom.setPosition(-50,0);
            actualRoomForeground = room2Foreground;
            actualRoomForeground.setScale(0.5,0.5);
            actualRoomForeground.setPosition(-50,0);

            break;
        case 3:
            plattforms = plattformsRoom3;
            actualRoom = room3;
            actualRoomForeground = room3Foreground;
            actualRoom.scale(0.5,0.5);
            actualRoomForeground.setScale(0.5,0.5);
            actualRoom.setPosition(-50,-200);
            actualRoomForeground.setPosition(-50,-200);
            break;
        case 4:
            plattforms = plattformsRoom4;
            actualRoom = room4;
            actualRoom.scale(0.5,0.5);
            actualRoomForeground = room4Foreground;
            break;
        case 5:
            plattforms = plattformsRoom5;
            actualRoom = room5;
            actualRoom.setPosition(-50, -50);
            actualRoomForeground = room5Foreground;
            actualRoomForeground.setPosition(-50,-50);
            break;
        case 6:
            plattforms = plattformsRoom6;
            actualRoom = room6;
            //actualRoomForeground.setScale(1,1);
            //actualRoomForeground.setTexture(this->data->assets.getTexture("room6_foreground_torch"));
            //actualRoomForeground.setPosition(0,-550);
            if(!torchRoom6){
                actualRoomForeground = room6Foreground;
            } else{
                actualRoomForeground = room6ForegroundTorch;
            }
            //actualRoomForeground.setPosition()
            actualRoom.setScale(0.5,0.5);
            break;
        case 7:
            plattforms = plattformsRoom7;
            actualRoom = room7;
            actualRoom.setScale(0.5,0.5);
            //actualRoom.setPosition(-100,50);
            actualRoomForeground = room7Foreground;
            actualRoomForeground.setScale(0.5,0.5);
            break;
        case 8:
            plattforms = plattformsRoom8;
            actualRoom = room8;
            actualRoom.scale(0.5,0.5);
            actualRoom.setPosition(-50,-50);
            actualRoomForeground = room8Foreground;
            actualRoomForeground.scale(0.5,0.5);
            actualRoomForeground.setPosition(-50,-50);
            break;

    }


    /*
    for(int i = 0; i < enemies.size(); i++){
        enemies.at(i)->update();
    }
     */




    /*
    for(auto it = this->items.begin(); it!=this->items.end(); ++it){
        this->items.at(it->first)->update();
    }
     */

    //items.at(0).setPicked(true);

}

void Level::drawLayer1() {

    //this->data->window.draw(startroomBackground);
    //plattforms->drawPlattforms();
    this->data->window.draw(actualRoom);
    for(int i = 0; i < gegenstaende.size(); i++){
        if(gegenstaende.at(i)->getRoomNo() == this->roomNo){
            gegenstaende.at(i)->draw();
        }

    }



}

void Level::drawLayer2() {
    for(int i = 0; i < gegenstaende.size(); i++){
        if(gegenstaende.at(i)->getRoomNo() == this->roomNo &&
                (gegenstaende.at(i)->name == "Door1" && gegenstaende.at(i)->getActivate()
                || gegenstaende.at(i)->getType() == "Fackel" || gegenstaende.at(i)->name == "Feuerstein")){
            //this->data->window.draw(actualRoomForeground);
            gegenstaende.at(i)->draw();
        };
    }

    if(roomNo == 6){
        this->data->window.draw(actualRoomForeground);
    }



}

void Level::drawLayer3() {
    for(int i = 0; i < gegenstaende.size(); i++) {
        Item *item = gegenstaende.at(i);
        if (item->getRoomNo() == this->roomNo && (item->name == "Door1" && item->getActivate())){
            item->draw();
            this->data->window.draw(actualRoomForeground);
        }

        if(item->getRoomNo() == this->roomNo && ((item->getName() == "FackelS_3" ||
                item->getName() == "FackelS_4" || item->getName() == "Fackel4_5") && item->getActivate())) {
            //this->data->window.draw(actualRoomForeground);
            item->draw();
        }

        if(item->getRoomNo() == this->roomNo && roomNo == 2 && item->getType() == "Fackel"){
            item->draw();
        }

        if(item->getRoomNo() == this->roomNo && item->getType() == "Fire" && item->getExist()){
            item->draw();
        }
    }
    if(roomNo > 1){
        this->data->window.draw(actualRoomForeground);
    }

    plattforms->drawPlattforms();


}

void Level::setItems(std::string filename) {

    std::ifstream infile(filename);

    if(!infile.good()){
        std::cerr << "could not open item-file: " << filename << std::endl;
        return;
    }

    std::string output_string;

    while (std::getline(infile, output_string)) {


        std::string coordinates(output_string);
        std::string::size_type size;

        std::string delimiter = " ";

        size_t pos = 0;
        std::string zwischenprodukt;

        std::vector<std::string> stringVector;

        std::string x_component;
        std::string y_component;
        std::string spriteTexture;

        while ((pos = coordinates.find(delimiter)) != std::string::npos) {
            stringVector.push_back(coordinates.substr(0, pos));
            zwischenprodukt = coordinates.erase(0, pos + delimiter.length());
            stringVector.push_back(zwischenprodukt);
        }

        for(int i = 1; i <= stringVector.size()/2 + 2; i++){
            stringVector.erase(stringVector.begin() + i);
        }

        bool exist = false;
        bool activate = false;
        bool picked = false;
        bool inventory = false;

        if(stringVector.at(0) == "t"){
            exist = true;
        }

        if(stringVector.at(1) == "t"){
            activate = true;
        }

        if(stringVector.at(2) == "t"){
            picked = true;
        }

        if(stringVector.at(3) == "t"){
            inventory = true;
        }


        std::string name = stringVector.at(4);

        std::string type = stringVector.at(5);
        float x = std::stof (stringVector.at(6),&size);
        float y = std::stof (stringVector.at(7),&size);
        int roomNo = std::stoi(stringVector.at(8), &size);

        /*
        for(int i = 0; i < stringVector.size(); i++){
            std::cout << stringVector.at(i) << std::endl;
        }
         */

        Item* item = new Item(this->data);
        item->inizialize(exist,activate,picked,inventory,name,type,sf::Vector2f(x,y),roomNo);
        //std::pair<std::string, Item*> pair (item->name, item);
        //this->items.insert(pair);
        //items[item->name] = *item;

        gegenstaende.push_back(item);
    }
}


/*
void Level::setEnemies(std::string filename) {

    std::ifstream infile(filename);

    if(!infile.good()){
        std::cerr << "could not open enemy-file: " << filename << std::endl;
        return;
    }

    std::string output_string;

    while (std::getline(infile, output_string)) {


        std::string coordinates(output_string);
        std::string::size_type size;

        std::string delimiter = " ";

        size_t pos = 0;
        std::string zwischenprodukt;

        std::vector<std::string> stringVector;

        std::string x_component;
        std::string y_component;
        std::string spriteTexture;

        while ((pos = coordinates.find(delimiter)) != std::string::npos) {
            stringVector.push_back(coordinates.substr(0, pos));
            zwischenprodukt = coordinates.erase(0, pos + delimiter.length());
            stringVector.push_back(zwischenprodukt);
        }

        for(int i = 1; i <= stringVector.size()/2 + 2; i++){
            stringVector.erase(stringVector.begin() + i);
        }

        std::string name = stringVector.at(0);

        std::string type = stringVector.at(1);

        float positionX = std::stof (stringVector.at(2),&size);
        float positionY = std::stof (stringVector.at(3),&size);

        float startPositionX = std::stof (stringVector.at(4),&size);
        float startPositionY = std::stof (stringVector.at(5),&size);

        float speedX = std::stof(stringVector.at(6),&size);
        float speedY = std::stof(stringVector.at(7),&size);

        Enemy* enemy = new Enemy(this->data);
        enemy->inizialize(name,type,sf::Vector2f(positionX, positionY),sf::Vector2f(startPositionX, startPositionY),
                          sf::Vector2f(speedX,speedY));
        enemies.push_back(enemy);
    }

}
 */


Level::~Level() {
    delete plattforms;

}

Plattforms *Level::getPlattforms() {
    return plattforms;
    //return plattformsRoom1;
}




