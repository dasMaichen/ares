//
// Created by das_maichen on 05.06.18.
//

#include "Item.h"

Item::Item(GameDataRef data) : data(data){

}

void Item::inizialize(bool exist, bool activate, bool picked, bool inventory, std::string name, std::string type, sf::Vector2f position, int roomNo) {

    debugModus = false;
    this->name = name;
    sprite.setScale(0.5,0.5);
    this->roomNo = roomNo;

    if(type == "Herz"){

        this->sprite.setTexture(this->data->assets.getTexture("Item"));
    }
    if(type == "Fackel"){

        this->sprite.setTexture(this->data->assets.getTexture("FackelAus"));
        //sprite.scale(0.5,0.5);
    }
    if(type == "Feuerstein"){

        this->sprite.setTexture(this->data->assets.getTexture("Feuerstein"));
        //sprite.setScale(0.5,0.5);
    }
    if(type == "NoType"){
        this->sprite.setTexture(this->data->assets.getTexture("NoItem"));
    }
    if(type == "Crossbow"){
        this->sprite.setTexture(this->data->assets.getTexture("Crossbow"));
    }

    if(type == "Torch"){
        this->sprite.setTexture(this->data->assets.getTexture("Torch"));
    }

    if(type == "Bowl"){
        this->sprite.setTexture(this->data->assets.getTexture("Bowl"));
    }

    /*
    if(name == "TombSwitch" || name == "TombSwitch2" || name == "Exit7_6" || name == "Shrine"){
        this->sprite.setTexture(this->data->assets.getTexture("Feuerstein"));
    }
     */

    if(name == "EyeSwitch" || name == "TombSwitchEye"){
        this->sprite.setTexture(this->data->assets.getTexture("EyeOff"));
    }

    /*
    if(type == "Exit"){
        this->sprite.setTexture(this->data->assets.getTexture("Crossbow"));
    }
     */

    if(type == "Fence"){
        this->sprite.setTexture(this->data->assets.getTexture("FenceClosed"));
    }
    if(type == "Chest"){
        this->sprite.setTexture(this->data->assets.getTexture("ChestClosed"));
    }

    if(name == "Door1"){
        this->sprite.setTexture(this->data->assets.getTexture("StartroomClosedDoor"));
    }

    if(name == "Door2"){
        this->sprite.setTexture(this->data->assets.getTexture("SealedDoor"));
    }

    if(name == "Door3"){
        this->sprite.setTexture(this->data->assets.getTexture("DoorClosedBig"));
    }

    if(name == "Stairs" || name == "FinalExit"){
        this->sprite.setTexture(this->data->assets.getTexture("NoItem"));
    }

    if(name == "Switch"){
        this->sprite.setTexture(this->data->assets.getTexture("Switch"));
    }

    if(name == "StairsGarden"){
        this->sprite.setTexture(this->data->assets.getTexture("NoItemBig"));
    }

    if(name == "GuardianButton"){
        this->sprite.setTexture(this->data->assets.getTexture("GuardianButton"));
    }

    if(name == "Exit2_7"){
        this->sprite.setTexture(this->data->assets.getTexture("ReliefDoor1"));
    }

    if(name == "Exit2_8-3"){
        this->sprite.setTexture(this->data->assets.getTexture("ReliefDoor2"));
    }

    if(name == "Exit2_3-1" || name == "Exit4_5-1" || name == "Exit7_2" || name == "Exit8_2-3"){
        this->sprite.setTexture(this->data->assets.getTexture("DoorDownLeft"));
    }
    if(name == "Exit2_3-2" || name == "Exit4_5-2"){
        this->sprite.setTexture(this->data->assets.getTexture("DoorDownRight"));
    }

    if(name == "FireViolett"){
        this->sprite.setTexture(this->data->assets.getTexture("FireViolett"));
    }

    if(name == "FireYellow"){
        this->sprite.setTexture(this->data->assets.getTexture("FireYellow"));
    }

    if(name == "FireBlue"){
        this->sprite.setTexture(this->data->assets.getTexture("FireBlue"));
    }

    if(name == "FireGreen"){
        this->sprite.setTexture(this->data->assets.getTexture("FireGreen"));
    }


    this->exist = exist;
    this->activate = activate;
    this->picked = picked;
    this->inventory = inventory;

    this->type = type;

    this->position = position;
    this->sprite.setPosition(position);
    //this->collidingBox.setSize(sf::Vector2f(sprite.getLocalBounds().width,sprite.getLocalBounds().height));
    //this->collidingBox.setFillColor(sf::Color(0,255,0,128));
    //this->collidingBox.setPosition(position);
}

void Item::update() {
    if(picked){
        exist = false;
    }

    if(type == "Fackel" && activate){
        //sprite.scale(0.5,0.5);
        this->sprite.setTexture(this->data->assets.getTexture("FackelAn"));

    }

    if(name == "Door1" && activate){
        this->sprite.setTexture(this->data->assets.getTexture("StartroomDoorOpen"));
    }

    if((name == "Door2" || name == "Door3") && activate){
        this->sprite.setTexture(this->data->assets.getTexture("NoItem"));
    }

    if((name == "Exit2_3-1" || name == "Exit2_3-2" || name == "Exit4_5-1" || name == "Exit4_5-2"
        || name == "Exit2_8-1" || name == "Exit2_8-2" || name == "Exit7_2" || name == "Exit8_2-3")
       && activate){
        this->sprite.setTexture(this->data->assets.getTexture("NoItem"));
    }

    if(type == "Chest" && activate){
        this->sprite.setTexture(this->data->assets.getTexture("ChestOpen"));
    }

    if(type == "Fence" && activate){
        this->sprite.setTexture(this->data->assets.getTexture("FenceOpen"));
    }

    if(name == "Stairs" && activate){
        this->sprite.setTexture(this->data->assets.getTexture("Stairs"));
    }

    if(name == "FinalExit" && activate){
        this->sprite.setTexture(this->data->assets.getTexture("Stairs"));
        sprite.setRotation(90);
    }

    if(name == "StairsGarden" && activate){
        sprite.setScale(0.5,0.5);
        this->sprite.setTexture(this->data->assets.getTexture("StairsGarden"));

    }

    if(name == "Switch" && activate){
        this->sprite.setTexture(this->data->assets.getTexture("SwitchActive"));
    }

    if((name == "Exit2_7" || name == "Exit2_8") && activate){
        this->sprite.setTexture(this->data->assets.getTexture("ReliefDoorOpen"));
    }

    sprite.setPosition(position);

    if(name == "BowlViolett" && activate){
        this->sprite.setTexture(this->data->assets.getTexture("BowlViolett"));
    }
    if(name == "BowlYellow" && activate){
        this->sprite.setTexture(this->data->assets.getTexture("BowlYellow"));
    }
    if(name == "BowlBlue" && activate){
        this->sprite.setTexture(this->data->assets.getTexture("BowlBlue"));
    }
    if(name == "BowlGreen" && activate){
        this->sprite.setTexture(this->data->assets.getTexture("BowlGreen"));
    }

    if((name == "EyeSwitch" && activate) || (name == "TombSwitchEye" && activate) ){
        this->sprite.setTexture(this->data->assets.getTexture("EyeOn"));
    }
}

void Item::draw() {

    //if(exist && debugModus){
        //this->data->window.draw(collidingBox);
    //}

    if(exist) {
        this->data->window.draw(sprite);
    }
}


