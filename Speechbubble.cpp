//
// Created by das_maichen on 20.06.18.
//

#include <sstream>
#include "Speechbubble.h"
#include "Game.h"
#include "DEFINITIONS.h"

Speechbubble::Speechbubble(GameDataRef data, sf::View& view) : data(data) {


}

void Speechbubble::inizializeTextBox( sf::Vector2f position, sf::Vector2f size, std::string text) {
    textBox.setSize(size);
    textBox.setFillColor(sf::Color::Magenta);
    textBox.setPosition(position);

    this->data->assets.loadTexture("StoryBox", STORY_BOX_FILEPATH);
    speechBox.setTexture(this->data->assets.getTexture("StoryBox"));
    speechBox.setPosition(textBox.getPosition().x - 20, textBox.getPosition().y - 10);
    speechBox.setScale(0.3,0.3);

    this->text.setCharacterSize(20);
    this->text.setPosition(textBox.getPosition().x + 50, textBox.getPosition().y + 10);
    this->text.setColor(sf::Color::White);
    this->text.setFont(this->data->assets.getFont("GameFont"));

    setText(text);

    if(characterSpeech){

    }


}

void Speechbubble::inizializeCharacterSpeechBox(sf::View& view, std::string text,
                                                sf::Sprite characterImage, std::string name) {

    this->data->assets.loadTexture("SpeechBox", SPEECH_BOX_FILEPATH);


    textBox.setSize(sf::Vector2f(650, 150));
    textBox.setFillColor(sf::Color::Magenta);
    textBox.setPosition(view.getCenter().x - 360, view.getCenter().y + 130);

    speechBox.setTexture(this->data->assets.getTexture("SpeechBox"));
    speechBox.setPosition(textBox.getPosition().x - 20, textBox.getPosition().y - 70);
    speechBox.setScale(0.3, 0.3);

    this->characterName.setFont(this->data->assets.getFont("GameFont"));
    this->characterName.setCharacterSize(30);
    this->characterName.setString(name);
    this->characterName.setColor(sf::Color(200,0,0));
    this->characterName.setPosition(textBox.getPosition().x + 120, textBox.getPosition().y - 60);

    this->text.setCharacterSize(20);
    this->text.setPosition(textBox.getPosition().x + 50, textBox.getPosition().y + 10);
    this->text.setColor(sf::Color::White);
    this->text.setFont(this->data->assets.getFont("GameFont"));

    setText(text);

    character = characterImage;
    character.setScale(1,1);
    character.setPosition(textBox.getPosition().x + 500, textBox.getPosition().y - 250);
}


void Speechbubble::setText(std::string text) {

    std::string textString = "";
    std::stringstream stringstream;
    std::string trialString;

    float maxWidth = textBox.getSize().x - 40;
    float maxHeight = textBox.getSize().y - 40;

    std::string coordinates(text);

    std::string delimiter = " ";

    size_t pos = 0;
    //std::string zwischenprodukt;
    std::string word_component;
    std::string text_component;
    std::string spriteTexture;
    while ((pos = coordinates.find(delimiter)) != std::string::npos) {
        word_component = coordinates.substr(0, pos);
        text_component = coordinates.erase(0, pos + delimiter.length());

        trialString = textString + word_component + " ";
        this->text.setString(trialString);

        if(this->text.getGlobalBounds().width > maxWidth){
            textString = textString + "\n" + word_component + " ";
            this->text.setString(textString);
        } else{
            textString = trialString;
        }
    }
}

void Speechbubble::draw() {
    //this->data->window.draw(textBox);
    this->data->window.draw(this->character);
    this->data->window.draw(speechBox);
    this->data->window.draw(this->text);
    this->data->window.draw(this->characterName);

}

void Speechbubble::setPosition(sf::Vector2f viewCenter) {
    textBox.setPosition(viewCenter.x - 360, viewCenter.y + 130);
    speechBox.setPosition(textBox.getPosition().x - 20, textBox.getPosition().y - 70);
    this->characterName.setPosition(textBox.getPosition().x + 120, textBox.getPosition().y - 60);
    this->text.setPosition(textBox.getPosition().x + 50, textBox.getPosition().y + 10);
    character.setPosition(textBox.getPosition().x + 500, textBox.getPosition().y - 250);
}
