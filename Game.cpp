#include <chrono>
#include <thread>
#include "Game.h"
#include "States/SplashState.h"
#include "DEFINITIONS.h"


Game::Game(int width, int height, std::string title) {
    data->window.create(sf::VideoMode(width, height), title, sf::Style::Close | sf::Style::Titlebar | sf::Style::Resize);
    SplashState* splashState = new SplashState(this->data);
    //_data->machine.AddState(StateRef(new SplashState (this->_data)));
    data->machine.AddState(StateRef(splashState));

    this->Run();
}



void Game::Run() {
    float newTime, frameTime, interpolation;

    float currentTime = this->clock.getElapsedTime().asSeconds();
    float accumulator = 0.0f;

    while (data->window.isOpen()){

        std::this_thread::sleep_for (std::chrono::milliseconds(20));

        data->machine.ProcessStateChanges();

        newTime = this->clock.getElapsedTime().asSeconds();
        frameTime = newTime - currentTime;

        if (frameTime > 0.25f){ //0.25
            frameTime = 0.25f;
        }

        currentTime = newTime;
        accumulator += frameTime;

        while (accumulator >= dt){
            this->data->machine.GetActiveState()->handleInput();
            this->data->machine.GetActiveState()->update(dt);

            accumulator -= dt;
        }

        interpolation = accumulator / dt;
        this->data->machine.GetActiveState()->draw(interpolation);
    }
}


