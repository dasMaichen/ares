//
// Created by das_maichen on 05.06.18.
//

#ifndef MYGAME_ITEM_H
#define MYGAME_ITEM_H


#include "Game.h"

class Item {

public:
    Item(GameDataRef data);
    ~Item(){};

    void inizialize(bool exist, bool activate, bool picked, bool inventory, std::string name, std::string type, sf::Vector2f position, int roomNo);
    void update();
    void draw();

    void setPicked(bool isPicked){picked = isPicked;};
    bool getActivate(){ return activate;};
    void setActivate(bool activate){this->activate = activate;};
    void setPosition(sf::Vector2f position){this->position = position;};
    sf::Vector2f getItemPosition(){ return sf::Vector2f(position.x + 25, position.y + 25);};
    bool getInventory(){ return inventory;};
    std::string& getType(){ return type;};

    sf::Sprite& getSprite(){ return this->sprite;};
    void setSprite(sf::Sprite sprite){this->sprite = sprite;}

    std::string& getName(){ return name;};
    void setName(std::string name){this->name = name;};
    bool setDebugModus(bool modus){debugModus = modus;};

    int getRoomNo(){ return this->roomNo;};

    bool getPicked(){ return picked;};

    void setExist(bool exist){ this->exist = exist;};
    bool getExist(){ return this->exist;};




private:
    GameDataRef data;

    sf::Sprite sprite;
    sf::Vector2f position;
    bool exist;
    bool activate;
    bool picked;
    bool inventory;
    std::string type;
    int roomNo;

    bool debugModus;
    sf::RectangleShape collidingBox;



public: std::string name;


};


#endif //MYGAME_ITEM_H
