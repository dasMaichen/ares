//
// Created by das_maichen on 20.06.18.
//

#ifndef MYGAME_SPEECHBUBBLE_H
#define MYGAME_SPEECHBUBBLE_H


#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "Game.h"

class Speechbubble {

public:
    Speechbubble(GameDataRef data, sf::View& view);
    ~Speechbubble();

    void draw();
    void inizializeTextBox(sf::Vector2f position, sf::Vector2f size, std::string text);
    void inizializeCharacterSpeechBox(sf::View& view, std::string text, sf::Sprite characterImage, std::string name);
    void setText(std::string text);
    void setTextSize(int charSize){this->text.setCharacterSize(charSize);}
    void setPosition(sf::Vector2f viewCenter);
    void setCharacterName(std::string name){characterName.setString(name);};
    void setCharacterImage(std::string filename){character.setTexture(this->data->assets.getTexture(filename));};

private:
    GameDataRef data;
    sf::RectangleShape textBox;
    sf::Sprite speechBox;
    sf::Sprite character;
    sf::RectangleShape characterBox;

    sf::Text text;
    sf::Text characterName;

    bool characterSpeech;

};


#endif //MYGAME_SPEECHBUBBLE_H
