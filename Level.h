//
// Created by das_maichen on 05.06.18.
//

#ifndef MYGAME_LEVEL_H
#define MYGAME_LEVEL_H


#include "Plattforms.h"
#include "Item.h"
#include "Enemy.h"

class Level {

public:
    Level(GameDataRef data, bool load);
    ~Level();

    void inizialize();
    void update();
    void drawLayer1();
    void drawLayer2();
    void drawLayer3();

    Plattforms* getPlattforms();
    //std::map<std::string, Item*> getItem(){ return items;};
    std::vector<Item*>& getGegenstaende(){ return gegenstaende;};
    std::vector<Enemy*>& getEnemies(){ return enemies;};

    void setRoomNo(int no){this->roomNo = no;};
    int getRoomNo(){ return roomNo;};

    void setForegroundPosition(sf::Vector2f position){
        this->actualRoomForeground.setPosition(position);
    }

    void setRoom6Torch(bool torchOn){
        this->torchRoom6 = torchOn;
    }

private:
    GameDataRef data;
    Plattforms* plattforms;
    bool load;

    Plattforms* plattformsRoom1;
    Plattforms* plattformsRoom2;
    Plattforms* plattformsRoom3;
    Plattforms* plattformsRoom4;
    Plattforms* plattformsRoom5;
    Plattforms* plattformsRoom6;
    Plattforms* plattformsRoom7;
    Plattforms* plattformsRoom8;
    //std::vector<Item> items;

    sf::Sprite actualRoom;
    sf::Sprite actualRoomForeground;
    sf::Sprite room1;
    sf::Sprite room1Foreground;
    sf::Sprite room2;
    sf::Sprite room2Foreground;
    sf::Sprite room3;
    sf::Sprite room3Foreground;
    sf::Sprite room4;
    sf::Sprite room4Foreground;
    sf::Sprite room5;
    sf::Sprite room5Foreground;
    sf::Sprite room6;
    sf::Sprite room6Foreground;
    sf::Sprite room6ForegroundTorch;
    sf::Sprite room7;
    sf::Sprite room7Foreground;
    sf::Sprite room8;
    sf::Sprite room8Foreground;


    //std::map<std::string, Item*> items;
    std::vector<Item*> gegenstaende;
    std::vector<Enemy*> enemies;
    //Item* items;
    int roomNo;

    bool torchRoom6;




    void setItems(std::string filename);
    void setEnemies(std::string filename);

};


#endif //MYGAME_LEVEL_H
