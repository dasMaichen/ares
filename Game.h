//
// Created by das_maichen on 09.05.18.
//

#ifndef MYGAME_GAME_H
#define MYGAME_GAME_H

#include <memory>
#include <string>
#include <SFML/Graphics.hpp>
#include "States/StateMachine.h"
#include "Managers/AssetManager.h"
#include "Managers/InputManager.h"

struct GameData
    {
        StateMachine machine;
        sf::RenderWindow window;
        AssetManager assets;
        InputManager input;
    };

typedef std::shared_ptr<GameData> GameDataRef;

class Game {
public:
    Game(int width, int height, std::string title);

private:
    // Updates run at 60 per second.
    const float dt = 1.0f / 60.0f;
    sf::Clock clock;

    GameDataRef data = std::make_shared<GameData>();

    void Run();
};


#endif //MYGAME_GAME_H
