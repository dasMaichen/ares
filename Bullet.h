//
// Created by das_maichen on 07.06.18.
//

#ifndef MYGAME_BULLET_H
#define MYGAME_BULLET_H


#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "Game.h"

class Bullet {
public:
    Bullet(GameDataRef data);
    ~Bullet();

    void inizialize(sf::Vector2f startposition, sf::Vector2f direction);
    sf::RectangleShape& getCore(){ return core;};
    sf::Sprite& getSprite(){ return sprite;};

    void draw();
    void update();
    void setSprite(sf::Sprite sprite){this->sprite = sprite;};

    void calculateNextPosition();


private:
    GameDataRef data;
    sf::RectangleShape core;
    sf::Texture texture;
    sf::Sprite sprite;
    sf::Vector2f direction;
    sf::Vector2f startposition;
    sf::Vector2f nextPosition;
    sf::Vector2f actualPosition;


};


#endif //MYGAME_BULLET_H
