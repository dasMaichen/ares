//
// Created by das_maichen on 12.05.18.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include "Plattforms.h"
#include "DEFINITIONS.h"

Plattforms::Plattforms(GameDataRef data) : data(data){

}

void Plattforms::drawPlattforms() {

    for (unsigned short int i = 0; i < plattformSprite.size(); i++){
        if(debugModus){
            this->data->window.draw(plattformBody.at(i));
        }
        //this->data->window.draw(plattformSprite.at(i));
        //
    }

}

void Plattforms::setPlattforms(float width, float height) {

    debugModus = false;

    sf::Sprite sprite(this->data->assets.getTexture("Plattform"));
    sf::Vector2f size;
    size.x = sprite.getGlobalBounds().width;
    size.y = sprite.getGlobalBounds().height;

    sprite.setOrigin(size/2.0f);
    sprite.setPosition(width,height);

    sf::RectangleShape body(size);
    body.setOrigin(size/2.0f);
    body.setPosition(width-size.x/2,height-size.y/2);
    body.setFillColor(sf::Color(255,255,255,128));

    plattformBody.push_back(body);

    //body.setPosition(width,height);

    //sf::Vector2f size;
    //size.x = sprite.getGlobalBounds().width;
    //size.y = sprite.getGlobalBounds().height;

    //body.setSize(size);
    //plattformBody.push_back(body);
    plattformSprite.push_back(sprite);



}
/*
void Plattforms::movePlattforms(float dt) {

    for (unsigned short int i = 0; i < plattformSprite.size(); i++)
    {
        if (plattformSprite.at(i).getPosition().x < 0 - plattformSprite.at(i).getLocalBounds().width)
        {
            plattformSprite.erase( plattformSprite.begin( ) + i );
        }
        else
        {
            sf::Vector2f position = plattformSprite.at(i).getPosition();
            float movement = PLATTFORM_SPEED * dt;

            plattformSprite.at(i).move(-movement, 0);

        }
    }
}

 */
void Plattforms::loadPlattformPosition(std::string filePath) {
    std::ifstream infile(filePath);

    if(!infile.good()){
        std::cerr << "could not open file: " << filePath << std::endl;
        return;
    }

    /*
    if (std::getline(infile, line)) {
        filename = line;
    }
     */

    std::string output_string;

    while (std::getline(infile, output_string)){


        std::string coordinates(output_string);
        std::string::size_type size;

        std::string delimiter = " ";

        size_t pos = 0;
        std::string x_component;
        std::string y_component;
        while ((pos = coordinates.find(delimiter)) != std::string::npos) {
            x_component = coordinates.substr(0, pos);
            y_component = coordinates.erase(0, pos + delimiter.length());
        }

        float x = std::stof (x_component,&size);
        float y = std::stof (y_component,&size);

        sf::Sprite sprite;
        setPlattforms(x,y);

        //plattformSprite.push_back(sprite);
    }

}

