//
// Created by das_maichen on 16.05.18.
//

#include <fstream>
#include <iostream>
#include "Obstacle.h"

Obstacle::Obstacle(GameDataRef data) : data(data) {

}

void Obstacle::drawObstacle() {

    for (unsigned short int i = 0; i < obstacleSprites.size(); i++){
        this->data->window.draw(obstacleSprites.at(i));
        if(debugModus){
            this->data->window.draw(obstacleBody.at(i));
        }
    }
}

void Obstacle::setObstacle(float width, float height, std::string spriteName) {

    debugModus = false;

    sf::Sprite sprite(this->data->assets.getTexture(spriteName));
    sf::Vector2f size;
    size.x = sprite.getGlobalBounds().width;
    size.y = sprite.getGlobalBounds().height;

    sprite.setOrigin(size/2.0f);
    sprite.setPosition(width+size.x/2,height-size.y/2);

    sf::RectangleShape body(size);
    body.setOrigin(size/2.0f);
    body.setPosition(width+size.x/2,height-size.y/2);
    body.setFillColor(sf::Color(255,255,255,128));

    obstacleBody.push_back(body);

    //body.setPosition(width,height);

    //sf::Vector2f size;
    //size.x = sprite.getGlobalBounds().width;
    //size.y = sprite.getGlobalBounds().height;

    //body.setSize(size);
    //plattformBody.push_back(body);
    obstacleSprites.push_back(sprite);

}

void Obstacle::loadObstaclePosition(std::string filePath) {
    std::ifstream infile(filePath);

    if(!infile.good()){
        std::cerr << "could not open file: " << filePath << std::endl;
        return;
    }

    /*
    if (std::getline(infile, line)) {
        filename = line;
    }
     */

    std::string output_string;

    while (std::getline(infile, output_string)){


        std::string coordinates(output_string);
        std::string::size_type size;

        std::string delimiter = " ";

        size_t pos = 0;
        std::string zwischenprodukt;
        std::string x_component;
        std::string y_component;
        std::string spriteTexture;
        while ((pos = coordinates.find(delimiter)) != std::string::npos) {
            //x_component = coordinates.substr(0, pos);
            //y_component = coordinates.erase(0, pos + delimiter.length());

            x_component = coordinates.substr(0, pos);
            zwischenprodukt = coordinates.erase(0,pos+delimiter.length());
            while ((pos = coordinates.find(delimiter)) != std::string::npos){
                y_component = coordinates.substr(0,pos);
                spriteTexture = coordinates.erase(0, pos + delimiter.length());
            }


        }

        float x = std::stof (x_component,&size);
        float y = std::stof (y_component,&size);

        setObstacle(x,y,spriteTexture);
    }

}
