//
// Created by das_maichen on 23.05.18.
//

#include "Lives.h"
#include "DEFINITIONS.h"

Lives::Lives(GameDataRef data) : data(data){

    this->data->assets.loadTexture("Herz",HEART_FILEPATH);

    for (int i = 0; i < 3; i++) {
        liveSprite.setTexture(this->data->assets.getTexture("Herz"));
        lives.push_back(liveSprite);

        //lives.at(i).setPosition(550+(50*(i+1)),50);
    }

    numberOfLives = 3;

    //lives.at(0).setPosition(400,100);

}


void Lives::countDown() {
    numberOfLives = numberOfLives - 1;
}

void Lives::countUp() {
    numberOfLives = numberOfLives + 1;
}

void Lives::draw() {
    for (unsigned short int i = 0; i < lives.size(); i++){
            this->data->window.draw(lives.at(i));
    }

}


