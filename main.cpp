#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
//#include "GameProbe.h"
#include "DEFINITIONS.h"
#include "Game.h"

/*
static  const float VIEW_HEIGHT = 600.0f;

void resizeView(const sf::RenderWindow& window, sf::View& view){
    float aspectRatio = float(window.getSize().x)/float(window.getSize().y);
    view.setSize(VIEW_HEIGHT * aspectRatio, VIEW_HEIGHT);
}


int main()
{
    sf::Music titleMusic;

    if(!titleMusic.openFromFile("Content/Music/titleMusic.ogg")){
        std::cout << "Fehler beim Musik laden." << std::endl;
    }

    titleMusic.setLoop(true);
    // create the window
    sf::RenderWindow window(sf::VideoMode(800, 600), "My Game");

    sf::View view(sf::Vector2f(0.0f, 0.0f),sf::Vector2f(VIEW_HEIGHT,VIEW_HEIGHT));

    MainMenu mainMenu(window.getSize().x, window.getSize().y);

    float deltaTime = 0.0f;
    sf::Clock clock;

    Game *game = new Game;





    titleMusic.play();

    while (window.isOpen())
    {

        deltaTime = clock.restart().asSeconds();

        if(deltaTime > 1.0f/20.0f){
            //deltaTime = 1.0f/20.0f;
        }
        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (window.pollEvent(event))
        {

            switch (event.type){
                case sf::Event::Closed:
                    window.close();
                    break;
                case sf::Event::Resized:
                    resizeView(window,view);
                    break;

                case sf::Event::KeyReleased:
                    switch(event.key.code){
                        case sf::Keyboard::Up:
                            mainMenu.moveUp();
                            break;
                        case sf::Keyboard::Down:
                            mainMenu.moveDown();
                            break;

                        case sf::Keyboard::Return:
                            switch (mainMenu.getChosenItem() && mainMenu.getVisibility()){
                                case 0:
                                    std::cout << "Play!" << std::endl;
                                    mainMenu.setVisibility(false);
                                    game->inizialize(window);

                                    break;
                                case 1:
                                    std::cout << "Option!" << std::endl;
                                    break;
                                case 2:
                                    std::cout << "Quit!" << std::endl;
                                    window.close();
                                    break;
                            }
                    }
                    break;
            }

        }

        window.clear(sf::Color(200,200,200));


        //view.setCenter(window.getSize().x/2, window.getSize().y/2);
       // window.setView(view);

        mainMenu.draw(window);
        game->draw(window);


        window.display();
    }

    return 0;
}

 */

int main(){

    Game game(SCREEN_WIDTH, SCREEN_HEIGHT, "Ares");

    return 1;
}