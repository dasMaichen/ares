//
// Created by das_maichen on 10.04.18.
//

#ifndef MYGAME_ANIMATION_H
#define MYGAME_ANIMATION_H


#include <SFML/Window.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>

class Animation {
public:
    //Animation(const std::string &animationTexture, sf::Vector2u imageCount, float switchTime);
    Animation(sf::Texture texture, sf::Vector2u imageCount, float switchTime);
    ~Animation();

    void update(int row, float deltaTime, bool faceRight);
    void setSwitchTime(float time){switchTime = time;};



public:
    sf::IntRect uvRect;

private:
    sf::Vector2u imageCount;
    sf::Vector2u currentImage;

    sf::Texture texture;

    float totalTime;
    float switchTime;

};


#endif //MYGAME_ANIMATION_H
