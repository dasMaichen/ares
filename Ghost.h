//
// Created by das_maichen on 07.08.18.
//

#ifndef MYGAME_GHOST_H
#define MYGAME_GHOST_H


#include "Game.h"
#include "Animation.h"

class Ghost {

public:
    Ghost(GameDataRef data, sf::Texture &playerTexture, sf::Vector2u imageCount, float switchTime, float speed, int roomNo, sf::Vector2f position,
          bool exist);
    ~Ghost();

    void update(float deltaTime);
    void calculateNextPosition(sf::Event event);
    void draw(sf::RenderWindow& window);

    sf::Vector2f getNextPosition(){ return nextPosition;};


    sf::Vector2f getPosition(){return body.getPosition();};
    // void setPosition(sf::Vector2f newPosition){position = newPosition; };


    sf::RectangleShape& getCollisionsBox(){ return collisionsBox;};
    void setCollisionBoxPosition(sf::Vector2f position){
        collisionsBox.setPosition(position);
    };

    sf::Vector2f getTargetPosition(){
        return targetPosition;
    };
    void setTargetPosition(sf::Vector2f pos){
        targetPosition = pos;
    };
    void setActualPosition(sf::Vector2f pos){
        actualPosition = pos;
    };
    void setNextPosition(sf::Vector2f pos){
        nextPosition = pos;
    };

    void setDebugModus(bool modus){
        debugModus = modus;
    };

    bool getAnimating(){ return animating;};
    void animate(float dt);

    sf::Vector2f getDirection(){ return this->direction;};
    int getRoomNo(){ return this->roomNo;};

    bool getExist(){ return this->exist;};
    void setExist(bool exist){this->exist = exist;};



private:

    GameDataRef data;
    sf::RectangleShape body;
    sf::RectangleShape collisionsBox;
    sf::Vector2f targetPosition;
    sf::Vector2f actualPosition;
    int roomNo;
    bool exist;

    bool animating;

    Animation animation;
    unsigned int row;
    bool faceRight;
    sf::Vector2f nextPosition;

    sf::Vector2f direction;

    sf::Texture texture;

    bool debugModus;

};


#endif //MYGAME_GHOST_H
