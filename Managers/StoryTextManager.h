//
// Created by das_maichen on 30.05.18.
//

#ifndef MYGAME_STORYTEXTMANAGER_H
#define MYGAME_STORYTEXTMANAGER_H


#include <SFML/Graphics/Texture.hpp>
#include <map>
#include <SFML/Graphics/Text.hpp>

class StoryTextManager {

    void loadText(std::string fileName);
    sf::Text &getText(int id);
    void readTextFile(std::string filename);

private:
    std::map<int, sf::Text> texts;
};


#endif //MYGAME_STORYTEXTMANAGER_H
