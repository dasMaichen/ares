//
// Created by das_maichen on 09.05.18.
//

#ifndef MYGAME_INPUTMANAGER_H
#define MYGAME_INPUTMANAGER_H


#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

class InputManager
    {
    public:
        InputManager() {}
        ~InputManager() {}

        bool IsSpriteClicked(sf::Sprite object, sf::Mouse::Button button, sf::RenderWindow &window);

        sf::Vector2i GetMousePosition(sf::RenderWindow &window);
    };


#endif //MYGAME_INPUTMANAGER_H
