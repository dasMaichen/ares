//
// Created by das_maichen on 09.05.18.
//

#ifndef MYGAME_ASSETMANAGER_H
#define MYGAME_ASSETMANAGER_H

#include <map>
#include <SFML/Graphics.hpp>

class AssetManager{
public:
    //AssetManager() : _textures() { }
    //~AssetManager() { }

    void loadTexture(std::string name, std::string fileName);
    sf::Texture &getTexture(std::string name);

    void loadFont(std::string name, std::string fileName);
    sf::Font &getFont(std::string name);


private:
    std::map<std::string, sf::Texture> textures;
    std::map<std::string, sf::Font> fonts;
};


#endif //MYGAME_ASSETMANEGER_H
