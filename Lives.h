//
// Created by das_maichen on 23.05.18.
//

#ifndef MYGAME_LIVES_H
#define MYGAME_LIVES_H


#include "Game.h"

class Lives {

public:
    Lives(GameDataRef data);
    //~Lives();

    void draw();
    void countDown();
    void countUp();


    sf::Sprite& getSprite(int position){ return lives.at(position);};

    std::vector<sf::Sprite>& getLives(){ return lives;};
    void setPosition(sf::Vector2f position){
        for(int i = 0; i < lives.size(); i++){
            lives.at(i).setPosition(position.x + (i*50),position.y);
        }
    }

    void setTransparency(int countRemainingLives){
        for(int i = 0; i < countRemainingLives; i++){
            lives.at(i).setColor(sf::Color(255,255,255,100));
        }
    }

    int getNumberOfLives(){ return numberOfLives;};

private:
    GameDataRef data;
    int numberOfLives;
    std::vector<sf::Sprite> lives;
    sf::Sprite liveSprite;
    sf::Texture texture;
};


#endif //MYGAME_LIVES_H
