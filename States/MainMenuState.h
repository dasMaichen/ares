//
// Created by das_maichen on 09.05.18.
//

#ifndef MYGAME_MAINMENUSTATE_H
#define MYGAME_MAINMENUSTATE_H


#include <SFML/Graphics.hpp>
#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Sound.hpp>
#include "BasicState.h"
#include "../Game.h"
#include "../OptionBox.h"

class MainMenuState : public BasicState{

public:
    MainMenuState(GameDataRef data);

    void inizialize();

    void handleInput();
    void update(float dt);
    void draw(float dt);

    void moveUp();
    void moveDown();
    int getChosenItem(){ return selectedItemIndex;};

private:
    GameDataRef data;

    sf::Music music;
    sf::SoundBuffer buffer;
    sf::Sound buttonSound;
    sf::SoundBuffer bufferChoseOption;
    sf::Sound choseSound;
    sf::SoundBuffer bufferCloseOption;
    sf::Sound closeOption;

    sf::Texture backgroundTexture;
    sf::Sprite background;
    sf::Sprite titleSprite;
    sf::Sprite playButton;
    sf::Sprite loadButton;
    sf::Sprite optionButton;
    sf::Sprite quitButton;
    sf::Sprite ares;
    sf::Text title;

    sf::View view;

    int selectedItemIndex;
    sf::Text menuText[MAX_NUMBER_OF_ITEMS_MAINMENU];

    OptionBox* optionBox;

    bool goToStory;
    bool adventurePart;
    bool optionOpen;
    bool goToStory2;
    bool load;

    bool musicOn;
    bool soundOn;
    int difficulityLevel;
    bool musicIsTurnedOff;

};


#endif //MYGAME_MAINMENUSTATE_H
