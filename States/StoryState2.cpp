//
// Created by das_maichen on 25.05.18.
//

#include <iostream>
#include "StoryState2.h"
#include "../DEFINITIONS.h"
#include "AdventureState.h"

void resizeViewStory2(const sf::RenderWindow& window, sf::View& view){
    float aspectRatio = float(window.getSize().x)/float(window.getSize().y);
    view.setSize(SCREEN_HEIGHT * aspectRatio, SCREEN_HEIGHT);
}

StoryState2::StoryState2(GameDataRef data)
        : data(data), view(sf::FloatRect(0,0,SCREEN_WIDTH, SCREEN_HEIGHT)){

}

void StoryState2::inizialize() {
    resizeViewStory2(this->data->window, view);

    readyToPlay = false;
    musicOn = true;

    //resizeViewStory1(this->data->window, view);

    speechbubble = new Speechbubble(this->data, view);
    speechbubble->inizializeTextBox(sf::Vector2f(50, 420), sf::Vector2f(700, 150), "Mist, die sind immer noch hinter mir her... "
    );

    readyToPlay = false;
    nextPage = false;
    pageNo = 1;
    maxPage = 2;

    if(!music.openFromFile("Content/Music/tumbling-down.ogg")){
        //if(!music.openFromFile("Content/Music/lost-in-the-forest.ogg")){
        std::cout << "Fehler beim Musik laden." << std::endl;
    }

    music.setLoop(true);
    if(musicOn){
        music.play();
    }


    bufferNextPage.loadFromFile("Content/Sounds/options.wav");
    nextPageSound.setBuffer(bufferNextPage);

    this->data->assets.loadTexture("Story2_1", "Content/ares_story2_1.png");
    this->data->assets.loadTexture("Story2_2", "Content/ares_story2_2.png");
    storyImage.setTexture(this->data->assets.getTexture("Story2_1"));
    storyImage.setScale(0.35,0.35);
    //storyImage.setScale(0.3,0.3);
    //this->data->assets.loadTexture("Story_2", STORY2_FILEPATH);
    //this->data->assets.loadTexture("Story_3", STORY3_FILEPATH);
    //this->data->assets.loadTexture("Story_4", STORY4_FILEPATH);

    storyImage.setPosition(-50,0);

    //this->data->assets.loadTexture("NextButton", STORY_NEXT_BUTTON_FILEPATH);

}

void StoryState2::handleInput() {
        sf::Event event;

        while (this->data->window.pollEvent(event)) {
            switch (event.type){
                case sf::Event::Closed:
                    this->data->window.close();
                    break;
                case sf::Event::Resized:
                    resizeViewStory2(this->data->window,view);
                case sf::Event::KeyReleased:
                    switch(event.key.code){
                        case sf::Keyboard::N:
                            readyToPlay = true;
                            break;
                        case sf::Keyboard::Space:
                            //goToNextScene();
                            nextPage = true;
                            pageNo = pageNo +1;
                            break;
                    }
                    break;
            }
        }

}

void StoryState2::update(float dt) {

    if(pageNo > maxPage){
        readyToPlay = true;
    }

    if(readyToPlay){
        AdventureState* gameStateAdventure = new AdventureState(this->data, difficulity, musicOn, soundOn, false);

        data->machine.AddState(StateRef(gameStateAdventure), true);

        music.stop();

        readyToPlay = false;
    }

    if(nextPage && pageNo <= maxPage){

        if(soundOn){
            nextPageSound.play();
        }
        std::string filename = "Story2_" + std::to_string(pageNo);
        storyImage.setTexture(this->data->assets.getTexture(filename));
        //storyImage.setColor(sf::Color(255,255,255,200));
        nextPage = false;
    }

    if(pageNo == 2){
        speechbubble->setText("Krach! \n"
                                      "Wah, der Boden bricht ein! ");
        speechbubble->setTextSize(30);

    }

}

void StoryState2::draw(float dt) {
    this->data->window.clear(sf::Color::Black);
    this->data->window.setView(view);


    this->data->window.draw(storyImage);
    speechbubble->draw();

    this->data->window.display();

}

