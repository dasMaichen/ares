//
// Created by das_maichen on 09.05.18.
//

#ifndef MYGAME_BASICSTATE_H
#define MYGAME_BASICSTATE_H
class BasicState{
public:
    virtual void inizialize() = 0;

    virtual void handleInput() = 0;
    virtual void update(float dt) = 0;
    virtual void draw(float dt) = 0;

    virtual void pause() { };
    virtual void resume() { }
};

#endif //MYGAME_BASICSTATE_H
