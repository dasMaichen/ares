//
// Created by das_maichen on 04.06.18.
//

#include <iostream>
#include <chrono>
#include <fstream>
#include "AdventureState.h"
#include "../DEFINITIONS.h"
#include "../Bullet.h"
#include "TextItem.h"
#include "MainMenuState.h"
#include "StoryState3.h"

void resizeViewAdventure(const sf::RenderWindow& window, sf::View& view){
    float aspectRatio = float(window.getSize().x)/float(window.getSize().y);
    view.setSize(SCREEN_HEIGHT * aspectRatio, SCREEN_HEIGHT);
}

AdventureState::AdventureState(GameDataRef data, int difficulity, bool musicOn, bool soundOn, bool load) :
        data(data), view(sf::FloatRect(0,0,SCREEN_WIDTH, SCREEN_HEIGHT)), arrows(), currentDialog(&dialogs.gameStarted),
        currentDialogIndex(0){

    this->difficulity = difficulity;
    this->musicOn = musicOn;
    this->soundOn = soundOn;
    this->load = load;
}

void AdventureState::inizialize() {

    resizeViewAdventure(data->window,view);

    clearLevel2 = false;

    debugModus = false;
    itemGet = false;
    menuOpen = false;
    //selectedItem = 0;
    speechBubbleOn = true;
    alreadyDialogOpen = false;
    torchDialog = false;
    threeTorchDialog = false;
    fourTorchDialog = false;
    guardianActivate = false;
    room3switchActive = false;
    room3chestOpen = false;
    itemActivate = false;
    enterRoom3 = false;
    bowlOn = false;

    ghost3Defeat = false;

    countSteps = 0;

    //Sounds
    bufferUnlockDoor.loadFromFile("Content/Sounds/door-unlock.wav");
    soundUnlockDoor.setBuffer(bufferUnlockDoor);

    bufferFire.loadFromFile("Content/Sounds/fire.wav");
    soundFire.setBuffer(bufferFire);

    bufferGetItem.loadFromFile("Content/Sounds/get-item.wav");
    soundGetItem.setBuffer(bufferGetItem);

    bufferGhost.loadFromFile("Content/Sounds/ghost.wav");
    soundGhost.setBuffer(bufferGhost);

    bufferOpenDoor.loadFromFile("Content/Sounds/door-open.wav");
    soundOpenDoor.setBuffer(bufferOpenDoor);

    bufferSolved.loadFromFile("Content/Sounds/solved.wav");
    soundSolved.setBuffer(bufferSolved);

    bufferSwitch.loadFromFile("Content/Sounds/switch.wav");
    soundSwitch.setBuffer(bufferSwitch);


    //Item Textures
    this->data->assets.loadTexture("FackelAn", "Content/Texture/torch_on_100.png");
    this->data->assets.loadTexture("FackelAus", "Content/Texture/torch_off_100.png");
    this->data->assets.loadTexture("Item", "Content/heart.png");
    this->data->assets.loadTexture("Feuerstein", "Content/Texture/flintstone_100.png");
    this->data->assets.loadTexture("Crossbow", "Content/Texture/crossbow_100.png");
    this->data->assets.loadTexture("NoItem", "Content/no_item.png");
    this->data->assets.loadTexture("NoItemBig", "Content/Texture/noItemBig.png");
    this->data->assets.loadTexture("ArrowRight", "Content/arrow_probe_rechts.png");
    this->data->assets.loadTexture("ArrowLeft", "Content/arrow_probe_links.png");
    this->data->assets.loadTexture("ArrowUp", "Content/arrow_probe_oben.png");
    this->data->assets.loadTexture("ArrowDown", "Content/arrow_probe_unten.png");
    this->data->assets.loadTexture("StartroomClosedDoor", "Content/Texture/startDoorClosed.png");
    this->data->assets.loadTexture("StartroomDoorOpen", "Content/Texture/startDoorOpen.png");
    this->data->assets.loadTexture("InventarBox", "Content/Texture/MenuTexture/menuBox.png");
    this->data->assets.loadTexture("Stairs", "Content/Texture/stairsDown.png");
    this->data->assets.loadTexture("Switch", "Content/Texture/switch.png");
    this->data->assets.loadTexture("SwitchActive", "Content/Texture/switchActive.png");
    this->data->assets.loadTexture("StairsGarden", "Content/Texture/stairs.png");
    this->data->assets.loadTexture("FenceOpen", "Content/Texture/fenceOpen.png");
    this->data->assets.loadTexture("FenceClosed", "Content/Texture/fenceClosed.png");
    this->data->assets.loadTexture("ChestClosed", "Content/Texture/chestClosed.png");
    this->data->assets.loadTexture("ChestOpen", "Content/Texture/chestOpen.png");
    this->data->assets.loadTexture("GuardianButton", "Content/Texture/guardianWithButton.png");
    this->data->assets.loadTexture("ReliefDoor1", "Content/Texture/reliefDoor1.png");
    this->data->assets.loadTexture("ReliefDoor2", "Content/Texture/reliefDoor2.png");
    this->data->assets.loadTexture("ReliefDoorOpen", "Content/Texture/reliefDoorOpen.png");
    this->data->assets.loadTexture("Talisman", "Content/Texture/talisman.png");
    this->data->assets.loadTexture("Torch", "Content/Texture/torchItem.png");
    this->data->assets.loadTexture("SealedDoor", "Content/Texture/sealedDoor.png");
    this->data->assets.loadTexture("DoorDownLeft", "Content/Texture/doorDownLeft.png");
    this->data->assets.loadTexture("DoorDownRight", "Content/Texture/doorDownRight.png");
    this->data->assets.loadTexture("DoorClosedBig", "Content/Texture/doorClosedBig.png");
    this->data->assets.loadTexture("GhostViolett", "Content/Texture/ghost_violett.png");
    this->data->assets.loadTexture("GhostYellow", "Content/Texture/ghost_yellow.png");
    this->data->assets.loadTexture("GhostBlue", "Content/Texture/ghost_blue.png");
    this->data->assets.loadTexture("GhostGreen", "Content/Texture/ghost_green.png");
    this->data->assets.loadTexture("Bowl", "Content/Texture/bowl.png");
    this->data->assets.loadTexture("BowlViolett", "Content/Texture/bowl_violett.png");
    this->data->assets.loadTexture("BowlBlue", "Content/Texture/bowl_blue.png");
    this->data->assets.loadTexture("BowlYellow", "Content/Texture/bowl_yellow.png");
    this->data->assets.loadTexture("BowlGreen", "Content/Texture/bowl_green.png");
    this->data->assets.loadTexture("EyeOff", "Content/Texture/eye_switch_off.png");
    this->data->assets.loadTexture("EyeOn", "Content/Texture/eye_switch_on.png");
    this->data->assets.loadTexture("FireViolett", "Content/Texture/fire_violett.png");
    this->data->assets.loadTexture("FireYellow", "Content/Texture/fire_yellow.png");
    this->data->assets.loadTexture("FireBlue", "Content/Texture/fire_blue.png");
    this->data->assets.loadTexture("FireGreen", "Content/Texture/fire_green.png");

    this->data->assets.loadTexture("ActiveItemButton", "Content/active_item.png");
    this->data->assets.loadTexture("Key", "Content/Texture/key.png");
    this->data->assets.loadFont("Number", "Content/Fonts/Palermo-Bold.ttf");

    this->data->assets.loadTexture("LevelBackground", "Content/level2.png");
    this->data->assets.loadTexture("Foreground", "Content/adventure_vordergrund.png");
    this->data->assets.loadTexture("CharacterAres", "Content/Ares_Passfoto_small.png");
    this->data->assets.loadTexture("Hermes", "Content/Texture/hermes.png");
    this->data->assets.loadTexture("AresHurt", "Content/Texture/ares_hurt.png");
    this->data->assets.loadTexture("AresSmile", "Content/Texture/ares_smile.png");
    this->data->assets.loadTexture("AresScared", "Content/Texture/ares_scared.png");
    this->data->assets.loadTexture("HermesEye", "Content/Texture/hermes_eye.png");




    this->data->assets.loadTexture("NoTorches", "Content/Texture/levelTexture/noTorches.png");
    this->data->assets.loadTexture("OneTorch", "Content/Texture/levelTexture/oneTorchOn.png");
    this->data->assets.loadTexture("TwoTorchesParallel", "Content/Texture/levelTexture/twoTorchesParallel.png");
    this->data->assets.loadTexture("TwoTorchesDiagonal", "Content/Texture/levelTexture/twoTorchesDiagonal.png");
    this->data->assets.loadTexture("ThreeTorches", "Content/Texture/levelTexture/threeTorches.png");
    this->data->assets.loadTexture("AllTorches", "Content/Texture/levelTexture/allTorches.png");

    fackelAnSprite.setTexture(this->data->assets.getTexture("NoTorches"));
    fackelAnSprite.setOrigin(fackelAnSprite.getGlobalBounds().width/2,fackelAnSprite.getGlobalBounds().height/2);
    fackelAnSprite.setPosition(650/2-150,650/2);

    if(!music.openFromFile("Content/Music/hidden-monster_vers2.ogg")){
        std::cout << "Fehler beim Musik laden." << std::endl;
    }

    music.setVolume(30);
    music.setLoop(true);
    if(musicOn){
        music.play();
    }

    //this->data->assets.loadTexture("AresAnimationAdventure", ADVENTURE_ARES_ANIMATION);
    this->data->assets.loadTexture("AresAnimationAdventure", "Content/Texture/ares_animation_sprites.png");
    //player = new PlayerAdventure(data,this->data->assets.getTexture("AresAnimationAdventure"), sf::Vector2u(6, 6), 0.08f, 200);
    player = new PlayerAdventure(data,this->data->assets.getTexture("AresAnimationAdventure"), sf::Vector2u(6, 6), 0.14f, 200);
    ghost1 = new Ghost(data, this->data->assets.getTexture("GhostViolett"), sf::Vector2u(1,1), 0.8f, 200, 3, sf::Vector2f(300,300), true);
    ghost2 = new Ghost(data, this->data->assets.getTexture("GhostYellow"), sf::Vector2u(1,1), 0.8f, 200, 5, sf::Vector2f(300,300), true);
    ghost3 = new Ghost(data, this->data->assets.getTexture("GhostBlue"), sf::Vector2u(1,1), 0.8f, 200, 7, sf::Vector2f(300,250), false);
    ghost4 = new Ghost(data, this->data->assets.getTexture("GhostGreen"), sf::Vector2u(1,1), 0.8f, 200, 8, sf::Vector2f(1000,300), true);

    level = new Level(data, load);
    level->inizialize();


    inventar = new Inventar(data);
    inventar->setPosition(view.getCenter());

    if(load){
        level->setRoomNo(7);
        ghost1->setExist(false);
        ghost2->setExist(false);

        player->setActualPosition(sf::Vector2f(175,275));
        player->setTargetPosition(sf::Vector2f(175,275));
        player->setNextPosition(sf::Vector2f(175,275));

        inventar->setGetTalisman(true);
        inventar->setGetTorch(true);
        inventar->setGetKey(true);
        inventar->setGetFlintstone(true);
    }

    view.setCenter(player->getPosition());

    activeItemSprite = inventar->getChosenItem()->getSprite();
    activeItemButton.setTexture(this->data->assets.getTexture("ActiveItemButton"));
    activeItemButton.setPosition(view.getCenter().x + 150, view.getCenter().y - 300);

    speechBoxCharacterSprite.setTexture(this->data->assets.getTexture("AresHurt"));
    speechbubble = new Speechbubble(this->data, this->view);
    speechbubble->inizializeCharacterSpeechBox(this->view, "Autsch, das tat weh... Wo bin ich hier? Ganz schoen finster...  ", speechBoxCharacterSprite, "Ares");

    currentPlayerDirection = player->getDirection();
    currentPlayerPosition = player->getPosition();

    if(load){
        currentDialog = updateDialog(player->getCollisionsBox().getPosition(), player->getDirection());
        currentDialogIndex = 0;
        updateSpeechbubble();

    }
    /*
    keySprite.setTexture(this->data->assets.getTexture("Key"));
    keySprite.setScale(0.5,0.5);
    keySprite.setPosition(view.getCenter().x + 220, view.getCenter().y - 110);

    keyText.setFont(this->data->assets.getFont("Number"));
    keyText.setCharacterSize(30);
    keyText.setColor(sf::Color::White);
    //keyText.setPosition(view.getCenter().x + 380, view.getCenter().y - 100);
    keyText.setString("0");
     */

}



void AdventureState::handleInput() {

    sf::Event event;


    while (this->data->window.pollEvent(event)) {

        switch (event.type) {
            case sf::Event::Closed:
                this->data->window.close();
                break;
            case sf::Event::Resized:
                resizeViewAdventure(this->data->window, view);
                break;
            case sf::Event::KeyReleased:
                switch (event.key.code) {
                    case sf::Keyboard::M:
                        if (menuOpen) {
                            menuOpen = false;
                        } else {
                            menuOpen = true;
                            if(inventar->getChosenItem()->name == "Crossbow"){
                                //inventar->setChosenItemPosition(sf::Vector2f(view.getCenter().x - 50, view.getCenter().y));
                            } else{
                                //inventar->setChosenItemPosition(view.getCenter());
                            }
                        }
                        break;
                }
                break;
        }

        if(menuOpen){
            inventar->chooseItem(event);
            return;
        }


        if (!player->getAnimating()) {
            switch (event.type){
                case sf::Event::KeyReleased:
                    switch (event.key.code) {
                        case sf::Keyboard::D:
                            if (debugModus) {
                                debugModus = false;
                            } else {
                                debugModus = true;
                            }
                            level->getPlattforms()->setDebugModus(debugModus);
                            player->setDebugModus(debugModus);
                            for(int i = 0; i < level->getGegenstaende().size(); i++){
                                level->getGegenstaende().at(i)->setDebugModus(debugModus);
                            }
                            break;

                        case sf::Keyboard::B:
                            if(inventar->getChosenItem()->name == "Crossbow") {
                                Bullet *bullet = new Bullet(data);
                                bullet->inizialize(sf::Vector2f(player->getCollisionsBox().getPosition().x-25,
                                                                player->getPosition().y - 25),
                                                   sf::Vector2f(player->getDirection().x * (-1),
                                                                player->getDirection().y));
                                arrows.push_back(bullet);
                            }
                            break;
                        case sf::Keyboard::Space:
                            if (currentDialog != nullptr) {
                                currentDialogIndex += 1;
                            }
                            itemActivate = true;
                            updateSpeechbubble();
                            break;
                    }
                    break;
                case sf::Event::GainedFocus:
                    // do nothing
                    break;
                case sf::Event::KeyPressed:
                    if (!speechBubbleOn) {
                        if(level->getRoomNo() == ghost1->getRoomNo()){
                            ghost1->calculateNextPosition(event);
                            sf::Vector2f nextGhost1Position = ghost1->getNextPosition();
                            ghost1->setTargetPosition(nextGhost1Position);
                        }
                        if(level->getRoomNo() == ghost2->getRoomNo()){
                            ghost2->calculateNextPosition(event);
                            sf::Vector2f nextGhostPosition = ghost2->getNextPosition();
                            ghost2->setTargetPosition(nextGhostPosition);
                        }
                        if(level->getRoomNo() == ghost3->getRoomNo() && ghost3->getExist()){
                            ghost3->calculateNextPosition(event);
                            sf::Vector2f nextGhostPosition = ghost3->getNextPosition();
                            ghost3->setTargetPosition(nextGhostPosition);
                        }
                        if(level->getRoomNo() == ghost4->getRoomNo()){
                            ghost4->calculateNextPosition(event);
                            sf::Vector2f nextGhostPosition = ghost4->getNextPosition();
                            ghost4->setTargetPosition(nextGhostPosition);
                        }

                        sf::Vector2f positionNow = player->getPosition();
                        player->calculateNextPosition(event);
                        sf::Vector2f nextPlayerPosition = player->getNextPosition();

                        bool collide = false;

                        for (int i = 0; i < level->getPlattforms()->getSprites().size(); i++) {
                            if (nextPlayerPosition == level->getPlattforms()->plattformBody.at(i).getPosition()) {
                                collide = true;
                            }
                        }
                        for (int i = 0; i < level->getGegenstaende().size(); i++) {
                            Item* item = level->getGegenstaende().at(i);
                            if ((item->getType() == "Door"
                                 || item->getType() == "Fence"
                                 || item->getType() == "Exit"
                                 || item->getType() == "Stairs")
                                && item->getRoomNo() == level->getRoomNo() && item->getExist()
                                &&!item->getActivate() && item->getItemPosition() == nextPlayerPosition){
                                collide = true;
                            }
                        }

                        for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                            auto item = level->getGegenstaende().at(j);
                            if((item->getType() == "Chest"||item->getType() == "Bowl")
                               && item->getExist() && item->getRoomNo() == level->getRoomNo()
                               && sf::Vector2f(item->getItemPosition().x, item->getItemPosition().y+50) == nextPlayerPosition){
                                collide = true;
                            }
                        }
                        for (int i = 0; i < level->getGegenstaende().size(); i++) {
                            Item* item = level->getGegenstaende().at(i);
                            if ((item->getName() == "Exit2_7" || item->getName() == "Exit2_8-3")
                                && item->getRoomNo() == level->getRoomNo()
                                &&!item->getActivate()
                                && sf::Vector2f(item->getItemPosition().x, item->getItemPosition().y+50) == nextPlayerPosition){
                                collide = true;
                            }
                        }
                        if (!collide) {
                            player->setTargetPosition(nextPlayerPosition);

                            //currentPlayerPosition = player->getPosition();
                            currentPlayerDirection = player->getDirection();
                            currentPlayerPosition = nextPlayerPosition;
                        }

                        if(inventar->getChosenItem()->name == "Torch" && inventar->getTorchCount() > 0){
                            torchOn = true;
                            useTorch = true;
                            countSteps = countSteps + 1;

                            if(countSteps > 20){
                                    inventar->setTorchCount(inventar->getTorchCount() - 1);
                                torchOn = false;
                                currentDialog = updateDialog(player->getCollisionsBox().getPosition(), player->getDirection());
                                currentDialogIndex = 0;
                                updateSpeechbubble();
                                }
                            if(inventar->getTorchCount() == 0){
                                inventar->setchosenItemName("NoItem");
                            }
                        }
                    }
            }

        }
    }
}

void AdventureState::update(float dt) {

    if(clearLevel2){
        StoryState3 *storyState3 = new StoryState3(this->data);
        data->machine.AddState(StateRef(storyState3), true);
        music.stop();
        clearLevel2 = false;

        return;
    }

    //view.setCenter(player->getPosition());



    if (menuOpen) {
        inventar->update(view.getCenter(), level->getRoomNo());

        if(inventar->getToMainMenu()){
            MainMenuState* mainMenuState = new MainMenuState(this->data);

            data->machine.AddState(StateRef(mainMenuState), true);

            music.stop();
        }
    }


    auto nonCollidingArrows = std::remove_if(arrows.begin(), arrows.end(), [this](Bullet *bullet) {
        for (int j = 0; j < level->getPlattforms()->getSprites().size(); j++) {
            if (bullet->getCore().getPosition() == sf::Vector2f(level->getPlattforms()->plattformBody.at(j).getPosition().x -25,
                                                                level->getPlattforms()->plattformBody.at(j).getPosition().y -25)
                || bullet->getCore().getPosition().x < 0
                || bullet->getCore().getPosition().x > 2500
                || bullet->getCore().getPosition().y < 0
                || bullet->getCore().getPosition().y > 2500) {
                delete bullet;
                return true;
            }
        }
        return false;
    });

    arrows.assign(arrows.begin(), nonCollidingArrows);


    for (auto &arrow : arrows) {
        arrow->update();
    }

    player->update(dt);
    ghost1->update(dt);
    ghost2->update(dt);
    ghost3->update(dt);
    ghost4->update(dt);
    level->update();


    //room1 Events
    if(level->getRoomNo() == 1) {
        view.setCenter(170, 270);

        for (int i = 0; i < level->getGegenstaende().size(); i++) {

            Item *item = level->getGegenstaende().at(i);
            if (item->getInventory() && item->getItemPosition() == player->getCollisionsBox().getPosition() && !item->getPicked()) {

                item->setPicked(true);
                if (!alreadyDialogOpen) {
                    if (item->name == "Feuerstein") {
                        soundGetItem.play();
                        inventar->setGetFlintstone(true);
                        currentDialog = updateDialog(player->getCollisionsBox().getPosition(), player->getDirection());
                        currentDialogIndex = 0;
                        alreadyDialogOpen = true;
                        updateSpeechbubble();
                    }
                }
                if (item->name == "Crossbow") {
                    inventar->setGetCrossbow(true);

                }
            }

            if (level->getRoomNo() == 1 && item->name == "Exit1_2" &&
                item->getItemPosition() == player->getCollisionsBox().getPosition()) {
                level->setRoomNo(2);

                player->setCollisionBoxPosition(sf::Vector2f(25 + 1250, 25 + 750));
                player->setActualPosition(player->getCollisionsBox().getPosition());
                player->setNextPosition(player->getCollisionsBox().getPosition());
                player->setTargetPosition(player->getCollisionsBox().getPosition());

                view.setCenter(sf::Vector2f(900,550));
            }

            if (item->getType() == "Fackel" && item->getRoomNo() == 1 && inventar->getChosenItem()->name == "Feuerstein"
                && !item->getActivate() && (
                    //von links
                    (item->getItemPosition().x - 50 == player->getCollisionsBox().getPosition().x &&
                     item->getItemPosition().y + 50 == player->getCollisionsBox().getPosition().y &&
                     player->getDirection() == sf::Vector2f(-1, 0))
                    ||
                    //von rechts
                    (item->getItemPosition().x + 50 == player->getCollisionsBox().getPosition().x &&
                     item->getItemPosition().y + 50 == player->getCollisionsBox().getPosition().y &&
                     player->getDirection() == sf::Vector2f(1, 0))
                    ||
                    //von oben
                    (item->getItemPosition().x == player->getCollisionsBox().getPosition().x &&
                     item->getItemPosition().y == player->getCollisionsBox().getPosition().y &&
                     player->getDirection() == sf::Vector2f(0, 1))
                    ||
                    (item->getItemPosition().x == player->getCollisionsBox().getPosition().x &&
                     item->getItemPosition().y + 100 == player->getCollisionsBox().getPosition().y &&
                     player->getDirection() == sf::Vector2f(0, -1))
            )) {

                item->setActivate(true);
                soundFire.setVolume(100);
                soundFire.play();

            }

            int countActivatedTorch = 0;
            bool torch1 = false;
            bool torch2 = false;
            bool torch3 = false;
            bool torch4 = false;

            for (int i = 0; i < level->getGegenstaende().size(); i++) {
                Item *item = level->getGegenstaende().at(i);

                if (item->getName() == "FackelS_1" && item->getActivate()) {
                    torch1 = true;
                    countActivatedTorch = countActivatedTorch + 1;
                }
                if (item->getName() == "FackelS_2" && item->getActivate()) {
                    torch2 = true;
                    countActivatedTorch = countActivatedTorch + 1;
                }
                if (item->getName() == "FackelS_3" && item->getActivate()) {
                    torch3 = true;
                    countActivatedTorch = countActivatedTorch + 1;
                }
                if(item->getName() == "FackelS_4" && item->getActivate()) {
                    torch4 = true;
                    countActivatedTorch = countActivatedTorch + 1;
                }

            }

            if (countActivatedTorch == 1) {// && !setTorchSprite){
                fackelAnSprite.setTexture(this->data->assets.getTexture("OneTorch"));
                if (torch2) {
                    fackelAnSprite.setRotation(90);
                } else if (torch3) {
                    fackelAnSprite.setRotation(180);
                } else if (torch4) {
                    fackelAnSprite.setRotation(-90);
                }
            }

            if (countActivatedTorch == 2) {// && !setTorchSprite){
                fackelAnSprite.setTexture(this->data->assets.getTexture("TwoTorchesParallel"));
                fackelAnSprite.setRotation(0);
                if(torch2 && torch3){
                    fackelAnSprite.setTexture(this->data->assets.getTexture("TwoTorchesParallel"));
                    fackelAnSprite.setRotation(90);
                } else if (torch3 && torch4) {
                    fackelAnSprite.setTexture(this->data->assets.getTexture("TwoTorchesParallel"));
                    fackelAnSprite.setRotation(180);
                } else if (torch4 && torch1) {
                    fackelAnSprite.setTexture(this->data->assets.getTexture("TwoTorchesParallel"));
                    fackelAnSprite.setRotation(-90);
                } else if (torch1 && torch3) {
                    fackelAnSprite.setTexture(this->data->assets.getTexture("TwoTorchesDiagonal"));
                } else if (torch2 && torch4) {
                    fackelAnSprite.setTexture(this->data->assets.getTexture("TwoTorchesDiagonal"));
                    fackelAnSprite.setRotation(90);
                }


            }

            if (countActivatedTorch == 3) {
                fackelAnSprite.setTexture(this->data->assets.getTexture("ThreeTorches"));
                fackelAnSprite.setRotation(0);
                if (torch1 && torch2 && torch3) {
                    fackelAnSprite.setRotation(90);
                } else if (torch2 && torch3 && torch4) {
                    fackelAnSprite.setRotation(180);
                } else if (torch1 && torch3 && torch4) {
                    fackelAnSprite.setRotation(-90);
                }

                if(!threeTorchDialog){
                    currentDialog = updateDialog(player->getCollisionsBox().getPosition(), player->getDirection());
                    currentDialogIndex = 0;
                    threeTorchDialog = true;
                    updateSpeechbubble();

                }
            }

            if (countActivatedTorch == 4) {

                fackelAnSprite.setTexture(this->data->assets.getTexture("AllTorches"));
                for (int i = 0; i < level->getGegenstaende().size(); i++) {
                    if ((level->getGegenstaende().at(i)->getName() == "Door1" ||
                        level->getGegenstaende().at(i)->name == "Exit1_2"
                        || level->getGegenstaende().at(i)->getName() == "Exit2_1") && !level->getGegenstaende().at(i)->getActivate()) {
                            level->getGegenstaende().at(i)->setActivate(true);
                        soundSolved.play();
                    }
                }

                if(!fourTorchDialog){
                    currentDialog = updateDialog(player->getCollisionsBox().getPosition(), player->getDirection());
                    currentDialogIndex = 0;
                    fourTorchDialog = true;
                    updateSpeechbubble();

                }


                //fackelZusatzSprite.setTexture(this->data->assets.getTexture("AllTorchesPlus"));
            }


        }
    }
    //room1 Events End

    //room2 Events
    if(level->getRoomNo() == 2){

    //Kameragrenzen

        if(player->getPosition().x > 500 && player->getPosition().x < 900){
            view.setCenter(player->getPosition().x, view.getCenter().y);
        }
        if(player->getPosition().y > 300 && player->getPosition().y < 550){
            view.setCenter(view.getCenter().x, player->getPosition().y);
        }

        for (int i = 0; i < level->getGegenstaende().size(); ++i) {
            auto item = level->getGegenstaende().at(i);

            if(item->getName() == "EyeSwitch" && !item->getActivate()){
                for (auto &arrow : arrows) {
                    if(arrow->getCore().getPosition() == sf::Vector2f(1000,100)){
                        item->setActivate(true);
                        soundOpenDoor.play();
                    }
                }

                if(item->getActivate()) {
                    for (auto &item2 : level->getGegenstaende()) {
                        if (item2->getName() == "Exit2_8-1" || item2->getName() == "Exit2_8-2" ||
                            item2->getName() == "Door3") {
                            item2->setActivate(true);
                        }

                    }
                }
            }


            if(!ghost1->getExist() && item->getName() == "BowlViolett" && !item->getActivate()){
                item->setActivate(true);
                for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                    if(level->getGegenstaende().at(j)->name == "FireViolett"){
                        level->getGegenstaende().at(j)->setExist(true);
                    }
                }
                soundGhost.play();
            }

            if(!ghost2->getExist() && item->getName() == "BowlYellow" && !item->getActivate()){
                item->setActivate(true);
                for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                    if(level->getGegenstaende().at(j)->name == "FireYellow"){
                        level->getGegenstaende().at(j)->setExist(true);
                    }
                }
                soundGhost.play();
            }

            if(ghost3Defeat && item->getName() == "BowlBlue" && !item->getActivate()){
                item->setActivate(true);
                for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                    if(level->getGegenstaende().at(j)->name == "FireBlue"){
                        level->getGegenstaende().at(j)->setExist(true);
                    }
                }
                soundGhost.play();
            }

            if(!ghost4->getExist() && item->getName() == "BowlGreen" && !item->getActivate()){
                item->setActivate(true);
                for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                    if(level->getGegenstaende().at(j)->name == "FireGreen"){
                        level->getGegenstaende().at(j)->setExist(true);
                    }
                }
                soundGhost.play();
            }

            if(!ghost1->getExist() && !ghost2->getExist() && ghost3Defeat && !ghost4->getExist() && item->name == "FinalExit"
                && !item->getExist()){
                item->setExist(true);
                item->setActivate(true);
            }

            if(item->getName() == "FinalExit" && item->getActivate() && item->getItemPosition() == player->getPosition()){
                clearLevel2 = true;
            }

            //Doors room2
            if (item->name == "Exit2_1" &&
                item->getItemPosition() == player->getCollisionsBox().getPosition()) {
                level->setRoomNo(1);

                player->setCollisionBoxPosition(sf::Vector2f(25 + 100, 25 + 150));
                player->setActualPosition(player->getCollisionsBox().getPosition());
                player->setNextPosition(player->getCollisionsBox().getPosition());
                player->setTargetPosition(player->getCollisionsBox().getPosition());
            }
            if ((item->name == "Exit2_3-1" || item->name == "Exit2_3-2") &&
                item->getItemPosition() == player->getCollisionsBox().getPosition()) {
                level->setRoomNo(3);
                view.setCenter(500, 150);


                if (item->name == "Exit2_3-1") {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 500, 25 + 100));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                } else {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 550, 25 + 100));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                }
            }

            if ((item->name == "Exit2_4-1" || item->name == "Exit2_4-2") &&
                item->getItemPosition() == player->getCollisionsBox().getPosition()) {
                level->setRoomNo(4);
                view.setCenter(450, 200);


                if (item->name == "Exit2_4-1") {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 700, 200 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                } else {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 750, 200 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                }

            }

            if (item->name == "Exit2_7" &&
                    sf::Vector2f(item->getItemPosition().x, item->getItemPosition().y + 50)
                    == player->getCollisionsBox().getPosition()) {
                level->setRoomNo(7);

                player->setCollisionBoxPosition(sf::Vector2f(25 + 250, 750 + 25));
                player->setActualPosition(player->getCollisionsBox().getPosition());
                player->setNextPosition(player->getCollisionsBox().getPosition());
                player->setTargetPosition(player->getCollisionsBox().getPosition());
            }

            if ((item->name == "Exit2_8-1" || item->name == "Exit2_8-2" || item->name == "Exit2_8-3")
                && item->getItemPosition() == player->getCollisionsBox().getPosition() && item->getActivate()) {
                level->setRoomNo(8);

                if (item->name == "Exit2_8-1") {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 400, 450 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                } else if (item->name == "Exit2_8-2") {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 450, 450 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                } else {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 50, 150 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                }
            }
            //doors room2 end

            sf::Vector2f neighbourField((player->getPosition().x - player->getDirection().x * 50) - 25,
                                        (player->getPosition().y + player->getDirection().y * 50) - 25);

            if (((neighbourField == sf::Vector2f(650, 700) && item->getName() == "Fackel2_r")
                 || (neighbourField == sf::Vector2f(500, 700) && item->getName() == "Fackel2_l"))
                && inventar->getChosenItem()->name == "Feuerstein" && !item->getActivate()) {
                item->setActivate(true);
                soundFire.play();
            }


            int countActiveTorches = 0;
            for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                if ((level->getGegenstaende().at(j)->getName() == "Fackel2_r" &&
                     level->getGegenstaende().at(j)->getActivate())
                    || level->getGegenstaende().at(j)->getName() == "Fackel2_l" &&
                       level->getGegenstaende().at(j)->getActivate()) {

                    countActiveTorches = countActiveTorches + 1;
                }
            }

            if (countActiveTorches == 2) {
                for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                    if ((level->getGegenstaende().at(j)->getName() == "Exit2_3-1"
                        || level->getGegenstaende().at(j)->getName() == "Exit2_3-2") &&
                            !level->getGegenstaende().at(j)->getActivate()) {
                        level->getGegenstaende().at(j)->setActivate(true);
                        soundSolved.play();
                    }
                }
            }

            if(itemActivate && item->getName() == "ChestTalisman" && neighbourField == sf::Vector2f(500, 250) &&
                    !item->getActivate()){
                item->setActivate(true);
                for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                    if(level->getGegenstaende().at(j)->getName() == "Talisman"){
                        inventar->setGetTalisman(true);
                        inventar->setTalismanCount(inventar->getTalismanCount() + 1);
                        soundGetItem.play();
                    }
                }
                currentDialog = updateDialog(player->getCollisionsBox().getPosition(), player->getDirection());
                currentDialogIndex = 0;
                updateSpeechbubble();
            }

            if(itemActivate && item->getName() == "ChestDungeonMap" && neighbourField == sf::Vector2f(650, 250) &&
               !item->getActivate()){
                item->setActivate(true);
                soundGetItem.play();
                inventar->setHaveDungeonMap(true);
                currentDialog = updateDialog(player->getCollisionsBox().getPosition(), player->getDirection());
                currentDialogIndex = 0;
                updateSpeechbubble();
            }

            if((neighbourField == sf::Vector2f(100,350) || neighbourField == sf::Vector2f(150, 350))
               && inventar->getChosenItem()->name == "Key"
               && inventar->getKeyCount() > 0){
                for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                    if((level->getGegenstaende().at(j)->getName() == "Exit2_4-1"
                       || level->getGegenstaende().at(j)->getName() == "Exit2_4-2"
                       || level->getGegenstaende().at(j)->getName() == "Door2") && !level->getGegenstaende().at(j)->getActivate()){
                        level->getGegenstaende().at(j)->setActivate(true);
                        soundUnlockDoor.play();
                    }
                }
                inventar->setKeyCount(inventar->getKeyCount() - 1);
                if(inventar->getKeyCount() == 0){
                    inventar->setchosenItemName("NoItem");
                }
            }

            if((player->getPosition().x == 550+25 || player->getPosition().x == 600+25) && player->getPosition().y == 700+25
               && !bowlOn && !ghost1->getExist()){
                currentDialog = updateDialog(player->getCollisionsBox().getPosition(), player->getDirection());
                currentDialogIndex = 0;
                updateSpeechbubble();
                bowlOn = true;
            }

        }
    }
    //room2 Events End

    //room3 Events
    if(level->getRoomNo() == 3){

        if(player->getPosition().y > 150 && player->getPosition().y < 350){
            view.setCenter(500, player->getPosition().y);
        }

        if(!enterRoom3 && (player->getPosition() == sf::Vector2f(500+25, 150+25) || player->getPosition() == sf::Vector2f(550+25,150+25))){
            currentDialog = updateDialog(player->getCollisionsBox().getPosition(), player->getDirection());
            currentDialogIndex = 0;
            updateSpeechbubble();
            enterRoom3 = true;

        }
          //  view.setCenter(player->getPosition().x, view.getCenter().y);
        //}

        for (int i = 0; i < level->getGegenstaende().size(); ++i) {
            auto item = level->getGegenstaende().at(i);

            if(item->getName() == "Exit2_3-1" || item->getName() == "Exit2_3-2" && item->getActivate()){
                for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                    if(level->getGegenstaende().at(j)->getName() == "Exit3_2-1"
                       || level->getGegenstaende().at(j)->getName() == "Exit3_2-2"){
                        level->getGegenstaende().at(j)->setActivate(true);
                    }
                }
            }


            if (item->name == "GuardianButton" && itemActivate &&
                player->getPosition() == sf::Vector2f(900 + 25, 200 + 25) &&
                player->getDirection() == sf::Vector2f(0, -1)) {

                if (item->getActivate() && item->name == "GuardianButton"){//item->getActivate()) {
                    for (int j = 0; j < level->getGegenstaende().size(); j++) {
                        if (level->getGegenstaende().at(j)->getName() == "Fence"
                            && !level->getGegenstaende().at(j)->getActivate()) {
                            level->getGegenstaende().at(j)->setActivate(true);
                            soundSolved.play();
                        }
                    }
                } else if(!item->getActivate() && item->name == "GuardianButton"){
                    item->setActivate(true);
                    itemActivate = false;
                    guardianActivate = true;
                    soundSwitch.play();
                }
            }

            if ((item->name == "Exit3_2-1" || item->name == "Exit3_2-2") &&
                item->getItemPosition() == player->getCollisionsBox().getPosition()) {
                level->setRoomNo(2);

                if (item->name == "Exit3_2-1") {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 550, 750 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                } else {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 600, 750 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                }
                view.setCenter(player->getPosition().x, 550);
            }

            if (item->name == "Exit3_5" &&
                item->getItemPosition() == player->getCollisionsBox().getPosition()) {
                level->setRoomNo(5);

                player->setCollisionBoxPosition(sf::Vector2f(25 + 650, 600 + 25));
                player->setActualPosition(player->getCollisionsBox().getPosition());
                player->setNextPosition(player->getCollisionsBox().getPosition());
                player->setTargetPosition(player->getCollisionsBox().getPosition());
            }

            if(item->name == "Stairs" &&
                    item->getItemPosition() == player->getCollisionsBox().getPosition()){
                level->setRoomNo(6);

                player->setCollisionBoxPosition(sf::Vector2f(50+25, 950 + 25));
                player->setActualPosition(player->getCollisionsBox().getPosition());
                player->setNextPosition(player->getCollisionsBox().getPosition());
                player->setTargetPosition(player->getCollisionsBox().getPosition());

                currentDialog = updateDialog(player->getCollisionsBox().getPosition(), player->getDirection());
                currentDialogIndex = 0;
                torchDialog = true;
                updateSpeechbubble();
            }


            if (item->name == "GuardianButton" && itemActivate &&
                player->getPosition() == sf::Vector2f(900 + 25, 200 + 25) &&
                player->getDirection() == sf::Vector2f(0, -1)) {
                if (item->getActivate()) {
                    for (int j = 0; j < level->getGegenstaende().size(); j++) {
                        if (level->getGegenstaende().at(j)->getName() == "Fence") {
                            level->getGegenstaende().at(j)->setActivate(true);
                        }
                    }
                } else {
                    item->setActivate(true);
                    itemActivate = false;
                    guardianActivate = true;
                }
            }

            if(item->getName() == "Chest" && itemActivate &&
                    player->getPosition() == sf::Vector2f(750 + 25, 350 + 25) &&
                    player->getDirection() == sf::Vector2f(0,-1) && !item->getActivate()){

                item->setActivate(true);
                soundGetItem.play();


                for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                    if(level->getGegenstaende().at(j)->getName() == "Key"){
                        inventar->setGetKey(true);
                        inventar->setKeyCount(inventar->getKeyCount() + 1);
                    }
                }

                    currentDialog = updateDialog(player->getCollisionsBox().getPosition(), player->getDirection());
                    currentDialogIndex = 0;
                    updateSpeechbubble();

                    room3chestOpen = true;
                    itemActivate = false;
            }

            if (item->getName() == "Switch" && itemActivate &&
                player->getPosition() == sf::Vector2f(100 + 25, 100 + 25) &&
                player->getDirection() == sf::Vector2f(0, -1)) {
                if (item->getActivate()) {
                    for (int j = 0; j < level->getGegenstaende().size(); j++) {
                        if (level->getGegenstaende().at(j)->getType() == "Stairs"
                            || level->getGegenstaende().at(j)->getName() == "Stairs") {
                            level->getGegenstaende().at(j)->setActivate(true);
                        }
                    }
                } else {
                    item->setActivate(true);
                    room3switchActive = true;
                    itemActivate = false;
                }
            }

            if (item->getInventory() && item->getItemPosition() == player->getCollisionsBox().getPosition() && !item->getPicked()) {
                if (item->name == "Torch" && !item->getPicked()) {
                    item->setPicked(true);
                    soundGetItem.play();
                    inventar->setGetTorch(true);
                    inventar->setTorchCount(inventar->getTorchCount() + 1);
                    }
            }

            sf::Vector2f neighbourField((player->getPosition().x - player->getDirection().x * 50),
                                        (player->getPosition().y + player->getDirection().y * 50));


            if(neighbourField == ghost1->getPosition() && inventar->getChosenItem()->name == "Talisman" && ghost1->getExist()){
                ghost1->setExist(false);
                soundGhost.setVolume(100);
                soundGhost.play();
                inventar->setTalismanCount(inventar->getTalismanCount() - 1);
                if(inventar->getTalismanCount() == 0){
                    inventar->setchosenItemName("NoItem");
                }
            }
        }
    }
    //room3 Events End


    //room4
    if(level->getRoomNo() == 4){
        if(player->getPosition().y > 200 && player->getPosition().y < 800){
            view.setCenter(450, player->getPosition().y);
        }

        for (int i = 0; i < level->getGegenstaende().size(); ++i) {
            auto item = level->getGegenstaende().at(i);

            if ((item->name == "Exit4_2-1" || item->name == "Exit4_2-2") &&
                item->getItemPosition() == player->getCollisionsBox().getPosition()) {
                level->setRoomNo(2);

                if (item->name == "Exit4_2-1") {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 100, 400 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                } else {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 150, 400 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                }

                view.setCenter(500, player->getPosition().y);
            }

            if ((item->name == "Exit4_5-1" || item->name == "Exit4_5-2") &&
                item->getItemPosition() == player->getCollisionsBox().getPosition()) {
                level->setRoomNo(5);

                if (item->name == "Exit4_5-1") {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 250, 50 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                } else {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 300, 50 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                }
            }

            sf::Vector2f neighbourField((player->getPosition().x - player->getDirection().x * 50) - 25,
                                        (player->getPosition().y + player->getDirection().y * 50) - 25);

            if(itemActivate && item->getName() == "ChestTalisman2" && neighbourField == sf::Vector2f(150, 700) &&
               !item->getActivate()){
                item->setActivate(true);
                soundGetItem.play();
                for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                    if(level->getGegenstaende().at(j)->getName() == "Talisman"){
                        inventar->setGetTalisman(true);
                        inventar->setTalismanCount(inventar->getTalismanCount() + 1);
                    }
                }
                currentDialog = updateDialog(player->getCollisionsBox().getPosition(), player->getDirection());
                currentDialogIndex = 0;
                updateSpeechbubble();
            }

            if (((neighbourField == sf::Vector2f(50, 100) && item->getName() == "Fackel4_1") ||
                    (neighbourField == sf::Vector2f(400, 250) && item->getName() == "Fackel4_2") ||
                    (neighbourField == sf::Vector2f(350,400) && item->getName() == "Fackel4_3") ||
                    (neighbourField == sf::Vector2f(50, 700) && item->getName() == "Fackel4_4") ||
                    (neighbourField == sf::Vector2f(400, 800) && item->getName() == "Fackel4_5") ||
                    (neighbourField == sf::Vector2f(750, 100) && item->getName() == "Fackel4_6")
                ) && inventar->getChosenItem()->name == "Feuerstein" && !item->getActivate()) {
                item->setActivate(true);
                soundFire.play();
            }

            int countActiveTorches = 0;
            for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                if ((level->getGegenstaende().at(j)->getName() == "Fackel4_1" && level->getGegenstaende().at(j)->getActivate())
                    || (level->getGegenstaende().at(j)->getName() == "Fackel4_2" && level->getGegenstaende().at(j)->getActivate())
                    || (level->getGegenstaende().at(j)->getName() == "Fackel4_3" && level->getGegenstaende().at(j)->getActivate())
                    || (level->getGegenstaende().at(j)->getName() == "Fackel4_4" && level->getGegenstaende().at(j)->getActivate())
                    || (level->getGegenstaende().at(j)->getName() == "Fackel4_5" && level->getGegenstaende().at(j)->getActivate())
                    || (level->getGegenstaende().at(j)->getName() == "Fackel4_6" && level->getGegenstaende().at(j)->getActivate())){

                    countActiveTorches = countActiveTorches + 1;
                }
            }

            if (countActiveTorches == 6) {
                for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                    if ((level->getGegenstaende().at(j)->getName() == "Exit4_5-1"
                        || level->getGegenstaende().at(j)->getName() == "Exit4_5-2") &&
                            !level->getGegenstaende().at(j)->getActivate()) {
                        level->getGegenstaende().at(j)->setActivate(true);
                        soundSolved.play();
                    }
                }
            }

        }
    }

    if(level->getRoomNo() == 5){
        if(player->getPosition().y > 200 && player->getPosition().y < 300){
            view.setCenter(450, player->getPosition().y);
        }

        for (int i = 0; i < level->getGegenstaende().size(); ++i) {
            auto item = level->getGegenstaende().at(i);

            sf::Vector2f neighbourField((player->getPosition().x - player->getDirection().x * 50) - 25,
                                        (player->getPosition().y + player->getDirection().y * 50) - 25);


            if ((item->name == "Exit5_4-1" || item->name == "Exit5_4-2") &&
                item->getItemPosition() == player->getCollisionsBox().getPosition()) {
                level->setRoomNo(4);
                view.setCenter(sf::Vector2f(450,800));

                if (item->name == "Exit5_4-1") {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 250, 1000 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                } else {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 300, 1000 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                }
            }
            if (level->getRoomNo() == 5 && item->name == "Exit5_3" &&
                item->getItemPosition() == player->getCollisionsBox().getPosition()) {
                level->setRoomNo(3);
                view.setCenter(500, 350);

                player->setCollisionBoxPosition(sf::Vector2f(25 + 50, 500 + 25));
                player->setActualPosition(player->getCollisionsBox().getPosition());
                player->setNextPosition(player->getCollisionsBox().getPosition());
                player->setTargetPosition(player->getCollisionsBox().getPosition());
            }

            if(neighbourField == sf::Vector2f( ghost2->getPosition().x-25, ghost2->getPosition().y-25) && ghost2->getExist()
               && inventar->getChosenItem()->name == "Talisman"){
                ghost2->setExist(false);
                soundGhost.play();
                inventar->setTalismanCount(inventar->getTalismanCount() - 1);

                if(inventar->getTalismanCount() == 0){
                    inventar->setchosenItemName("NoItem");
                }
            }
        }
    }


    //room6

    if(level->getRoomNo() == 6) {

        if(inventar->getChosenItem()->name == "Torch"){
            level->setRoom6Torch(true);
        } else{
            level->setRoom6Torch(false);
        }
        for (int i = 0; i < level->getGegenstaende().size(); ++i) {
            auto item = level->getGegenstaende().at(i);

            if (item->name == "Exit6_7" &&
                item->getItemPosition() == player->getCollisionsBox().getPosition()) {
                level->setRoomNo(7);

                player->setCollisionBoxPosition(sf::Vector2f(25 + 200, 350 + 25));
                player->setActualPosition(player->getCollisionsBox().getPosition());
                player->setNextPosition(player->getCollisionsBox().getPosition());
                player->setTargetPosition(player->getCollisionsBox().getPosition());
            }

            if(item->name == "Exit6_3" && item->getItemPosition() == player->getPosition()){
                level->setRoomNo(3);

                player->setCollisionBoxPosition(sf::Vector2f(25 + 350, 500 + 25));
                player->setActualPosition(player->getCollisionsBox().getPosition());
                player->setNextPosition(player->getCollisionsBox().getPosition());
                player->setTargetPosition(player->getCollisionsBox().getPosition());

            }

        }



        level->setForegroundPosition(sf::Vector2f(-50, player->getPosition().y - 1550));
    }

    //room7
    if (level->getRoomNo() == 7){
        for (int i = 0; i < level->getGegenstaende().size(); ++i) {
            auto item = level->getGegenstaende().at(i);

            sf::Vector2f neighbourField((player->getPosition().x - player->getDirection().x * 50) - 25,
                                        (player->getPosition().y + player->getDirection().y * 50) - 25);

            if (item->name == "Exit7_2" && item->getItemPosition() == player->getCollisionsBox().getPosition()) {
                level->setRoomNo(2);

                player->setCollisionBoxPosition(sf::Vector2f(25 + 350, 150 + 25));
                player->setActualPosition(player->getCollisionsBox().getPosition());
                player->setNextPosition(player->getCollisionsBox().getPosition());
                player->setTargetPosition(player->getCollisionsBox().getPosition());

                view.setCenter(500, 300 );
            }

            if(item->name == "Exit7_6" && item->getItemPosition() == player->getPosition()){
                level->setRoomNo(6);

                player->setCollisionBoxPosition(sf::Vector2f(25 + 50, 150 + 25));
                player->setActualPosition(player->getCollisionsBox().getPosition());
                player->setNextPosition(player->getCollisionsBox().getPosition());
                player->setTargetPosition(player->getCollisionsBox().getPosition());

            }


            if(item->name == "TombSwitch" && neighbourField == sf::Vector2f(600, 350) && !item->getActivate()  && itemActivate){
                item->setActivate(true);
                ghost3->setExist(true);
                soundGhost.play();
            }

            if(sf::Vector2f(neighbourField.x+25, neighbourField.y+25) == ghost3->getPosition() && inventar->getChosenItem()->name == "Talisman"
               && ghost3->getExist()){
                ghost3->setExist(false);
                soundGhost.play();
                soundSolved.play();
                ghost3Defeat = true;
                inventar->setTalismanCount(inventar->getTalismanCount() - 1);
                if(inventar->getTalismanCount() == 0){
                    inventar->setchosenItemName("NoItem");
                }

                for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                    if(level->getGegenstaende().at(j)->name == "ChestCrossbow"){
                        level->getGegenstaende().at(j)->setExist(true);
                    }
                }
            }

            if(item->name == "TombSwitchEye" && !item->getActivate()) {
                for (auto &arrow : arrows) {
                    if (arrow->getCore().getPosition() == sf::Vector2f(550, 150)) {
                        item->setActivate(true);
                        for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                            if (level->getGegenstaende().at(j)->getName() == "Exit7_2") {
                                level->getGegenstaende().at(j)->setActivate(true);
                                soundOpenDoor.play();
                            }
                        }
                    }
                }
            }

            if(itemActivate && item->getName() == "ChestCrossbow" && neighbourField == sf::Vector2f(450, 400) &&
               !item->getActivate() && item->getExist()){
                item->setActivate(true);
                soundGetItem.play();
                for (int j = 0; j < level->getGegenstaende().size(); ++j) {
                    if(level->getGegenstaende().at(j)->getName() == "Crossbow"){
                        inventar->setGetCrossbow(true);
                    }
                }
                currentDialog = updateDialog(player->getCollisionsBox().getPosition(), player->getDirection());
                currentDialogIndex = 0;
                updateSpeechbubble();
            }

            if(item->name == "Shrine" && neighbourField == sf::Vector2f(450,150) && !item->getActivate() && itemActivate){
                item->setActivate(true);
                inventar->setGetTalisman(true);
                inventar->setTalismanCount(inventar->getTalismanCount() + 2);
                soundGetItem.play();
            }

            /*
            if(neighbourField == sf::Vector2f( ghost3->getPosition().x-25, ghost3->getPosition().y-25) && ghost3->getExist()
               && inventar->getChosenItem()->name == "Talisman"){
                ghost3->setExist(false);
                ghost3Defeat = true;
                inventar->setTalismanCount(inventar->getTalismanCount() - 1);
            }*/

        }
    }

    //room8

    if(level->getRoomNo() == 8){
        for (int i = 0; i < level->getGegenstaende().size(); ++i) {
            auto item = level->getGegenstaende().at(i);

            sf::Vector2f neighbourField((player->getPosition().x - player->getDirection().x * 50) - 25,
                                        (player->getPosition().y + player->getDirection().y * 50) - 25);


            if((item->name == "Exit8_2-1" || item->name == "Exit8_2-2" || item->name == "Exit8_2-3")
               && item->getItemPosition() == player->getCollisionsBox().getPosition()) {
                level->setRoomNo(2);

                if (item->name == "Exit8_2-1") {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 1150, 400 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                    view.setCenter(sf::Vector2f(900,player->getPosition().y));
                } else if (item->name == "Exit8_2-2") {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 1200, 400 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                    view.setCenter(sf::Vector2f(900,player->getPosition().y));
                } else {
                    player->setCollisionBoxPosition(sf::Vector2f(25 + 800, 150 + 25));
                    player->setActualPosition(player->getCollisionsBox().getPosition());
                    player->setNextPosition(player->getCollisionsBox().getPosition());
                    player->setTargetPosition(player->getCollisionsBox().getPosition());
                    view.setCenter(sf::Vector2f(player->getPosition().x, 300 ));
                }
            }

            if(neighbourField == sf::Vector2f( ghost4->getPosition().x-25, ghost4->getPosition().y-25) && ghost4->getExist()
               && inventar->getChosenItem()->name == "Talisman"){
                ghost4->setExist(false);
                soundGhost.play();
                inventar->setTalismanCount(inventar->getTalismanCount() - 1);
                if(inventar->getTalismanCount() == 0){
                    inventar->setchosenItemName("NoItem");
                }
            }

            if((item->getName() == "Exit8_2-3" || item->getName() == "Exit2_8-3") && !item->getActivate() && !ghost4->getExist()){
                item->setActivate(true);
                if(item->name == "Exit8_2-3"){
                    soundOpenDoor.play();
                }
            }

        }
    }


    /*
    for (int i = 0; i < level->getGegenstaende().size(); i++) {

        Item *item = level->getGegenstaende().at(i);
        if (item->getInventory() && item->getItemPosition() == player->getCollisionsBox().getPosition()) {

            item->setPicked(true);
            if (!alreadyDialogOpen) {
                if (item->name == "Feuerstein") {
                    inventar->setGetFlintstone(true);
                    currentDialog = updateDialog(player->getCollisionsBox().getPosition(), player->getDirection());
                    currentDialogIndex = 0;
                    alreadyDialogOpen = true;
                    updateSpeechbubble();
                }
            }

     //TODO Crossbow wieder einbauen!
            if (item->name == "Crossbow") {
                inventar->setGetCrossbow(true);

            }
        }

     */

        /*
        if (level->getRoomNo() == 3) {
            if (item->name == "GuardianButton" && itemActivate && player->getPosition() == sf::Vector2f(900+25,200+25) &&
                    player->getDirection() == sf::Vector2f(0,-1)) {
                if (item->getActivate()) {
                    for (int j = 0; j < level->getGegenstaende().size(); j++) {
                        if (level->getGegenstaende().at(j)->getName() == "Fence") {
                            level->getGegenstaende().at(j)->setActivate(true);
                        }
                    }
                } else {
                    item->setActivate(true);
                    itemActivate = false;
                    guardianActivate = true;
                }
            }
        }
         */

        ////////////////////////////////////////////Nicht wegschmeißen
        //DoorLogic
        /*















        if (level->getRoomNo() == 8 &&

        }//DoorLogicEnd
         */

        //Ab hier Level Ereignisse
        if (!speechBubbleOn) {
            auto lastDialog = this->currentDialog;
            this->currentDialog = updateDialog(player->getPosition(), player->getDirection());
            if (this->currentDialog != lastDialog) {
                currentDialogIndex = -1;
                updateSpeechbubble();
            }
        }



    if(inventar->getChosenItem()->getName() == "Feuerstein"){
        activeItemSprite.setTexture(this->data->assets.getTexture("Feuerstein"));
    }else if(inventar->getChosenItem()->getName() == "Crossbow"){
        activeItemSprite.setTexture(this->data->assets.getTexture("Crossbow"));
    } else if(inventar->getChosenItem()->getName() == "Talisman"){
        activeItemSprite.setTexture(this->data->assets.getTexture("Talisman"));
    } else if(inventar->getChosenItem()->getName() == "Torch"){
        activeItemSprite.setTexture(this->data->assets.getTexture("Torch"));
    } else if(inventar->getChosenItem()->getName() == "Key"){
        activeItemSprite.setTexture(this->data->assets.getTexture("Key"));
    } else if(inventar->getChosenItem()->getName() == "NoItem"){
        activeItemSprite.setTexture(this->data->assets.getTexture("NoItem"));
    }

    if(speechBubbleOn){
        speechbubble->setPosition(view.getCenter());
    }

    if(level->getRoomNo() > 4){
        view.setCenter(player->getPosition());
    }


    activeItemButton.setPosition(view.getCenter().x + 150, view.getCenter().y - 300);
    activeItemSprite.setPosition(view.getCenter().x + 250, view.getCenter().y - 250);

    //keySprite.setPosition(view.getCenter().x + 370, view.getCenter().y + 250);
    //keyText.setPosition(view.getCenter().x + 280, view.getCenter().y - 130);


    if(currentPlayerPosition != player->getPosition() || player->getDirection() != currentPlayerDirection){
        itemActivate = false;
    }

}


void AdventureState::updateSpeechbubble() {
    speechBubbleOn = currentDialog != nullptr
                     && (currentDialogIndex > -1)
                     && (currentDialogIndex < (*currentDialog).size());
    if(speechBubbleOn){
            auto currentTextItem = (*currentDialog)[currentDialogIndex];
            speechbubble->setText(currentTextItem.text);
            speechbubble->setCharacterName(currentTextItem.characterName);
            speechbubble->setCharacterImage(currentTextItem.characterImage);
        }
}


void AdventureState::draw(float dt) {

    this->data->window.clear(sf::Color::Black);
    this->data->window.setView(view);

    level->drawLayer1();

    if(level->getRoomNo() == 1){
        this->data->window.draw(fackelAnSprite);
    }


    for(int i = 0; i < arrows.size(); i++){
        arrows.at(i)->draw();
    }

    level->drawLayer2();
    player->draw(this->data->window);
    if(ghost1->getRoomNo() == level->getRoomNo() && ghost1->getExist()){
        ghost1->draw(this->data->window);
    }
    if(ghost2->getRoomNo() == level->getRoomNo() && ghost2->getExist()){
        ghost2->draw(this->data->window);
    }
    if(ghost3->getRoomNo() == level->getRoomNo() && ghost3->getExist()){
        ghost3->draw(this->data->window);
    }
    if(ghost4->getRoomNo() == level->getRoomNo() && ghost4->getExist()){
        ghost4->draw(this->data->window);
    }

    level->drawLayer3();

    if (menuOpen) {

        inventar->draw();
    }

    this->data->window.draw(activeItemButton);
    this->data->window.draw(activeItemSprite);
    //this->data->window.draw(keySprite);
    //this->data->window.draw(keyText);
    if(speechBubbleOn){
        speechbubble->draw();
    }


    this->data->window.display();


}

AdventureState::~AdventureState() {
    delete player;
    delete level;
    delete ghost1;
    delete ghost2;
    delete ghost3;
    delete ghost4;

}




std::vector<TextItem>* AdventureState::updateDialog(sf::Vector2f position, sf::Vector2f direction) {

    sf::Vector2f neighbourField((position.x - direction.x * 50)-25, (position.y + direction.y*50) - 25);

    if(!torchOn && useTorch){
        useTorch = false;
        return &dialogs.torchOff;
    }

    //Room1
    if(level->getRoomNo() == 1){

        /*
        if(neighbourField == sf::Vector2f(0, 150)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                auto item = level->getGegenstaende().at(i);
                if (item->getName() == "FackelS_1" && item->getActivate()) {
                    if(threeTorchDialog){
                        return &dialogs.speakWithTutorial;
                    }
                    return &dialogs.commonTorchOn;
                }
            }
            return &dialogs.commonTorchOff;
        }

        if(neighbourField == sf::Vector2f(300, 150)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                auto item = level->getGegenstaende().at(i);
                if (item->getName() == "FackelS_2" && item->getActivate()) {
                    return &dialogs.commonTorchOn;
                }
            }
            return &dialogs.commonTorchOff;
        }

        if(neighbourField == sf::Vector2f(0, 450)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                auto item = level->getGegenstaende().at(i);
                if (item->getName() == "FackelS_4" && item->getActivate()) {
                    return &dialogs.commonTorchOn;
                }
            }

            return &dialogs.commonTorchOff;
        }

        if(neighbourField == sf::Vector2f(300, 450)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                auto item = level->getGegenstaende().at(i);
                if (item->getName() == "FackelS_3" && item->getActivate()) {
                    return &dialogs.commonTorchOn;
                }
            }
            return &dialogs.commonTorchOff;
        }
         */

        if (neighbourField == sf::Vector2f(200,150)) {
            return &dialogs.deadMan;
        }

        if(!alreadyDialogOpen && position == sf::Vector2f(200+25,400+25)){
            return  &dialogs.itemTutorial;
        }


        if(neighbourField == sf::Vector2f(150,300)){
            return &dialogs.fireplace;
        }

        if(neighbourField == sf::Vector2f(100, 450) || neighbourField == sf::Vector2f(150, 450) || neighbourField == sf::Vector2f(200, 450)){
            return &dialogs.bed;
        }

        if(neighbourField == sf::Vector2f(300, 250)){
            return &dialogs.animalBones;
        }

        if(neighbourField == sf::Vector2f(100,100)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                auto door = level->getGegenstaende().at(i);
                if(door->name == "Door1" && !door->getActivate()){
                    return &dialogs.closedDoor;
                }
            }
        }

        int countTorches = 0;

        if(!threeTorchDialog ){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                if(level->getGegenstaende().at(i)->getType() == "Fackel" && level->getGegenstaende().at(i)->getRoomNo() == 1
                        && level->getGegenstaende().at(i)->getActivate()){
                    countTorches = countTorches + 1;
                }
            }
        }

        if(!threeTorchDialog && countTorches == 3) {
            return &dialogs.speakWithTutorial;
        }

        countTorches = 0;

        if(!fourTorchDialog ){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                if(level->getGegenstaende().at(i)->getType() == "Fackel" && level->getGegenstaende().at(i)->getRoomNo() == 1
                   && level->getGegenstaende().at(i)->getActivate()){
                    countTorches = countTorches + 1;
                }
            }
        }

        if(!fourTorchDialog && countTorches == 4){
            return &dialogs.toBright;
        }


    }

    if(level->getRoomNo() == 2){

        if(neighbourField == sf::Vector2f(700,150)){
            return &dialogs.fujin;
        }

        if(neighbourField == sf::Vector2f(450, 150)){
            return &dialogs.raijin;
        }

        if(neighbourField.x >= 500 && neighbourField.x < 700 && neighbourField.y == 200){
            return &dialogs.shivaFigure;
        }
        if(neighbourField == sf::Vector2f(350,100)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                if(level->getGegenstaende().at(i)->name == "Exit2_7" && !level->getGegenstaende().at(i)->getActivate()){
                    return &dialogs.dancingShiva;
                }
            }
        }

        if(neighbourField == sf::Vector2f(500, 250)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                if(level->getGegenstaende().at(i)->name == "ChestTalisman" && level->getGegenstaende().at(i)->getActivate()){
                    return &dialogs.talisman;
                }
            }
        }

        if(neighbourField == sf::Vector2f(650, 250)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                if(level->getGegenstaende().at(i)->name == "ChestDungeonMap" && level->getGegenstaende().at(i)->getActivate()){
                    return &dialogs.dungeonMap;
                }
            }
        }

        if(neighbourField == sf::Vector2f(100,350) || neighbourField == sf::Vector2f(150,350)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                if(level->getGegenstaende().at(i)->name == "Door2" && !level->getGegenstaende().at(i)->getActivate()){
                    return &dialogs.sealedDoor;
                }
            }
        }

        if(neighbourField == sf::Vector2f(550,800) || neighbourField == sf::Vector2f(600,800)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                if(level->getGegenstaende().at(i)->name == "Exit2_3-1" && !level->getGegenstaende().at(i)->getActivate()){
                    return &dialogs.closedDoorBig;
                }
            }
        }

        /*
        if(neighbourField == sf::Vector2f(500, 500) || neighbourField == sf::Vector2f(500,350)
           || neighbourField == sf::Vector2f(650, 350) || neighbourField == sf::Vector2f(650, 500) ){

            if(inventar->getChosenItem()->getName() == "Feuerstein"){
                return &dialogs.bowlFlintstone;
            }
            else{
                return &dialogs.bowl;
            }
        }
         */

        if(neighbourField == sf::Vector2f(500, 500)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                if(level->getGegenstaende().at(i)->getName() == "BowlViolett" && level->getGegenstaende().at(i)->getActivate() ){
                    return &dialogs.bowlOn;
                }
            }
            if(inventar->getChosenItem()->getName() == "Feuerstein"){
                return &dialogs.bowlFlintstone;
            } else{
                return &dialogs.bowl;
            }
        }

        if(neighbourField == sf::Vector2f(500, 350)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                if(level->getGegenstaende().at(i)->getName() == "BowlYellow" && level->getGegenstaende().at(i)->getActivate() ){
                    return &dialogs.bowlOn;
                }
            }
            if(inventar->getChosenItem()->getName() == "Feuerstein"){
                return &dialogs.bowlFlintstone;
            } else{
                return &dialogs.bowl;
            }
        }

        if(neighbourField == sf::Vector2f(650, 500)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                if(level->getGegenstaende().at(i)->getName() == "BowlGreen" && level->getGegenstaende().at(i)->getActivate() ){
                    return &dialogs.bowlOn;
                }
            }
            if(inventar->getChosenItem()->getName() == "Feuerstein"){
                return &dialogs.bowlFlintstone;
            } else{
                return &dialogs.bowl;
            }
        }

        if(neighbourField == sf::Vector2f(650, 350)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                if(level->getGegenstaende().at(i)->getName() == "BowlBlue" && level->getGegenstaende().at(i)->getActivate() ){
                    return &dialogs.bowlOn;
                }
            }
            if(inventar->getChosenItem()->getName() == "Feuerstein"){
                return &dialogs.bowlFlintstone;
            } else{
                return &dialogs.bowl;
            }
        }

        if((position.x == 550+25 || position.x == 600+25) && position.y == 700+25 && !bowlOn){
            return &dialogs.bowlOnFirst;
        }

        if(neighbourField == sf::Vector2f(650, 700)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                auto item = level->getGegenstaende().at(i);
                if (item->getName() == "Fackel2_l" && item->getActivate()) {
                    return &dialogs.commonTorchOn;
                }
            }
            return &dialogs.commonTorchOff;
        }

        if(neighbourField == sf::Vector2f(500, 700)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                auto item = level->getGegenstaende().at(i);
                if (item->getName() == "Fackel2_r" && item->getActivate()) {
                    return &dialogs.commonTorchOn;
                }
            }
            return &dialogs.commonTorchOff;
        }

    }



    if(level->getRoomNo() == 3){
        if(neighbourField == sf::Vector2f(450, 150) || neighbourField == sf::Vector2f(600, 150)
           || (neighbourField == sf::Vector2f(900, 150) && guardianActivate)){
            return &dialogs.guardian;
        }

        if((neighbourField.x == 500 || neighbourField.x == 550) && neighbourField.y == 600){
            return &dialogs.entrance;
        }

        if(neighbourField == sf::Vector2f(350,100)){
            return &dialogs.overgrownRelief;
        }
        if(neighbourField == sf::Vector2f(250, 100)){
            return &dialogs.sittingShiva;
        }
        if(neighbourField == sf::Vector2f(700, 100)){
            return &dialogs.femaleGod;
        }
        if(neighbourField == sf::Vector2f(800,100)){
            return &dialogs.dancingShiva;
        }

        if(!guardianActivate && position == sf::Vector2f(900+25,200+25) && direction == sf::Vector2f(0,-1)){
            //itemActivate = true;
            return &dialogs.guardianButton;
        }

        if(!room3switchActive && position == sf::Vector2f(100+25,100+25) && direction == sf::Vector2f(0,-1)){
            return &dialogs.room3Switch;
        }

        if(room3switchActive && position == sf::Vector2f(100+25,100+25) && direction == sf::Vector2f(0,-1)){
            return &dialogs.room3SwitchActive;
        }

        if(neighbourField == sf::Vector2f(750,400) && !guardianActivate){
            return &dialogs.closedDoor;
        }

        if((neighbourField == sf::Vector2f(150,350) || neighbourField == sf::Vector2f(150,400)) && !room3switchActive){
            return &dialogs.stairsGarden;
        }

        if(neighbourField == sf::Vector2f(350, 450) && !room3switchActive){
            return &dialogs.bassin;
        }

        if(neighbourField == sf::Vector2f(750, 300) && !room3chestOpen){
            return &dialogs.chestOpen;
        }

        if((position == sf::Vector2f(500+25, 150+25) || position == sf::Vector2f(550+25,150+25)) && !enterRoom3){
                    return &dialogs.ghost;
        }


    }


    if(level->getRoomNo() == 6){
        if(position == sf::Vector2f(50+25, 950+25) && !torchDialog){
            return &dialogs.torchOn;
        }
    }

    //room7
    if(level->getRoomNo() == 7){
        if(neighbourField  == sf::Vector2f(250,250)){
            return &dialogs.tombstone3;
        }

        if(neighbourField == sf::Vector2f(150,300)){
            return &dialogs.tombstone;
        }

        if(neighbourField == sf::Vector2f(300, 450)){
            return &dialogs.tombstone2;
        }

        if(neighbourField == sf::Vector2f(600,250)){
            return &dialogs.tombstone2;
        }

        if(neighbourField == sf::Vector2f(700,250)){
            return &dialogs.tombstone;
        }

        if(neighbourField == sf::Vector2f(600,350)){
            return &dialogs.tombstone;
        }

        if(neighbourField == sf::Vector2f(600,450)){
            return &dialogs.tombstone2;
        }

        if(neighbourField == sf::Vector2f(700,450)){
            return &dialogs.tombstone;
        }

        if(neighbourField == sf::Vector2f(450,150)){
            return &dialogs.shrine;
        }

        if(neighbourField == sf::Vector2f(550,150)){
            return &dialogs.eyeSwitch;
        }
        if(neighbourField == sf::Vector2f(450, 400)){
            for (int i = 0; i < level->getGegenstaende().size(); ++i) {
                if (level->getGegenstaende().at(i)->name == "ChestCrossbow" &&
                    level->getGegenstaende().at(i)->getActivate()) {
                    return &dialogs.crossbow;
                }
            }
        }

    }

    if(level->getRoomNo() == 8){
        if((neighbourField.x == 700 || neighbourField.x == 650) && neighbourField.y == 50){
            return &dialogs.reliefStory1;
        }

        if((neighbourField.x == 600 || neighbourField.x == 550) && neighbourField.y == 50){
            return &dialogs.reliefStory2;
        }

        if((neighbourField.x == 500 || neighbourField.x == 450) && neighbourField.y == 50){
            return &dialogs.reliefStory3;
        }

        if(neighbourField == sf::Vector2f(400, 50)){
            return &dialogs.reliefStory4;
        }

        if((neighbourField.x == 350 || neighbourField.x == 300) && neighbourField.y == 50){
            return &dialogs.reliefStory5;
        }

        if((neighbourField.x == 250 || neighbourField.x == 200) && neighbourField.y == 50){
            return &dialogs.reliefStory6;
        }
        if((neighbourField.x == 150 || neighbourField.x == 100) && neighbourField.y == 50){
            return &dialogs.reliefStory7;
        }

        if(neighbourField == sf::Vector2f(700, 450) || neighbourField == sf::Vector2f(900, 500)){
            return &dialogs.jarFull;
        }

        if(neighbourField == sf::Vector2f(800, 550) || neighbourField == sf::Vector2f(1000, 500)){
            return &dialogs.jarEmpty;
        }

        if((neighbourField.x == 900 || neighbourField.x == 950) && neighbourField.y == 300){
            return &dialogs.scale;
        }

        if(neighbourField == sf::Vector2f(850, 500)){
            return &dialogs.jarHermes;
        }


    }

    return nullptr;
}