//
// Created by das_maichen on 25.05.18.
//

#ifndef MYGAME_STORYSTATE2_H
#define MYGAME_STORYSTATE2_H


#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Sound.hpp>
#include "../Game.h"
#include "../Speechbubble.h"

class StoryState2 : public BasicState {
public:
    StoryState2(GameDataRef data);

    void inizialize();

    void handleInput();
    void update(float dt);
    void draw(float dt);

    int getChosenItem(){ return selectedItemIndex;};

private:
    GameDataRef data;

    sf::SoundBuffer bufferNextPage;
    sf::Sound nextPageSound;

    sf::Music music;

    sf::Text storyText;

    sf::View view;

    int selectedItemIndex;
    sf::Sprite storyImage;

    bool readyToPlay;
    bool nextPage;

    int pageNo;
    int maxPage;

    int difficulity;
    bool musicOn;
    bool soundOn;

    Speechbubble* speechbubble;

};



#endif //MYGAME_STORYSTATE2_H
