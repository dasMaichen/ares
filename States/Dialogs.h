//
// Created by das_maichen on 26.07.18.
//

#ifndef MYGAME_DIALOGS_H
#define MYGAME_DIALOGS_H

#include <vector>
#include "TextItem.h"


struct Dialogs {
    std::string ares = "Ares";
    std::string hermes = "Hermes";
    std::string hermesDark = "???";

    std::string aresImage = "CharacterAres";
    std::string hermesImage = "Hermes";
    std::string aresScared = "AresScared";
    std::string aresSmile = "AresSmile";
    std::string aresHurt = "AresHurt";
    std::string hermesEye = "HermesEye";

    //common
    std::vector<TextItem> commonTalisman = {
            {ares, aresImage, "Das ist ein Talisman. Damit kann man ruhelose Seelen bannen. "}
    };

    std::vector<TextItem> commonTorchOff = {
            {ares, aresImage, "Das ist eine Fackel. Vielleicht kann man sie mit einem Feuerstein anzuenden. "}
    };

    std::vector<TextItem> commonTorchOn = {
            {ares, aresImage, "Das ist eine angezuendete Fackel. "}
    };

    //room1
    std::vector<TextItem> gameStarted = {
            {ares, aresHurt, "Autsch, das tat weh... Wo bin ich hier? Ganz schoen finster... "},
            {hermesDark, hermesEye, "Na suuuuuper, jetzt ist einer durch das Loch in der Decke in meine Wohnung hinabgestuerzt. "
                                          "Was faellt dir eigentlich ein, he? "},
            {ares, aresScared, "Wah, wer spricht da? "},
            {hermesDark, hermesEye, "Ach ihr Menschen koennt ja nicht im Dunkeln sehen, richtig. Wie unpraktisch... "
                                          "Geh doch mal nach Sueden, da habe ich Feuersteine gesehen. Ich habe gehoert, "
                                          "ihr koennt damit Feuer machen. "}
    };

    std::vector<TextItem> itemTutorial = {
            {hermesDark, hermesEye, "Gut, Mensch. Jetzt hast du Feuersteine gefunden. "},
            {ares, aresImage, "Aehm... und wie benutze ich sie? "},
            {hermesDark, hermesEye, "Was denn? Das weisst du nicht? Du bist ja gerade nicht der Hellste. Ok, hoer mir gut zu. "},
            {hermesDark, hermesEye, "Drueck auf die M-Taste deiner Tastertur und du siehst dein Inventar. "
                                          "Mit den Pfeiltasten waehlst du dein gewuenschtes Item aus und bestaetigst diese Wahl mit der Space-Taste. "
                                          "Jetzt musst du dich nur vor eine Fackel stellen und schwupp geht dir ein Licht auf. "
            },
            {ares, aresSmile, "Ah, ok... ich versuch's... "}
    };

    std::vector<TextItem> speakWithTutorial = {
            {hermesDark, hermesImage, "Gut, das sollte doch hell genug fuer dich sein, Mensch. "},
            {ares, aresImage, "Oh, du... du bist eine ... "},
            {hermes, hermesImage, "Ja, eine Fledermaus. Und mein Name ist Hermes. Freut mich dich kennenzulernen. "},
            {ares, aresImage, "Kannst du mir sagen, wie ich aus dieser Hoehle rauskomme? "},
            {hermes, hermesImage, "Hm... also, ich fliege ja immer aus dem Loch in der Decke raus, woher du auch gekommen bist... "},
            {ares, aresImage, "Na toll... "},
            {hermes, hermesImage, "He! Es ist nicht meine Schuld, dass du nicht fliegen kannst. "
                                          "Aber jetzt wo es hell genug ist, kannst du dich ja in dieser Hoehle umschauen."
                                          " Stell dich vor einem Objekt und druecke die 'Space'-Taste. "
                                          "Dann siehst du dir die Objekte genauer an. Versuch das mal. "},
            {ares, aresImage, "Ok, davorstellen und 'Space' "}
    };

    std::vector<TextItem> toBright = {
            {hermes, hermesImage, "Bah! Das ist doch viel zu hell, Mensch... Na wenigstens hast du die Gitter aufbekommen. "}
    };

    std::vector<TextItem> deadMan = {
            {ares, aresImage, "Der sitzt schon eine ganze Weile hier... "}
    };

    std::vector<TextItem> animalBones = {
            {ares, aresImage, "Alte Tierknochen... "}
    };

    std::vector<TextItem> fireplace = {
            {ares, aresImage, "Das ist eine Kochstelle... "}
    };

    std::vector<TextItem> bed = {
            {ares, aresSmile, "Eine Schlafstaette, gemuetlich... "}
    };

    std::vector<TextItem> closedDoor = {
            {ares, aresImage, "Diese Gitter krieg ich nicht auf... "}
    };


    //room2
    std::vector<TextItem> fujin = {
            {ares, aresImage, "Eine grimmig schauende Figur... sie scheint einen laenglichen Sack zu tragen. "}
    };

    std::vector<TextItem> raijin = {
            {ares, aresImage, "Eine grimmig schauende Figur... sie scheint von Trommeln umgeben zu sein. "}
    };

    std::vector<TextItem> shivaFigure = {
            {ares, aresImage, "Es scheint eine Statue von einer alten Gottheit zu sein. Sie hat acht Arme. "}
    };
    std::vector<TextItem> dungeonMap = {
            {ares, aresImage, "Eine Truhe. "},
            {ares, aresImage, "Hm... ein altes Papierfetzen... "},
            {hermes, hermesImage, "Schau doch mal genauer hin, das ist eine Dungeon-Karte. "},
            {ares, aresImage, "Dungeon-Karte? "},
            {hermes, hermesImage, "Ja, geh in dein Inventar mit 'M' und geh nach rechts. "
                                          "Dann kannst du dir die Karte genauer anschauen. " }
    };

    std::vector<TextItem> talisman = {
            {ares, aresImage, "Eine Truhe... "},
            {ares,aresImage, "Ein altes Stueck Papier? "},
            {hermes, hermesImage, "Nein, das ist ein Talisman. Damit hat der Typ, der in meiner Hoehle verendet ist "
                                          "die ruhelosen Geister besaenftigt. "},
            {ares, aresImage, "Aha... und wie sollen die mir helfen? "},
            {hermes, hermesImage, "Keine Ahnung? "}
    };

    std::vector<TextItem> sealedDoor = {
            {ares, aresImage, "Diese Tuer ist verschlossen... da braucht man wohl einen Schluessel... "}
    };

    std::vector<TextItem> closedDoorBig = {
            {ares, aresImage, "Diese Tuer laesst sich nicht oeffnen. "}
    };

    std::vector<TextItem> bowl = {
            {ares, aresImage, "Scheint ein Kohlebecken zu sein. "}
    };

    std::vector<TextItem> bowlFlintstone = {
            {hermes, hermesImage, "Was machst du denn da? "},
            {ares, aresImage, "Der Kohlebecken laesst sich nicht mit dem Feuerstein anzuenden. "},
            {hermes, hermesImage, "Hm... vielleicht brauchst du etwas anderes. "}
    };

    std::vector<TextItem> bowlOnFirst = {
            {ares, aresImage, "Hast du das gehoert? "},
            {hermes, hermesImage, "Das war der Geist... Oh schau mal! Der Kohlebecken ist an. "},
    };

    std::vector<TextItem> bowlOn = {
            {ares, aresImage, "Dieser Kohlebecken ist angegangen als ich die ruhelose Seele besaenftigt habe. "}
    };


    //room3
    std::vector<TextItem> guardian = {
            {ares, aresImage, "Scheint ein uralter Waechter zu sein... "}
    };

    std::vector<TextItem> entrance = {
            {ares, aresImage, "Hier muss wohl der eigentliche Zugang zu diesem uralten Bau gewesen sein... "
                                      "Jetzt ist er aber zugeschuettet... ich muss wohl einen anderen Weg hier raus finden... "}
    };

    std::vector<TextItem> overgrownRelief = {
            {ares, aresImage, "Das Bild ist zugewachsen, ich kann es nicht erkennen. "}
    };

    std::vector<TextItem> sittingShiva = {
            {ares, aresImage, "Das Bild zeigt eine sitzende Gottheit mit sechs Armen. "}
    };

    std::vector<TextItem> femaleGod = {
            {ares, aresImage, "Dieses Bild zeigt moeglicherweise eine weibliche Gottheit. "}
    };

    std::vector<TextItem> dancingShiva = {
            {ares, aresImage, "Eine achtarmige Gottheit, sie scheint zu tanzen. "}
    };

    std::vector<TextItem> guardianButton = {
            {ares, aresImage, "Wohl ein uralter Waechter... Oh, in seinem Maul ist ein Schalter. "},
            {ares, aresImage, "Das Geraeusch kam von dem kleinen Bau. "}
    };

    std::vector<TextItem> room3Switch = {
            {ares, aresImage, "Scheint eine Art Schalter zu sein. Was wohl passiert, wenn man dran dreht? "}
    };

    std::vector<TextItem> room3SwitchActive = {
            {ares, aresImage, "Das habe ich schon aktiviert. "}
    };

    std::vector<TextItem> stairsGarden = {
            {ares, aresImage, "Nein, das ist viel zu hoch. Da komme ich nicht rauf. "}
    };

    std::vector<TextItem> bassin = {
            {ares, aresImage, "Das ist ein Wasserbassin. "}
    };

    std::vector<TextItem> chestOpen = {
            {ares,aresImage,"Eine Truhe... "},
            {ares, aresSmile, "Oh, ein alter Schluessel... "}
    };

    std::vector<TextItem> ghost = {
            {ares, aresScared, "Wa... was ist das denn? "},
            {hermes, hermesImage, "Ein Geist. Eine ruhelose Seele, die in dieser Welt noch gefangen ist, "
                                          "ein Talisman koennte den Geist beruhigen... "}
    };


    //room5
    std::vector<TextItem> stele = {
            {ares, aresImage, "Hm... eine Stele. Da steht etwas geschrieben, aber diese uralte Sprache beherrsche ich nicht. "}
    };

    //room6
    std::vector<TextItem> torchOn = {
            {ares, aresImage, "Ganz schoen finster hier... "},
            {hermes, hermesEye, "Zeit deine Fackel zu benutzen. "},
            {ares, aresSmile, "Gute Idee. "}
    };

    //room7
    std::vector<TextItem> cemetery = {
            {ares, aresImage, "Oh, ein Friedhof. "}
    };

    std::vector<TextItem> tombstone = {
            {ares, aresImage, "Das ist ein alter Grabstein, er ist ganz verwittert. "}
    };

    std::vector<TextItem> tombstone2 = {
            {ares, aresImage, "Ein alter Grabstein, es steht vermutlich etwas geschrieben, aber die Sprache kenne ich nicht. "}
    };

    std::vector<TextItem> tombstone3 = {
            {ares, aresImage, "Dieser Grabstein hat schon viel abbekommen. "}
    };

    std::vector<TextItem> tombstoneBig = {
            {ares, aresImage, "Ein grosser Grabstein. Vielleicht liegt hier eine ehemals bedeutende Persoenlichkeit. "}
    };

    std::vector<TextItem> shrine = {
            {ares, aresImage, "Das ist ein Schrein. Drinnen steht ein Bildnis der achtarmigen Gottheit. "
                                      "Man kann noch die Ueberreste der Opfergaben erkennen. "},
            {ares, aresImage, "Und Talismane... "},
            {hermes, hermesImage, "Du solltest die mitnehmen. "},
            {ares, aresImage, "Ernsthaft? "}
    };

    std::vector<TextItem> crossbow = {
            {ares, aresImage, "Eine altertuemliche Armbrust. Aber wie benutze ich sie? "},
            {hermes, hermesImage, "Du bist aber auch ungeschickt. Waehle die Armbrust als aktives Item aus. "
                                          "Mit der Taste 'B' schiesst du. Aber sei vorsichtig. "}
    };

    std::vector<TextItem> eyeSwitch = {
            {ares, aresImage, "Hm... sieht aus wie ein Auge. In der Mitte ist eine Einkerbung. "}
    };



    //room8
    std::vector<TextItem> reliefStory1 = {
            {ares, aresImage, "Eine friedliche Szene. Bauern arbeiten zufrieden auf den Feldern. "}
    };

    std::vector<TextItem> reliefStory2 = {
            {ares, aresImage, "Ein Erdloch tut sich auf und ein Daemon erscheint. Um ihm herum fliegen "
                                      "nachtschwarze Schmetterlinge. "}
    };

    std::vector<TextItem> reliefStory3 = {
            {ares, aresImage, "Die Menschen fluechten vor den schwarzen Schmetterlingen. Einige liegen auf dem Boden. "}
    };

    std::vector<TextItem> reliefStory4 = {
            {ares, aresImage, "Hier ist eine bewaffnete Person dargestellt. Vielleicht ein Held? "}
    };

    std::vector<TextItem> reliefStory5 = {
            {ares, aresImage, "Die bewaffnete Person stellt sich dem Daemon. " }
    };

    std::vector<TextItem> reliefStory6 = {
            {ares, aresImage, "Der Held besiegt den Daemon. Die schwarzen Schmetterlinge fallen hinab. " }
    };
    std::vector<TextItem> reliefStory7 = {
            {ares, aresImage, "Die Menschen bejubeln den Helden. Die Landschaft blueht auch auf. " }
    };

    std::vector<TextItem> jarEmpty = {
            {ares, aresImage, "Da ist nichts drin. "}
    };

    std::vector<TextItem> jarFull = {
            {ares, aresImage, "Urgh, ich will nicht wissen, was das ist... "}
    };

    std::vector<TextItem> jarHermes = {
            {ares, aresImage, "Urgh, da drinnen kriecht etwas... "},
            {hermes, hermesImage, "Mmmh, lecker Kakerlaken. "}
    };

    std::vector<TextItem> scale = {
            {ares, aresImage, "Da steht eine goldene Waage und ein paar Figuren aus Marmor. "}
    };

    std::vector<TextItem> torchOff = {
            {ares, aresImage, "Oh, die Fackel ist ausgegangen... "}
    };

};

#endif //MYGAME_DIALOGS_H
