//
// Created by das_maichen on 12.05.18.
//

#include <iostream>
#include <sstream>
#include <fstream>
#include "GameStateJumpNRun.h"
#include "../DEFINITIONS.h"
#include "../Player/PlayerJumpNRun.h"
#include "GameOverState.h"
#include "StoryState2.h"

void resizeViewGameState(const sf::RenderWindow& window, sf::View& view){
    float aspectRatio = float(window.getSize().x)/float(window.getSize().y);
    view.setSize(SCREEN_HEIGHT * aspectRatio, SCREEN_HEIGHT);
}


GameStateJumpNRun::GameStateJumpNRun(GameDataRef data, int difficulity, bool musicOn, bool soundOn) :
        data(data), view(sf::FloatRect(0,0,SCREEN_WIDTH, SCREEN_HEIGHT)) {
    this->difficulity = difficulity;
    this->musicOn = musicOn;
    this->soundOn = soundOn;

}

void GameStateJumpNRun::inizialize(){

    resizeViewGameState(this->data->window, view);
    view.setCenter(view.getCenter().x + 200, view.getCenter().y);
    startTime = clock.getElapsedTime().asSeconds();

    startGame = false;
    pauseModus = false;
    waiting = false;

    debugModus = false;
    gameState = READY;

    startWaitingTime = clock.getElapsedTime().asSeconds();

    this->data->assets.loadTexture("background", "Content/background_behind.png");
    this->data->assets.loadTexture("backgroundIntermediate", "Content/background_intermediate.png");

    //this->data->assets.loadTexture("JumpNRunBackground", JUMP_N_RUN_BACKGROUND_FILEPATH)
    this->data->assets.loadTexture("JumpNRunBackgroundPart1", JUMP_N_RUN_BACKGROUND_PART1_FILEPATH);
    this->data->assets.loadTexture("JumpNRunBackgroundPart2", JUMP_N_RUN_BACKGROUND_PART2_FILEPATH);
    //this->data->assets.loadTexture("gras", JUMP_N_RUN_BACKGROUND_2_FILEPATH);
    //this->data->assets.loadTexture("foreground", JUMP_N_RUN_FOREGROUND_FILEPATH);
    this->data->assets.loadTexture("foreground_part1", JUMP_N_RUN_FOREGROUND_PART1_FILEPATH);
    this->data->assets.loadTexture("foreground_part2", JUMP_N_RUN_FOREGROUND_PART2_FILEPATH);
    this->data->assets.loadTexture("aresStanding", "Content/ares_standing.png");

    this->data->assets.loadTexture("ArrowKey", "Content/Texture/arrowKeys.png");
    this->data->assets.loadTexture("PKey", "Content/Texture/PKey.png");
    this->data->assets.loadTexture("SpaceKey", "Content/Texture/spaceKey.png");

    if(!music.openFromFile("Content/Music/run-faster.ogg")){
        std::cout << "Fehler beim Musik laden." << std::endl;
    }

    music.setLoop(true);
    if(musicOn){
        music.play();
    }

    bufferHitSound.loadFromFile("Content/Sounds/hitSound.wav");
    hitSound.setBuffer(bufferHitSound);

    controlInfo.setTexture(this->data->assets.getTexture("OptionBox"));
    controlInfo.setScale(0.3,0.3);
    controlInfo.setPosition(170,70);
    controlInfo.setColor(sf::Color(255,255,255,200));

    controlInfoTitle.setFont(this->data->assets.getFont("GameFont"));
    controlInfoTitle.setCharacterSize(30);
    controlInfoTitle.setColor(sf::Color::Cyan);
    controlInfoTitle.setPosition(300, 100);
    controlInfoTitle.setString("Control");

    controlArrowKey.setFont(this->data->assets.getFont("GameFont"));
    controlArrowKey.setCharacterSize(20);
    controlArrowKey.setColor(sf::Color::White);
    controlArrowKey.setPosition(300, 170);
    controlArrowKey.setString("Move left and right");
    arrowKey.setTexture(this->data->assets.getTexture("ArrowKey"));
    arrowKey.setScale(0.6,0.6);
    arrowKey.setPosition(200, 170);

    controlSpace.setFont(this->data->assets.getFont("GameFont"));
    controlSpace.setCharacterSize(20);
    controlSpace.setColor(sf::Color::White);
    controlSpace.setPosition(400, 220);
    controlSpace.setString("Jump");
    spaceKey.setTexture(this->data->assets.getTexture("SpaceKey"));
    spaceKey.setScale(0.6,0.6);
    spaceKey.setPosition(200, 220);

    controlPause.setFont(this->data->assets.getFont("GameFont"));
    controlPause.setCharacterSize(20);
    controlPause.setColor(sf::Color::White);
    controlPause.setPosition(250, 270);
    controlPause.setString("Press 'P' to pause");
    pKey.setTexture(this->data->assets.getTexture("PKey"));
    pKey.setScale(0.6,0.6);
    pKey.setPosition(200, 270);


    controlReturn.setFont(this->data->assets.getFont("GameFont"));
    controlReturn.setCharacterSize(20);
    controlReturn.setColor(sf::Color::White);
    controlReturn.setPosition(220, 350);
    controlReturn.setString("Press 'Return' to start Game");




    this->data->assets.loadTexture("AresAnimation", ARES_ANIMATION_FILEPATH);
    //player = new PlayerJumpNRun("Content/ares_pixel_animation_probe.png", sf::Vector2u(8,3), 0.1f, 100.0f, 200);


    player = new PlayerJumpNRun(this->data->assets.getTexture("AresAnimation"), sf::Vector2u(8,3), 0.08f, ARES_SPEED,
                                ARES_JUMP_HEIGHT);

    startPlayerSprite.setTexture(this->data->assets.getTexture("aresStanding"));
    startPlayerSprite.setOrigin(startPlayerSprite.getGlobalBounds().width/2, startPlayerSprite.getGlobalBounds().height/2);
    startPlayerSprite.setScale(0.32,0.32);
    startPlayerSprite.setPosition(625,400);


    lives = new Lives(data);
    lives->setPosition(sf::Vector2f(view.getCenter().x + view.getSize().x * 0.25, view.getCenter().y - 250));

    this->data->assets.loadTexture("Plattform", PLATTFORM_FILEPATH);
    plattform = new Plattforms(data);
    //plattform->loadPlattformPosition("Content/plattform_position_probe.level");
    plattform->loadPlattformPosition("Content/ground.level");

    this->data->assets.loadTexture("Stein", OBSTACLE_STONE_FILEPATH);
    this->data->assets.loadTexture("Ast_Links", OBSTACLE_BRANCH_LEFT_FILEPATH);
    this->data->assets.loadTexture("Ast_Rechts", OBSTACLE_BRANCH_RIGHT_FILEPATH);
    this->data->assets.loadTexture("Ast_Links_Klein", OBSTACLE_BRANCH_LEFT_SMALL_FILEPATH);
    this->data->assets.loadTexture("Ast_Rechts_Klein", OBSTACLE_BRANCH_RIGHT_SMALL_FILEPATH);
    this->data->assets.loadTexture("Baumkrone", OBSTACLE_TREETOP_FILEPATH);
    this->data->assets.loadTexture("Stein_eins", OBSTACLE_STONE1_FILEPATH);
    this->data->assets.loadTexture("Stein_zwei", OBSTACLE_STONE2_FILEPATH);
    this->data->assets.loadTexture("Stein_Klein", OBSTACLE_STONE_SMALL_FILEPATH);
    this->data->assets.loadTexture("Hohler_Baum", OBSTACLE_HOLLOW_TREE_FILEPATH);
    this->data->assets.loadTexture("Blume", OBSTACLE_FLOWER_FILEPATH);

    this->data->assets.loadTexture("Hirsch", ENEMIES_DEER_FILEPATH);

    this->data->assets.loadTexture("EnemyDeerFrame1", ENEMY_FRAME_1_FILEPATH);
    this->data->assets.loadTexture("EnemyDeerFrame2", ENEMY_FRAME_2_FILEPATH);
    this->data->assets.loadTexture("EnemyDeerFrame3", ENEMY_FRAME_3_FILEPATH);
    this->data->assets.loadTexture("EnemyDeerFrame4", ENEMY_FRAME_4_FILEPATH);
    this->data->assets.loadTexture("EnemyDeerFrame5", ENEMY_FRAME_5_FILEPATH);
    this->data->assets.loadTexture("EnemyDeerFrame6", ENEMY_FRAME_6_FILEPATH);
    this->data->assets.loadTexture("EnemyDeerFrame7", ENEMY_FRAME_7_FILEPATH);
    this->data->assets.loadTexture("EnemyDeerFrame8", ENEMY_FRAME_8_FILEPATH);
    this->data->assets.loadTexture("EnemyDeerFrame9", ENEMY_FRAME_9_FILEPATH);

    this->data->assets.loadTexture("EnemyDeer2Frame1", "Content/Texture/Hirsch/deer1.png");
    this->data->assets.loadTexture("EnemyDeer2Frame2", "Content/Texture/Hirsch/deer2.png");
    this->data->assets.loadTexture("EnemyDeer2Frame3", "Content/Texture/Hirsch/deer3.png");
    this->data->assets.loadTexture("EnemyDeer2Frame4", "Content/Texture/Hirsch/deer4.png");
    this->data->assets.loadTexture("EnemyDeer2Frame5", "Content/Texture/Hirsch/deer5.png");
    this->data->assets.loadTexture("EnemyDeer2Frame6", "Content/Texture/Hirsch/deer6.png");
    this->data->assets.loadTexture("EnemyDeer2Frame7", "Content/Texture/Hirsch/deer7.png");
    this->data->assets.loadTexture("EnemyDeer2Frame8", "Content/Texture/Hirsch/deer8.png");
    this->data->assets.loadTexture("EnemyDeer2Frame9", "Content/Texture/Hirsch/deer9.png");


    this->data->assets.loadTexture("Butterfly1", "Content/Texture/butterfly1.png");
    this->data->assets.loadTexture("Butterfly2", "Content/Texture/butterfly2.png");
    this->data->assets.loadTexture("Butterfly3", "Content/Texture/butterfly3.png");


    if(difficulity == 0){
        setEnemies("Content/enemies.enemies");
    }
    if(difficulity == 1){
        setEnemies("Content/enemies_normal.enemies");
    }
    if(difficulity == 2){
        setEnemies("Content/enemies_hard.enemies");
    }



    //butterfly = new Butterfly(data,"butterfly", sf::Vector2f(100,0), sf::Vector2f(600,0), sf::Vector2f(2,0));

    //enemy = new Enemy(data, "deer", sf::Vector2f(3000,430), sf::Vector2f(4000,0));
    //enemy = new Enemy(data, "deer", sf::Vector2f(2700,430), sf::Vector2f(3000,0), sf::Vector2f(6,0));

    //enemy2 = new Enemy(data, "deer", sf::Vector2f(300,430), sf::Vector2f(900,0), sf::Vector2f(5,0));

    //enemies.push_back(enemy);
    //enemies.push_back(enemy2);

    obstacle = new Obstacle(data);
    //plattform->loadPlattformPosition("Content/plattform_position_probe.level");

    obstacle->loadObstaclePosition("Content/trees.level");

    //movableStone.setSize(sf::Vector2f(150,150));
    movableStone.setSize(sf::Vector2f(140,140));
    movableStone.setOrigin(70,70);
    //movableStone.setOrigin(75,75);
    movableStoneTexture.loadFromFile("Content/Texture/movableStone.png");

    movableStoneSprite.setTexture(movableStoneTexture);
    movableStoneSprite.setOrigin(75,75);

    //movableStone.setTexture(&movableStoneTexture);
    //movableStone.setFillColor(sf::Color::Cyan);
    movableStone.setPosition(13750,450);
    movableStoneSprite.setPosition(movableStone.getPosition());


    backgroundPart1.setTexture(this->data->assets.getTexture("JumpNRunBackgroundPart1"));
    backgroundPart2.setTexture(this->data->assets.getTexture("JumpNRunBackgroundPart2"));
    //background.setTexture(this->data->assets.getTexture("JumpNRunBackground"));
    //background2.setTexture(this->data->assets.getTexture("gras"));
    //background2.setColor(sf::Color(255,255,255,200));
    //background2.setPosition(0,470);
    //background2.setPosition(0,-200);
    backgroundPart1.setPosition(0,-200);
    backgroundPart2.setPosition(10000,-200);

    foregroundPart1.setTexture(this->data->assets.getTexture("foreground_part1"));
    foregroundPart2.setTexture(this->data->assets.getTexture("foreground_part2"));
    foregroundPart1.setPosition(0,-200);
    foregroundPart2.setPosition(10000,-200);
    //foreground.setTexture(this->data->assets.getTexture("foreground"));
    //foreground.setScale(0.5f, 0.5f);
    //foreground.setPosition(0,510);
    //foreground.setPosition(0,-200);
    //butterflies.setFillColor(sf::Color(27,27,27,128));
    //butterflies.setOrigin(400,-200);
    //butterflies.setRadius(300);

    background2.setTexture(this->data->assets.getTexture("background"));
    background2.setScale(0.8f,0.8f);
    background2.setPosition(0, -200);

    backgroundIntermediate.setTexture(this->data->assets.getTexture("backgroundIntermediate"));
    backgroundIntermediate.setScale(0.8f, 0.8f);
    backgroundIntermediate.setPosition(0, -150);

    pauseMenu.setTexture(this->data->assets.getTexture("StoryBox"));
    pauseMenu.setScale(sf::Vector2f(0.2,0.2));
    //pauseMenu.setPosition(200,200);
    //pauseMenu.setColor(sf::Color(255,255,255,200));
    pauseText.setFont(this->data->assets.getFont("GameFont"));
    pauseText.setCharacterSize(40);
    pauseText.setColor(sf::Color::Cyan);
    pauseText.setString("Pause");
    continueText.setFont(this->data->assets.getFont("GameFont"));
    continueText.setCharacterSize(20);
    continueText.setColor(sf::Color::White);
    continueText.setString("Press 'P' to continue.");
}


void GameStateJumpNRun::handleInput(){
    sf::Event event;

    while (this->data->window.pollEvent(event))
    {

        switch (event.type){
            case sf::Event::Closed:
                this->data->window.close();
                break;
            case sf::Event::Resized:
                resizeViewGameState(this->data->window,view);
                break;

            case sf::Event::KeyReleased:
                switch(event.key.code){
                    case sf::Keyboard::D:
                        if(debugModus){
                            debugModus = false;
                        } else{
                            debugModus = true;
                        }
                        plattform->setDebugModus(debugModus);
                        obstacle->setDebugModus(debugModus);
                        player->setDebugModus(debugModus);
                        for (int i = 0; i < enemies.size(); i++){
                            enemies.at(i)->setDebugModus(debugModus);
                        }
                        break;


                    case sf::Keyboard::Return:
                        startGame = true;
                        break;
                    case sf::Keyboard::P:
                        if(pauseModus){
                            pauseModus = false;
                        }else if(!pauseModus){
                            pauseModus = true;
                        }
                        break;

                }
                break;
        }
    }
}

void GameStateJumpNRun::update(float dt) {


    //Game Over
    if (gameState == GAMEOVER) {
        GameOverState *gameOverState = new GameOverState(this->data, this->difficulity, this->musicOn, this->soundOn);

        data->machine.AddState(StateRef(gameOverState), true);

        music.stop();
    }

    //Next Level
    if (gameState == LEVEL_1_CLEAR) {
        StoryState2 *storyState2 = new StoryState2(this->data);

        data->machine.AddState(StateRef(storyState2), true);

        music.stop();
    }

    //Start Level 1
    //if (clock.getElapsedTime().asSeconds() > JUMP_N_RUN_BEGINN_WAITING_TIME) {


    if (startWaitingTime + 1 < clock.getElapsedTime().asSeconds()) {
        waiting = false;
    }

    if (pauseModus) {
        pauseMenu.setPosition(view.getCenter().x - 250, view.getCenter().y - 200);
        pauseText.setPosition(view.getCenter().x - 60, view.getCenter().y - 190);
        continueText.setPosition(view.getCenter().x - 100, view.getCenter().y - 140);
    }


    if (!startGame || pauseModus || waiting) {
        return;
    }


    //Wenn man fällt
    if (player->getPosition().y > this->data->window.getSize().y) {
        lives->countDown();
        int remainingLives = 3 - lives->getNumberOfLives();

        waiting = true;
        startWaitingTime = clock.getElapsedTime().asSeconds();


        if (lives->getNumberOfLives() == 0) {
            gameState = GAMEOVER;
        } else if (remainingLives <= 3) {
            view.setCenter(view.getCenter().x - 800, view.getCenter().y);
            player->setPosition(sf::Vector2f(view.getCenter().x, view.getCenter().y - 100));
            //background.setPosition(background.getPosition().x - 800, background.getPosition().y);
            lives->setTransparency(remainingLives);

            for (int i = 0; i < enemies.size(); ++i) {
                if(enemies.at(i)->getType() == "butterfly"){
                    enemies.at(i)->setPosition(sf::Vector2f(view.getCenter().x - 400, 190));
                }
            }
        }
    }



    //View Positionen
    if (player->getPosition().y < 250) {
        if (view.getCenter().y < 150) {
            view.move(0, 0);
        } else {
            view.move(0, -2);
        }
    } else if (player->getPosition().y > 250) {
        if (view.getCenter().y >= 300) {
            view.move(0, 0);
        } else {
            view.move(0, 3);
        }
    }

    //if(player->getPosition().x > 15000){
    if (view.getCenter().x > 15300) {
        view.move(0, 0);
        for (int i = 0; i < enemies.size(); ++i) {
            if(enemies.at(i)->getType() == "butterfly"){
                enemies.at(i)->setPosition(sf::Vector2f(view.getCenter().x - 400, 190));
            }
        }
        background2.move(0, 0);
        backgroundIntermediate.move(0, 0);
    } else {
        view.move(3, 0);
        background2.move(1, 0);
        backgroundIntermediate.move(0.5, 0);
    }


    //updates
    player->update(dt);
    for(int i = 0; i < enemies.size(); i ++){
        enemies.at(i)->update(dt, view.getCenter());
    }
    //enemy->update(dt, view.getCenter());
    //enemy2->update(dt, view.getCenter());

    //butterfly->update(dt, view.getCenter());

    Collider colliderPlayer(player->getCollisionsBox());


    //Kollision mit dem Ground
    std::vector<Collider> colliderVectorPlattform;
    for (int i = 0; i < this->plattform->plattformBody.size(); i++) {
        Collider colliderPlattform(plattform->plattformBody.at(i));
        colliderVectorPlattform.push_back(colliderPlattform);
    }

    std::vector<sf::Sprite> plattformSprites = plattform->getSprites();
    sf::Vector2f direction;
    for (int i = 0; i < plattformSprites.size(); i++) {

        if (colliderVectorPlattform.at(i).checkCollision(colliderPlayer, direction, 1.0f)) {
            player->onCollision(direction);
        }
    }


    //Kollision mit dem schiebbaren Stein
    Collider movableStoneCollider(movableStone);
    if (movableStoneCollider.checkCollision(colliderPlayer, direction, 0.5f)) {
        player->onCollision(direction);
        //movableStone.rotate(2);
        movableStoneSprite.rotate(2);
    }
    if (movableStone.getPosition().x > 14300) {
        movableStone.move(0, 6);
    }
    if(movableStone.getPosition().y > 700){
        movableStone.setPosition(13750,450);
    }
    movableStoneSprite.setPosition(movableStone.getPosition());


    //Kollision mit Hindernissen
    std::vector<Collider> obstacleColliders;
    for (int i = 0; i < this->obstacle->obstacleBody.size(); i++) {
        Collider colliderPlattform(obstacle->obstacleBody.at(i));
        obstacleColliders.push_back(colliderPlattform);
    }

    std::vector<sf::Sprite> obstacleSprites = obstacle->getSprites();
    for (int i = 0; i < obstacleSprites.size(); i++) {
        if (obstacleColliders.at(i).checkCollision(colliderPlayer, direction, 1.0f)) {
            player->onCollision(direction);
        }
    }


    for(int i = 0; i < this->enemies.size(); i++){
        if(enemies.at(i)->getInGame()){
            Collider colliderEnemy(enemies.at(i)->getCollisionBox());
            if (colliderEnemy.checkCollision(colliderPlayer, direction, 1.0f) ||
                player->getPosition().x < view.getCenter().x - view.getSize().x / 2) {
                player->getBody().setFillColor(sf::Color(255, 255, 255, 128));

                if (clock.getElapsedTime().asSeconds() > startTime) {
                    startTime = clock.getElapsedTime().asSeconds() + UNVULNERABLE_DURATION;
                    lives->countDown();
                    int remainingLives = 3 - lives->getNumberOfLives();

                    if (lives->getNumberOfLives() == 0) {
                        gameState = GAMEOVER;
                    } else if (remainingLives <= 3) {
                        lives->setTransparency(remainingLives);
                        hitSound.play();
                    }
                }
            }
        }
    }

    /*
    if (enemy->getInGame()) {
    //Kollision mit Gegnern und unverwundbarer Zustand des Players
        Collider colliderEnemy(enemy->getCollisionBox());
        if (colliderEnemy.checkCollision(colliderPlayer, direction, 1.0f) ||
            player->getPosition().x < view.getCenter().x - view.getSize().x / 2) {
            player->getBody().setFillColor(sf::Color(255, 255, 255, 128));

            if (clock.getElapsedTime().asSeconds() > startTime) {
                startTime = clock.getElapsedTime().asSeconds() + UNVULNERABLE_DURATION;
                lives->countDown();
                int remainingLives = 3 - lives->getNumberOfLives();

                if (lives->getNumberOfLives() == 0) {
                    gameState = GAMEOVER;
                } else if (remainingLives <= 3) {
                    lives->setTransparency(remainingLives);
                }
            }
        }
    }
     */
    if(clock.getElapsedTime().asSeconds() > startTime){
        player->getBody().setFillColor(sf::Color::White);
    }

    lives->setPosition(sf::Vector2f(view.getCenter().x + view.getSize().x * 0.25, view.getCenter().y - 250));



    if(player->getPosition().x > 15700){
        gameState = LEVEL_1_CLEAR;
    }
}


void GameStateJumpNRun::draw(float dt) {
    this->data->window.clear(sf::Color(200,200,200));
    this->data->window.setView(view);

    //this->data->window.draw(this->background);
    this->data->window.draw(this->background2);
    this->data->window.draw(this->backgroundIntermediate);
    this->data->window.draw(this->backgroundPart1);
    this->data->window.draw(this->backgroundPart2);

    plattform->drawPlattforms();
    obstacle->drawObstacle();
    //this->data->window.draw(movableStone);
    this->data->window.draw(movableStoneSprite);

    //if (clock.getElapsedTime().asSeconds() > JUMP_N_RUN_BEGINN_WAITING_TIME){
    //    player->draw(this->data->window);
    //}

    if(startGame){
        player->draw(this->data->window);
    }

    for(int i = 0; i < enemies.size(); i++){
        enemies.at(i)->draw();
    }

    //butterfly->draw();


    //enemy->draw();
    /*
    if(player->getPosition().x > 3500) {
        enemy->draw();
    }
     */

    //enemy2->draw();

    //this->data->window.draw(this->foreground);
    this->data->window.draw(this->foregroundPart1);
    this->data->window.draw(this->foregroundPart2);
    lives->draw();
    //this->data->window.draw(this->butterflies);
    if(pauseModus){
        this->data->window.draw(pauseMenu);
        this->data->window.draw(pauseText);
        this->data->window.draw(continueText);
    }


    if(!startGame){
        this->data->window.draw(startPlayerSprite);
        this->data->window.draw(controlInfo);
        this->data->window.draw(controlInfoTitle);
        this->data->window.draw(controlArrowKey);
        this->data->window.draw(arrowKey);
        this->data->window.draw(controlSpace);
        this->data->window.draw(spaceKey);
        this->data->window.draw(controlPause);
        this->data->window.draw(pKey);
        this->data->window.draw(controlReturn);
    }

    this->data->window.display();
}

GameStateJumpNRun::~GameStateJumpNRun() {
    delete player;
    delete plattform;
    delete lives;
    delete obstacle;

}



void GameStateJumpNRun::setEnemies(std::string filename) {

    std::ifstream infile(filename);

    if(!infile.good()){
        std::cerr << "could not open enemy-file: " << filename << std::endl;
        return;
    }

    std::string output_string;

    while (std::getline(infile, output_string)) {


        std::string coordinates(output_string);
        std::string::size_type size;

        std::string delimiter = " ";

        size_t pos = 0;
        std::string zwischenprodukt;

        std::vector<std::string> stringVector;

        std::string x_component;
        std::string y_component;
        std::string spriteTexture;

        while ((pos = coordinates.find(delimiter)) != std::string::npos) {
            stringVector.push_back(coordinates.substr(0, pos));
            zwischenprodukt = coordinates.erase(0, pos + delimiter.length());
            stringVector.push_back(zwischenprodukt);
        }

        for(int i = 1; i <= stringVector.size()/2 + 2; i++){
            stringVector.erase(stringVector.begin() + i);
        }

        std::string name = stringVector.at(0);

        std::string type = stringVector.at(1);

        float positionX = std::stof (stringVector.at(2),&size);
        float positionY = std::stof (stringVector.at(3),&size);

        float startPositionX = std::stof (stringVector.at(4),&size);
        float startPositionY = std::stof (stringVector.at(5),&size);

        float speedX = std::stof(stringVector.at(6),&size);
        float speedY = std::stof(stringVector.at(7),&size);

        Enemy* enemy = new Enemy(this->data, type, sf::Vector2f(positionX, positionY), sf::Vector2f(startPositionX,startPositionY),
        sf::Vector2f(speedX,speedY));
        this->enemies.push_back(enemy);
    }

}

