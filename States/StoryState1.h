//
// Created by das_maichen on 29.05.18.
//

#ifndef MYGAME_STORYSTATE1_H
#define MYGAME_STORYSTATE1_H


#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Sound.hpp>
#include "BasicState.h"
#include "../Game.h"
#include "../Speechbubble.h"

class StoryState1 : public BasicState{

public:
    StoryState1(GameDataRef data, int difficulity, bool musicOn, bool soundOn);

    void inizialize();

    void handleInput();
    void update(float dt);
    void draw(float dt);

    void goToNextScene();
    void skipStory();
    int getPageNo(){ return pageNo;};

private:
    GameDataRef data;

    sf::Music music;
    sf::SoundBuffer bufferNextPage;
    sf::Sound nextPageSound;

    sf::Sprite storyImage;
    sf::Sprite storyBox;
    sf::Sprite nextButton;

    sf::RectangleShape textBox;

    sf::Text text1;

    sf::View view;

    bool readyToPlay;
    bool nextPage;
    int pageNo;
    int maxPage;

    int difficulity;
    bool musicOn;
    bool soundOn;

    Speechbubble* speechbubble;

};


#endif //MYGAME_STORYSTATE1_H
