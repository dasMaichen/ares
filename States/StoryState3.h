//
// Created by das_maichen on 07.08.18.
//

#ifndef MYGAME_STORYSTATE3_H
#define MYGAME_STORYSTATE3_H


#include <SFML/Audio/Music.hpp>
#include "BasicState.h"
#include "../Game.h"
#include "../Speechbubble.h"

class StoryState3 : public BasicState {
public:
        StoryState3(GameDataRef data);

        void inizialize();

        void handleInput();
        void update(float dt);
        void draw(float dt);

        int getChosenItem(){ return selectedItemIndex;};

private:
        GameDataRef data;

        sf::Music music;

        sf::View view;

        int selectedItemIndex;

        bool backToMainMenu;

        int difficulity;
        bool musicOn;
        bool soundOn;
    Speechbubble* speechbubble;
    sf::Sprite image;


};



#endif //MYGAME_STORYSTATE3_H
