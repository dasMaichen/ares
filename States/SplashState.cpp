#include <sstream>
#include "SplashState.h"
#include "../DEFINITIONS.h"
#include "MainMenuState.h"

#include <iostream>

    SplashState::SplashState(GameDataRef data) : data(data)
    {

    }

    void SplashState::inizialize(){
        this->data->assets.loadTexture("CGLogo", "Content/cg_logo.png");
        this->data->assets.loadFont("TitleFont", "Content/Fonts/Vollkorn-Semibold.ttf");

        praktischeAspekte.setFont(this->data->assets.getFont("TitleFont"));
        derInformatik.setFont(this->data->assets.getFont("TitleFont"));
        sem.setFont(this->data->assets.getFont("TitleFont"));
        name.setFont(this->data->assets.getFont("TitleFont"));

        praktischeAspekte.setCharacterSize(40);
        derInformatik.setCharacterSize(40);
        sem.setCharacterSize(30);
        name.setCharacterSize(30);

        praktischeAspekte.setColor(sf::Color(0,36,72));
        derInformatik.setColor(sf::Color(0,36,72));
        sem.setColor(sf::Color::White);
        name.setColor(sf::Color(0,36,72));

        praktischeAspekte.setString("Praktische Aspekte");
        derInformatik.setString("der Informatik");
        sem.setString("SoSe 2018");
        name.setString("Mai Hellmann");

        praktischeAspekte.setPosition(220, 200);
        derInformatik.setPosition(260, 260);
        sem.setPosition(330,320);
        name.setPosition(300, 430);

        /*
        if(!music.openFromFile("Content/Music/titleMusic.ogg")){
            std::cout << "Fehler beim Musik laden." << std::endl;
        }

        music.setLoop(true);
        music.play();

         */
        background.setTexture(this->data->assets.getTexture("CGLogo"));
        background.setColor(sf::Color(255,255,255,50));
        background.setScale(0.5,0.5);
        background.setPosition(190,100);
    }

    void SplashState::handleInput()
    {
        sf::Event event;

        while (this->data->window.pollEvent(event))
        {
            if (sf::Event::Closed == event.type){
                this->data->window.close();
            }
        }
    }

    void SplashState::update(float dt)
    {
        if (clock.getElapsedTime().asSeconds() > SPLASH_STATE_SHOW_TIME)
        {
            //music.stop();

            // Switch To Main Menu

            MainMenuState* menu = new MainMenuState(this->data);

            data->machine.AddState(StateRef(menu), true);
            std::cout << "Go to main menu" << std::endl;
        }
    }

    void SplashState::draw(float dt) {
        this->data->window.clear(sf::Color(200,200,200));

        this->data->window.draw(this->background);
        this->data->window.draw(praktischeAspekte);
        this->data->window.draw(derInformatik);
        this->data->window.draw(sem);
        this->data->window.draw(name);

        this->data->window.display();
    }
