//
// Created by das_maichen on 07.08.18.
//

#include <iostream>
#include "StoryState3.h"
#include "../DEFINITIONS.h"
#include "MainMenuState.h"

void resizeViewStory3(const sf::RenderWindow& window, sf::View& view){
    float aspectRatio = float(window.getSize().x)/float(window.getSize().y);
    view.setSize(SCREEN_HEIGHT * aspectRatio, SCREEN_HEIGHT);
}

StoryState3::StoryState3(GameDataRef data)
        : data(data), view(sf::FloatRect(0,0,SCREEN_WIDTH, SCREEN_HEIGHT)){

}

void StoryState3::inizialize() {
    resizeViewStory3(this->data->window, view);

    backToMainMenu = false;
    musicOn = true;

    this->data->assets.loadTexture("Story3Image", "Content/story3_end.png");
    image.setTexture(this->data->assets.getTexture("Story3Image"));
    image.setScale(0.7,0.7);
    image.setPosition(-125,0);

    speechbubble = new Speechbubble(this->data, view);
    speechbubble->inizializeTextBox(sf::Vector2f(50, 420), sf::Vector2f(700, 150), "ARES: Wo fuehrt dieser Gang wohl hin? \n"
            "\n \n \n Fortsetzung folgt... "
    );

    if(!music.openFromFile("Content/Music/tumbling-down.ogg")){
        //if(!music.openFromFile("Content/Music/lost-in-the-forest.ogg")){
        std::cout << "Fehler beim Musik laden." << std::endl;
    }

    music.setLoop(true);
    music.play();

}

void StoryState3::handleInput() {
    sf::Event event;

    while (this->data->window.pollEvent(event)) {
        switch (event.type){
            case sf::Event::Closed:
                this->data->window.close();
                break;
            case sf::Event::Resized:
                resizeViewStory3(this->data->window, view);
            case sf::Event::KeyReleased:
                switch(event.key.code){
                    case sf::Keyboard::N:
                        backToMainMenu = true;
                        break;
                }
                break;
        }
    }

}

void StoryState3::update(float dt) {

    if(backToMainMenu){
        MainMenuState* mainMenuState = new MainMenuState(this->data);

        data->machine.AddState(StateRef(mainMenuState), true);

        music.stop();

        backToMainMenu = false;
    }

}

void StoryState3::draw(float dt) {
    this->data->window.clear(sf::Color(200,200,200));
    this->data->window.setView(view);

    this->data->window.draw(image);
    speechbubble->draw();


    this->data->window.display();

}

