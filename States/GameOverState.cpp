//
// Created by das_maichen on 22.05.18.
//

#include <iostream>
#include "GameOverState.h"
#include "../DEFINITIONS.h"
#include "GameStateJumpNRun.h"
#include "MainMenuState.h"


void resizeViewGameOverState(const sf::RenderWindow& window, sf::View& view){
    float aspectRatio = float(window.getSize().x)/float(window.getSize().y);
    view.setSize(SCREEN_HEIGHT * aspectRatio, SCREEN_HEIGHT);
}


GameOverState::GameOverState(GameDataRef data, int difficulity, bool musicOn, bool soundOn) :
        data(data),view(sf::FloatRect(0,0,SCREEN_WIDTH, SCREEN_HEIGHT)) {

    this->difficulity = difficulity;
    this->musicOn = musicOn;
    this->soundOn = soundOn;

}

void GameOverState::inizialize() {

    resizeViewGameOverState(this->data->window, view);

    if(!music.openFromFile("Content/Music/ares-died.ogg")){
        std::cout << "Fehler beim Musik laden." << std::endl;
    }

    music.setLoop(true);
    if(musicOn){
        music.play();
    }


    bufferOptions.loadFromFile("Content/Sounds/options.wav");
    optionSound.setBuffer(bufferOptions);

    tryAgain = false;
    backToMenu = false;

    int width = this->data->window.getSize().x;
    int height = this->data->window.getSize().y;

    this->data->assets.loadTexture("GameOverBackground", GAME_OVER_BACKGROUND_FILEPATH);
    this->data->assets.loadTexture("TryAgainButton", BUTTON_BACKGROUND_FILEPATH);
    this->data->assets.loadTexture("MenuButton", BUTTON_BACKGROUND_FILEPATH);

    text.setFont(this->data->assets.getFont("GameFont"));
    text.setString("Game Over");
    text.setCharacterSize(80);
    text.setColor(sf::Color::White);
    text.setPosition(220,60);

    menuText[0].setFont(this->data->assets.getFont("GameFont"));
    menuText[0].setColor(sf::Color::Cyan);
    menuText[0].setString("Try Again");
    //-50 ist hier noch ein workaround :P
    menuText[0].setPosition(340,300);

    menuText[1].setFont(this->data->assets.getFont("GameFont"));
    menuText[1].setColor(sf::Color::White);
    menuText[1].setString("Menu");
    menuText[1].setPosition(360,375);

    selectedItemIndex = 0;

    tryAgainButton.setTexture(this->data->assets.getTexture("TryAgainButton"));
    tryAgainButton.setPosition(265,288);
    tryAgainButton.setScale(0.2,0.2);

    quitButton.setTexture(this->data->assets.getTexture("MenuButton"));
    quitButton.setPosition(265,363);
    quitButton.setScale(0.2,0.2);
    quitButton.setColor((sf::Color(255, 255, 255, 128)));

    background.setTexture(this->data->assets.getTexture("GameOverBackground"));
    background.setPosition(-125,-100);
    background.setColor(sf::Color(255,255,255));
    background.setScale(sf::Vector2f(0.7,0.7));
}

void GameOverState::handleInput() {

    sf::Event event;

    while (this->data->window.pollEvent(event)){

        switch (event.type){
            case sf::Event::Closed:
                this->data->window.close();
                break;
            case sf::Event::Resized:
                resizeViewGameOverState(this->data->window,view);
                break;

            case sf::Event::KeyReleased:
                switch(event.key.code){
                    case sf::Keyboard::Up:
                        moveUp();
                        break;
                    case sf::Keyboard::Down:
                        moveDown();
                        break;

                    case sf::Keyboard::Return:
                        switch (getChosenItem()){
                            case 0:
                                //std::cout << "Play!" << std::endl;
                                tryAgain = true;
                                break;
                            case 1:
                                //std::cout << "Quit!" << std::endl;
                                //this->data->window.close();
                                backToMenu = true;
                                break;
                        }
                }
                break;
        }
    }

}

void GameOverState::update(float dt) {
    if(tryAgain){
        GameStateJumpNRun* gameStateJumpNRun = new GameStateJumpNRun(this->data, difficulity, musicOn, soundOn);

        data->machine.AddState(StateRef(gameStateJumpNRun), true);

        music.stop();
    }

    tryAgain = false;

    if(backToMenu){
        MainMenuState* mainMenuState = new MainMenuState(this->data);

        data->machine.AddState(StateRef(mainMenuState), true);

        music.stop();
    }

    backToMenu = false;

}

void GameOverState::draw(float dt) {
    this->data->window.clear(sf::Color::White);
    this->data->window.setView(view);
    this->data->window.draw(this->background);
    this->data->window.draw(text);
    this->data->window.draw(this->tryAgainButton);
    this->data->window.draw(this->quitButton);



    for(int i = 0; i<MAX_NUMBER_OF_ITEMS_GAMEOVER; i++){
        this->data->window.draw(this->menuText[i]);
    }

    this->data->window.display();

}


void GameOverState::moveUp() {
    if(selectedItemIndex -1 >= 0){
        if(soundOn){
            optionSound.play();
        }

        menuText[selectedItemIndex].setColor(sf::Color::White);
        selectedItemIndex--;
        menuText[selectedItemIndex].setColor(sf::Color::Cyan);
    }

    if(selectedItemIndex == 0){
        tryAgainButton.setColor(sf::Color(255,255,255,255));
        quitButton.setColor(sf::Color(255, 255, 255,128));
    }

    if(selectedItemIndex == 1){
        tryAgainButton.setColor(sf::Color(255,255,255,128));
        quitButton.setColor(sf::Color(255, 255, 255,255));
    }
}

void GameOverState::moveDown() {
    if(selectedItemIndex + 1 < MAX_NUMBER_OF_ITEMS_GAMEOVER){
        if(soundOn) {
            optionSound.play();
        }
        menuText[selectedItemIndex].setColor(sf::Color::White);
        selectedItemIndex ++;
        menuText[selectedItemIndex].setColor(sf::Color::Cyan);
    }

    if(selectedItemIndex == 0){
        tryAgainButton.setColor(sf::Color(255,255,255,255));
        quitButton.setColor(sf::Color(255, 255, 255,128));
    }

    if(selectedItemIndex == 1){
        tryAgainButton.setColor(sf::Color(255,255,255,128));
        quitButton.setColor(sf::Color(255, 255, 255,255));
    }
}

