//
// Created by das_maichen on 26.07.18.
//

#ifndef MYGAME_TEXTITEM_H
#define MYGAME_TEXTITEM_H

#include <SFML/Graphics/Sprite.hpp>

struct TextItem{
    std::string characterName;
    std::string characterImage;
    std::string text;

};

#endif //MYGAME_TEXTITEM_H
