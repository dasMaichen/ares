//
// Created by das_maichen on 12.05.18.
//

#ifndef MYGAME_GAMESTATEJUMPNRUN_H
#define MYGAME_GAMESTATEJUMPNRUN_H


#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include "BasicState.h"
#include "../Game.h"
#include "../Plattforms.h"
#include "../Player/PlayerAdventure.h"
#include "../Player/PlayerJumpNRun.h"
#include "../Obstacle.h"
#include "../Enemy.h"
#include "../Butterfly.h"

class GameStateJumpNRun : public BasicState{
public:
    GameStateJumpNRun(GameDataRef data, int difficulity, bool musicOn, bool soundOn);
    ~GameStateJumpNRun();
    void inizialize();

    void handleInput();
    void update(float dt);
    void draw(float dt);

    void setEnemies(std::string filename);


private:
    GameDataRef data;

    sf::Clock clock;
    sf::Texture backgroundTexture;
    sf::Sprite backgroundPart1;
    sf::Sprite backgroundPart2;
    sf::Sprite background2;
    sf::Sprite backgroundIntermediate;

    sf::Sprite foregroundPart1;
    sf::Sprite foregroundPart2;
    sf::CircleShape butterflies;

    sf::Music music;
    sf::Sound hitSound;
    sf::SoundBuffer bufferHitSound;

    Plattforms* plattform;
    Obstacle* obstacle;
    sf::View view;

    //PlayerAdventure* playerAdventure;

    PlayerJumpNRun* player;

    sf::RectangleShape movableStone;
    sf::Texture movableStoneTexture;
    sf::Sprite movableStoneSprite;

    bool debugModus;

    int gameState;

    Lives* lives;
    //Enemy* enemy;
    //Enemy* enemy2;

    std::vector<Enemy*> enemies;
    Butterfly* butterfly;

    float startTime;

    bool startGame;
    bool pauseModus;
    bool waiting;

    sf:: Sprite pauseMenu;
    sf::Text pauseText;
    sf::Text continueText;
    float startWaitingTime;

    int difficulity;
    bool musicOn;
    bool soundOn;

    sf::Sprite startPlayerSprite;
    sf::Sprite controlInfo;
    sf::Text controlInfoTitle;
    sf::Text controlArrowKey;
    sf::Text controlSpace;
    sf::Text controlPause;
    sf::Text controlReturn;

    sf::Sprite arrowKey;
    sf::Sprite spaceKey;
    sf::Sprite pKey;

    //std::vector<Plattforms> plattforms;
};


#endif //MYGAME_GAMESTATEJUMPNRUN_H
