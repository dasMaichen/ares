//
// Created by das_maichen on 09.05.18.
//

#ifndef MYGAME_SPLASHSTATE_H
#define MYGAME_SPLASHSTATE_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio/Music.hpp>
#include "BasicState.h"
#include "../Game.h"


class SplashState : public BasicState{
public:
    SplashState(GameDataRef data);

    void inizialize();

    void handleInput();
    void update(float dt);
    void draw(float dt);

private:
    GameDataRef data;
    sf::Clock clock;

    sf::Texture backgroundTexture;
    sf::Sprite background;
    sf::Text praktischeAspekte;
    sf::Text derInformatik;
    sf::Text sem;
    sf::Text name;

    sf::Music music;
};

#endif //MYGAME_SPLASHSTATE_H
