//
// Created by das_maichen on 09.05.18.
//



#include <sstream>
#include "../DEFINITIONS.h"
#include "MainMenuState.h"
#include "GameStateJumpNRun.h"
#include "StoryState1.h"
#include "AdventureState.h"
#include "StoryState2.h"

#include <iostream>

void resizeView(const sf::RenderWindow& window, sf::View& view){
    float aspectRatio = float(window.getSize().x)/float(window.getSize().y);
    view.setSize(SCREEN_HEIGHT * aspectRatio, SCREEN_HEIGHT);
}

MainMenuState::MainMenuState(GameDataRef data) :
  data(data), view(sf::FloatRect(0,0,SCREEN_WIDTH, SCREEN_HEIGHT))
{

}

void MainMenuState::inizialize(){

    resizeView(this->data->window, view);
    optionBox = new OptionBox(this->data, view);

    goToStory = false;
    adventurePart = false;
    optionOpen = false;
    goToStory2 = false;
    load = false;

    musicOn = true;
    soundOn = true;
    musicIsTurnedOff = false;

    if(!music.openFromFile("Content/Music/titleMusic.ogg")){
        std::cout << "Fehler beim Musik laden." << std::endl;
    }

    music.setLoop(true);
    music.play();

    buffer.loadFromFile("Content/Sounds/options.wav");
    buttonSound.setBuffer(buffer);

    //bufferChoseOption.loadFromFile("Content/Sounds/choseOption.wav");
    bufferChoseOption.loadFromFile("Content/Sounds/open-settings.wav");
    choseSound.setBuffer(bufferChoseOption);

    bufferCloseOption.loadFromFile("Content/Sounds/close-settings.wav");
    closeOption.setBuffer(bufferCloseOption);
    /*
    int width = this->data->window.getSize().x;
    int height = this->data->window.getSize().y;
    float aspectRatio = float(this->data->window.getSize().x)/float(this->data->window.getSize().y);
    //int width = view.getSize().x;
    //int height = view.getSize().y;
*/
    //this->data->assets.loadTexture("OptionBox", "Content/optionBox.png");
    this->data->assets.loadTexture("MainMenuBackground", MAIN_MENU_BACKGROUND_FILEPATH);
    //this->data->assets.loadTexture("GameTitle", GAME_TITLE_FILEPATH);
    this->data->assets.loadTexture("PlayButton", BUTTON_BACKGROUND_FILEPATH);
    this->data->assets.loadTexture("LoadButton", BUTTON_BACKGROUND_FILEPATH);
    this->data->assets.loadTexture("OptionButton", BUTTON_BACKGROUND_FILEPATH);
    this->data->assets.loadTexture("QuitButton", BUTTON_BACKGROUND_FILEPATH);

    this->data->assets.loadTexture("Ares", MAIN_MENU_ARES_FILEPATH);

    this->data->assets.loadFont("GameFont", GAME_FONT_FILEPATH);
    title.setFont(this->data->assets.getFont("GameFont"));

    title.setString("Ares");
    title.setCharacterSize(150);
    title.setColor(sf::Color::Cyan);
    title.setPosition(250,30);
    //title.setPosition((width/2 - 150)/aspectRatio, height/20);

    menuText[0].setFont(this->data->assets.getFont("GameFont"));
    menuText[0].setColor(sf::Color::Cyan);
    menuText[0].setString("New Game");
    //-50 ist hier noch ein workaround :P
    menuText[0].setPosition(335,260);
    //menuText[0].setPosition(sf::Vector2f((width/2 - 50), height/(MAX_NUMBER_OF_ITEMS_MAINMENU+1)*2));

    menuText[1].setFont(this->data->assets.getFont("GameFont"));
    menuText[1].setColor(sf::Color::White);
    menuText[1].setString("Load");
    menuText[1].setPosition(365,335);

    menuText[2].setFont(this->data->assets.getFont("GameFont"));
    menuText[2].setColor(sf::Color::White);
    menuText[2].setString("Settings");
    menuText[2].setPosition(345,410);
    //menuText[1].setPosition(sf::Vector2f(width/2 - 50, height/(MAX_NUMBER_OF_ITEMS_MAINMENU+1)*2.5));

    menuText[3].setFont(this->data->assets.getFont("GameFont"));
    menuText[3].setColor(sf::Color::White);
    menuText[3].setString("Quit");
    menuText[3].setPosition(370,485);
    //menuText[2].setPosition(sf::Vector2f(width/2 - 50, height/(MAX_NUMBER_OF_ITEMS_MAINMENU+1)*3));

    selectedItemIndex = 0;

    background.setTexture(this->data->assets.getTexture("MainMenuBackground"));
    background.setColor(sf::Color(0, 200, 200, 128));
    background.setPosition(-120,0);
    background.setScale(1.3,1);
    //titleSprite.setTexture(this->data->assets.getTexture("GameTitle"));

    playButton.setTexture(this->data->assets.getTexture("PlayButton"));
    //playButton.setPosition(sf::Vector2f(width/2 - 135, height/(MAX_NUMBER_OF_ITEMS_MAINMENU+1)*2-12));
    playButton.setPosition(265,247);
    playButton.setScale(0.2,0.2);

    loadButton.setTexture(this->data->assets.getTexture("LoadButton"));
    //playButton.setPosition(sf::Vector2f(width/2 - 135, height/(MAX_NUMBER_OF_ITEMS_MAINMENU+1)*2-12));
    loadButton.setPosition(265,322);
    loadButton.setScale(0.2,0.2);
    loadButton.setColor(sf::Color(255,255,255,128));

    optionButton.setTexture(this->data->assets.getTexture("OptionButton"));
    //optionButton.setPosition(sf::Vector2f(width/2 - 135, height/(MAX_NUMBER_OF_ITEMS_MAINMENU+1)*2.5-12));
    optionButton.setPosition(265,397);
    optionButton.setScale(0.2,0.2);
    optionButton.setColor((sf::Color(255, 255, 255, 128)));

    quitButton.setTexture(this->data->assets.getTexture("QuitButton"));
    //quitButton.setPosition(sf::Vector2f(width/2 - 135, height/(MAX_NUMBER_OF_ITEMS_MAINMENU+1)*3-12));
    quitButton.setPosition(265,472);
    quitButton.setScale(0.2,0.2);
    quitButton.setColor(sf::Color(255, 255, 255, 128));

    ares.setTexture(this->data->assets.getTexture("Ares"));
    //ares.setPosition(sf::Vector2f(width/4 * 3, 100));
    ares.setPosition(600,100);
    ares.setScale(0.5,0.5);


}

void MainMenuState::handleInput() {
    sf::Event event;

    while (this->data->window.pollEvent(event)) {

        switch (event.type) {
            case sf::Event::Closed:
                this->data->window.close();
                break;
            case sf::Event::Resized:
                resizeView(this->data->window, view);
                break;
        }
        if (optionOpen) {
            switch (event.type) {
                case sf::Event::KeyReleased:
                    if(optionBox->getReturnTo() && event.key.code == sf::Keyboard::Return){
                        if(soundOn){
                            closeOption.play();
                        }
                        optionOpen = false;
                        optionBox->setSelectedBoxIndexToBeginn();
                        return;
                    }
                    switch (event.key.code) {
                        case sf::Keyboard::Up:
                            optionBox->moveUp();
                            optionBox->setSelectedOptionIndex(optionBox->getChosenOptionIndex());
                            break;
                        case sf::Keyboard::Down:
                            optionBox->moveDown();
                            optionBox->setSelectedOptionIndex(optionBox->getChosenOptionIndex());
                            break;
                        case sf::Keyboard::Right:
                            optionBox->moveRight();
                            if(optionBox->getMusicState() == 0){
                                musicOn = true;
                            } else if(optionBox->getMusicState() == 1){
                                musicOn = false;
                            }
                            if(optionBox->getSoundState() == 0){
                                soundOn = true;
                            }else if(optionBox->getSoundState() == 1){
                                soundOn = false;
                            }
                            break;
                        case sf::Keyboard::Left:
                            optionBox->moveLeft();
                            if(optionBox->getMusicState() == 0){
                                musicOn = true;
                            } else if(optionBox->getMusicState() == 1){
                                musicOn = false;
                            }
                            if(optionBox->getSoundState() == 0){
                                soundOn = true;
                            }else if(optionBox->getSoundState() == 1){
                                soundOn = false;
                            }
                            break;
                    }
                    break;
            }
        }

        if (!optionOpen) {
            switch (event.type) {

                case sf::Event::KeyReleased:
                    switch (event.key.code) {
                        case sf::Keyboard::Up:
                            moveUp();
                            break;
                        case sf::Keyboard::Down:
                            moveDown();
                            break;
                        case sf::Keyboard::S:
                            adventurePart = true;
                            break;
                        case sf::Keyboard:: A:
                            goToStory2 = true;


                        case sf::Keyboard::Return:
                            switch (getChosenItem()) {
                                case 0:
                                    std::cout << "Play!" << std::endl;
                                    goToStory = true;
                                    break;
                                case 1:
                                    std::cout << "Load!" << std::endl;
                                    load = true;
                                    break;
                                case 2:
                                    std::cout << "Option!" << std::endl;
                                    optionOpen = true;
                                    if(soundOn){
                                        choseSound.play();
                                    }
                                    break;
                                case 3:
                                    std::cout << "Quit!" << std::endl;
                                    this->data->window.close();
                                    break;
                            }
                    }
                    break;
            }
        }
    }
}


void MainMenuState::update(float dt) {

    this->difficulityLevel = optionBox->getDifficultyLevelState();

    if(optionBox->getMusicState() == 0){
        musicOn = true;
    }else if(optionBox->getMusicState() == 1){
        musicOn = false;
    }
    /*
    if(optionBox->getMusicState() == 0){
        musicOn = true;
        //musicIsTurnedOff = false;
    } else if(optionBox->getMusicState() == 1){
        musicOn = false;
    }
    */
    if(!musicOn) {
        //musicOn = false;
        music.stop();
        musicIsTurnedOff = true;
    }if(musicIsTurnedOff && musicOn){
        music.play();
        musicIsTurnedOff = false;
    }
    if(goToStory){
        //choseSound.play();

        StoryState1* storyState1 = new StoryState1(this->data, difficulityLevel, musicOn, soundOn);
        data->machine.AddState(StateRef(storyState1), true);
        music.stop();

        //GameStateJumpNRun* gameStateJumpNRun = new GameStateJumpNRun(this->data);

        //data->machine.AddState(StateRef(gameStateJumpNRun), true);

        //music.stop();
    }

    goToStory = false;

    if(goToStory2){
        StoryState2* storyState2 = new StoryState2(this->data);
        data->machine.AddState(StateRef(storyState2), true);
        music.stop();

        goToStory2 = false;
    }

    if(adventurePart){
        AdventureState* adventureState = new AdventureState(this->data, difficulityLevel, musicOn, soundOn, false);
        data->machine.AddState(StateRef(adventureState), true);
        music.stop();

        adventurePart = false;
    }

    if(load){
        AdventureState* adventureState = new AdventureState(this->data, difficulityLevel, musicOn, soundOn, true);
        data->machine.AddState(StateRef(adventureState), true);
        music.stop();

        load = false;
    }

    //if(optionOpen){
      //  optionBox->update();
    //}


    //this->title.setString("Ares bla");
}

void MainMenuState::draw(float dt) {
    this->data->window.clear();
    this->data->window.setView(view);

    this->data->window.draw(this->background);
    this->data->window.draw(this->titleSprite);
    this->data->window.draw(this->playButton);
    this->data->window.draw(this->loadButton);
    this->data->window.draw(this->optionButton);
    this->data->window.draw(this->quitButton);
    this->data->window.draw(this->title);
    this->data->window.draw(ares);

    for(int i = 0; i<MAX_NUMBER_OF_ITEMS_MAINMENU; i++){
        this->data->window.draw(this->menuText[i]);
    }

    if(optionOpen){
        optionBox->draw();
    }

    this->data->window.display();
}

void MainMenuState::moveUp() {
    if(selectedItemIndex -1 >= 0){
        if(soundOn){
            buttonSound.play();
        }
        menuText[selectedItemIndex].setColor(sf::Color::White);
        selectedItemIndex--;
        menuText[selectedItemIndex].setColor(sf::Color::Cyan);
    }

    if(selectedItemIndex == 0){
        playButton.setColor(sf::Color(255,255,255,255));
        loadButton.setColor(sf::Color(255,255,255, 128));
        optionButton.setColor(sf::Color(255, 255, 255,128));
        quitButton.setColor(sf::Color(255,255,255,128));
    }

    if(selectedItemIndex == 1){
        playButton.setColor(sf::Color(255,255,255,128));
        loadButton.setColor(sf::Color(255,255,255, 255));
        optionButton.setColor(sf::Color(255, 255, 255,128));
        quitButton.setColor(sf::Color(255,255,255,128));
    }

    if(selectedItemIndex == 2){
        playButton.setColor(sf::Color(255,255,255,128));
        loadButton.setColor(sf::Color(255,255,255, 128));
        optionButton.setColor(sf::Color(255, 255, 255,255));
        quitButton.setColor(sf::Color(255,255,255,128));
    }

    if(selectedItemIndex == 3){
        playButton.setColor(sf::Color(255,255,255,128));
        loadButton.setColor(sf::Color(255,255,255, 128));
        optionButton.setColor(sf::Color(255, 255, 255,128));
        quitButton.setColor(sf::Color(255,255,255,255));
    }


}

void MainMenuState::moveDown() {
    if(selectedItemIndex + 1 < MAX_NUMBER_OF_ITEMS_MAINMENU){
        if(soundOn){
            buttonSound.play();
        }
        menuText[selectedItemIndex].setColor(sf::Color::White);
        selectedItemIndex ++;
        menuText[selectedItemIndex].setColor(sf::Color::Cyan);
    }

    if(selectedItemIndex == 0){
        playButton.setColor(sf::Color(255,255,255,255));
        loadButton.setColor(sf::Color(255,255,255, 128));
        optionButton.setColor(sf::Color(255, 255, 255,128));
        quitButton.setColor(sf::Color(255,255,255,128));
    }

    if(selectedItemIndex == 1){
        playButton.setColor(sf::Color(255,255,255,128));
        loadButton.setColor(sf::Color(255,255,255, 255));
        optionButton.setColor(sf::Color(255, 255, 255,128));
        quitButton.setColor(sf::Color(255,255,255,128));
    }

    if(selectedItemIndex == 2){
        playButton.setColor(sf::Color(255,255,255,128));
        loadButton.setColor(sf::Color(255,255,255, 128));
        optionButton.setColor(sf::Color(255, 255, 255,255));
        quitButton.setColor(sf::Color(255,255,255,128));
    }

    if(selectedItemIndex == 3){
        playButton.setColor(sf::Color(255,255,255,128));
        loadButton.setColor(sf::Color(255,255,255, 128));
        optionButton.setColor(sf::Color(255, 255, 255,128));
        quitButton.setColor(sf::Color(255,255,255,255));
    }

}
