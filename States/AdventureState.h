//
// Created by das_maichen on 04.06.18.
//

#ifndef MYGAME_ADVENTURESTATE_H
#define MYGAME_ADVENTURESTATE_H


#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include "../Game.h"
#include "../Player/PlayerAdventure.h"
#include "../Plattforms.h"
#include "../Level.h"
#include "../Bullet.h"
#include "../Inventar.h"
#include "../Speechbubble.h"
#include "Dialogs.h"
#include "../Ghost.h"

class AdventureState : public BasicState{
public:
    AdventureState(GameDataRef data, int difficulity, bool musicOn, bool soundOn, bool load);
    ~AdventureState();

    void inizialize();

    void handleInput();
    void update(float dt);
    void draw(float dt);

    void itemMoveRight();
    void itemMoveLeft();

    std::vector<TextItem>* updateDialog(sf::Vector2f position, sf::Vector2f direction);


private:
    GameDataRef data;
    sf::View view;
    sf::Music music;

    PlayerAdventure* player;
    Ghost* ghost1;
    Ghost* ghost2;
    Ghost* ghost3;
    Ghost* ghost4;

    Level* level;

    Dialogs dialogs;
    std::vector<TextItem>* currentDialog;
    int currentDialogIndex;

    bool debugModus;
    bool itemGet;
    bool menuOpen;

    bool alreadyDialogOpen;
    bool torchDialog;
    bool threeTorchDialog;
    bool fourTorchDialog;
    bool guardianActivate;
    bool room3switchActive;
    bool room3chestOpen;
    bool enterRoom3;
    bool itemActivate;
    bool torchOn;
    bool useTorch;
    bool ghost3Defeat;
    bool clearLevel2;
    bool bowlOn;

    bool load;

    sf::Vector2f currentPlayerPosition;
    sf::Vector2f currentPlayerDirection;

    int difficulity;
    bool musicOn;
    bool soundOn;
    bool speechBubbleOn;

    int countSteps;

    sf::Sprite activeItemSprite;
    sf::Sprite activeItemButton;

    sf::Sprite fackelAnSprite;

    //sf::Sprite keySprite;
    //sf::Text keyText;

    std::vector<Bullet*> arrows;

    Inventar* inventar;

    Speechbubble* speechbubble;
    sf::Sprite speechBoxCharacterSprite;

    void updateSpeechbubble();


    ///Sounds
    sf::Sound soundSwitch;
    sf::SoundBuffer bufferSwitch;
    sf::Sound soundOpenDoor;
    sf::SoundBuffer bufferOpenDoor;
    sf::Sound soundUnlockDoor;
    sf::SoundBuffer bufferUnlockDoor;
    sf::Sound soundSolved;
    sf::SoundBuffer bufferSolved;
    sf::Sound soundFire;
    sf::SoundBuffer bufferFire;
    sf::Sound soundGhost;
    sf::SoundBuffer bufferGhost;
    sf::Sound soundGetItem;
    sf::SoundBuffer bufferGetItem;
};


#endif //MYGAME_ADVENTURESTATE_H
