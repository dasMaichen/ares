//
// Created by das_maichen on 22.05.18.
//

#ifndef MYGAME_GAMEOVERSTATE_H
#define MYGAME_GAMEOVERSTATE_H


#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Sound.hpp>
#include "BasicState.h"
#include "../Game.h"
#include "../DEFINITIONS.h"

class GameOverState : public BasicState {
public:
    GameOverState(GameDataRef data, int difficulity, bool musicOn, bool soundOn);
    ~GameOverState();

    void inizialize();

    void handleInput();
    void update(float dt);
    void draw(float dt);
    int getChosenItem(){ return selectedItemIndex;};

private:
    void moveUp();
    void moveDown();

    GameDataRef data;
    sf::Sprite background;
    sf::Text text;
    sf::View view;

    bool tryAgain;
    bool backToMenu;
    sf::Sprite tryAgainButton;
    sf::Sprite quitButton;
    sf::Text menuText[MAX_NUMBER_OF_ITEMS_GAMEOVER];
    int selectedItemIndex;
    sf::Music music;
    sf::SoundBuffer bufferOptions;
    sf::Sound optionSound;

    int difficulity;
    bool musicOn;
    bool soundOn;
};


#endif //MYGAME_GAMEOVERSTATE_H
