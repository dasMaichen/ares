//
// Created by das_maichen on 09.05.18.
//

#ifndef MYGAME_STATEMACHINE_H
#define MYGAME_STATEMACHINE_H

#include <memory>
#include <stack>

#include "BasicState.h"


    typedef std::unique_ptr<BasicState> StateRef;

    class StateMachine
    {
    public:
        StateMachine() { }
        ~StateMachine() { }

        void AddState(StateRef newState, bool isReplacing = true);
        void RemoveState();
        // Run at start of each loop in Game.cpp
        void ProcessStateChanges();

        StateRef &GetActiveState();

    private:
        std::stack<StateRef> _states;
        StateRef _newState;

        bool _isRemoving;
        bool _isAdding;
        bool _isReplacing;
    };



#endif //MYGAME_STATEMACHINE_H
