//
// Created by das_maichen on 29.05.18.
//

#include <iostream>
#include "StoryState1.h"
#include "../DEFINITIONS.h"
#include "GameStateJumpNRun.h"

void resizeViewStory1(const sf::RenderWindow& window, sf::View& view){
    float aspectRatio = float(window.getSize().x)/float(window.getSize().y);
    view.setSize(SCREEN_HEIGHT * aspectRatio, SCREEN_HEIGHT);
}


StoryState1::StoryState1(GameDataRef data, int difficulity, bool musicOn, bool soundOn) : data(data), view(sf::FloatRect(0,0,SCREEN_WIDTH, SCREEN_HEIGHT)){

    this->musicOn = musicOn;
    this->soundOn = soundOn;
    this->difficulity = difficulity;
}

void StoryState1::inizialize() {
    resizeViewStory1(this->data->window, view);

    speechbubble = new Speechbubble(this->data, view);
    speechbubble->inizializeTextBox(sf::Vector2f(50, 420), sf::Vector2f(700, 150), "Na toll, ich habe mich verlaufen. "
            "Dieser Teil des Waldes kommt mir recht unbekannt vor... und urgh, was sind das fuer Schmetterlinge? "
            "Sie verstreuen eine unangenehme Miasma. "
    );

    readyToPlay = false;
    nextPage = false;
    pageNo = 1;
    maxPage = 4;

    if(!music.openFromFile("Content/Music/tumbling-down.ogg")){
    //if(!music.openFromFile("Content/Music/lost-in-the-forest.ogg")){
        std::cout << "Fehler beim Musik laden." << std::endl;
    }

    music.setLoop(true);
    if(musicOn){
        music.play();
    }


    bufferNextPage.loadFromFile("Content/Sounds/options.wav");
    nextPageSound.setBuffer(bufferNextPage);

    this->data->assets.loadTexture("Story_1", "Content/story1.png");
    storyImage.setTexture(this->data->assets.getTexture("Story_1"));
    this->data->assets.loadTexture("Story_2", "Content/story2.png");
    this->data->assets.loadTexture("Story_3", "Content/story3.png");
    this->data->assets.loadTexture("Story_4", "Content/story4.png");

    storyImage.setPosition(0,0);

    this->data->assets.loadTexture("NextButton", STORY_NEXT_BUTTON_FILEPATH);

}




void StoryState1::handleInput() {
    sf::Event event;

    while (this->data->window.pollEvent(event)){

        switch (event.type){
            case sf::Event::Closed:
                this->data->window.close();
                break;
            case sf::Event::Resized:
                resizeViewStory1(this->data->window,view);
                break;

            case sf::Event::KeyReleased:
                switch(event.key.code){
                    case sf::Keyboard::Space:
                        //goToNextScene();
                        nextPage = true;
                        pageNo = pageNo +1;
                    std::cout << "next Image" << std::endl;
                        break;
                    case sf::Keyboard::S:
                        readyToPlay = true;
                        break;
                }
                break;
        }
    }
}



void StoryState1::update(float dt) {

    if(pageNo > maxPage){
        readyToPlay = true;
    }

    if(readyToPlay){
        GameStateJumpNRun* gameStateJumpNRun = new GameStateJumpNRun(this->data, difficulity, musicOn, soundOn);

        data->machine.AddState(StateRef(gameStateJumpNRun), true);

        music.stop();

        readyToPlay = false;
    }


    if(nextPage && pageNo <= maxPage){

        if(soundOn){
            nextPageSound.play();
        }
        std::string filename = "Story_" + std::to_string(pageNo);
        storyImage.setTexture(this->data->assets.getTexture(filename));
        //storyImage.setColor(sf::Color(255,255,255,200));
        nextPage = false;
    }

    if(pageNo == 2){
        speechbubble->setText("Hm... aber sie moegen wie andere Schmetterlinge auch Blumen. ");
    }
    if(pageNo == 3){
        speechbubble->setText("Oh nein! Die Blume... die verwelkt... Sie scheinen wirklich irgendwas Uebles auszustreuen! "
                                      "Besser ich komme nicht mit ihnen in Kontakt. ");
    }
    if(pageNo == 4){
        speechbubble->setTextSize(40);
        speechbubble->setText("Oh nein! Ganz viele! ");
    }

    storyImage.setScale(0.7,0.7);
    storyImage.setPosition(-120,0);


}

void StoryState1::draw(float dt) {

    this->data->window.clear();
    this->data->window.setView(view);

    this->data->window.draw(storyImage);

    //this->data->window.draw(textBox);

    //this->data->window.draw(storyBox);
    //this->data->window.draw(nextButton);


    //this->data->window.draw(text1);
    speechbubble->draw();

    this->data->window.display();

}
